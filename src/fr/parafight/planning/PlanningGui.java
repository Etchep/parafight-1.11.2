package fr.parafight.planning;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class PlanningGui implements Listener, CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player){
			Player p = (Player)sender;
			p.openInventory(planning);
		}
		return false;
	}
	
	Inventory planning = Bukkit.createInventory(null, 54, "§6§lPlanning des events");
	
	
	
	public static ItemStack getItem(Material material, String name, int quantity, int damage, String desc1, String desc2, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		ArrayList<String> list = new ArrayList<String>();
		if(desc1 != null) list.add(desc1);
		list.add("§c");
		if(desc2 != null) list.add(desc2);
		itM.setLore(list);
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}
	
	
	@EventHandler
	public void onOpen(InventoryOpenEvent e){
		Inventory inv = e.getInventory();
		if(inv.getName().equalsIgnoreCase(planning.getName())){
			inv.setItem(19, getItem(Material.WOOL, "§6§lLundi", 1, 5, "§e§l18h §6➥ §e§lKOTH", null, true));
			inv.setItem(20, getItem(Material.WOOL, "§6§lMardi", 1, 5, "§e§l18h §6➥ §e§lKOTH", null, true));
			inv.setItem(21, getItem(Material.WOOL, "§6§lMercredi", 1, 5, "§e§l15h §6➥ §e§lKOTH", "§e§l18h §6➥ §e§lKOTH", true));
			inv.setItem(22, getItem(Material.WOOL, "§6§lJeudi", 1, 5, "§e§l18h §6➥ §e§lKOTH", null, true));
			inv.setItem(23, getItem(Material.WOOL, "§6§lVendredi", 1, 5, "§e§l18h §6➥ §e§lKOTH", "§e§l21h §6➥ §e§lKOTH", true));
			inv.setItem(24, getItem(Material.WOOL, "§6§lSamedi", 1, 5, "§e§l15h §6➥ §e§lKOTH", "§e§l21h §6➥ §e§lKOTH", true));
			inv.setItem(25, getItem(Material.WOOL, "§6§lDimanche", 1, 5, "§e§l15h §6➥ §e§lKOTH", "§e§l19h §6➥ §e§lKOTH", true));
		}
	}
	
	@EventHandler
	public void onInteract(InventoryClickEvent e){
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if(current == null) return;
		if(inv.getName().equals(planning.getName())){
			e.setCancelled(true);
		}


	}

}
