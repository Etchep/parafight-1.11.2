package fr.parafight.factions;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandClassement
  implements CommandExecutor
{
  public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args)
  {
    if ((sender instanceof Player))
    {
      Player p = (Player)sender;
      if (args.length == 0)
      {
        p.sendMessage("�e�m---------------------------");
        p.sendMessage("�dTop 10 des meilleurs Factions:");
        for (int i = 0; i < FactionsSql.getFTopFactions().size(); i++) {
          p.sendMessage("�6�l" + (i + 1) + ") �e" + (String)FactionsSql.getFTopFactions().get(i) + " �6-�e " + (String)FactionsSql.getFTopPoints().get(i) + " points");
        }
        p.sendMessage("�e�m---------------------------");
      }
    }

    return false;
  }
}