package fr.parafight.factions;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;

public class CommandPoints implements CommandExecutor{	

	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args)
	{
		if ((sender instanceof Player))
		{
			Player p = (Player)sender;
			Faction f = FPlayers.getInstance().getByPlayer(p).getFaction();
			if (args.length == 0)
			{
				p.sendMessage("�dVotre faction a �5" + FactionsSql.getFPoints(f) + " points�d.");
			}
			if (args.length == 1)
			{
				Faction target = Factions.getInstance().getByTag(args[0]);
				if(target != null){
					p.sendMessage("�dLa faction "+ target.getTag() +"  a �5" + FactionsSql.getFPoints(f) + " points�d.");
				}else{
					p.sendMessage("La faction " + args[0] + " n'a pas �t� trouv�e.");
				}
			}
		}
		return false;
	}

}
