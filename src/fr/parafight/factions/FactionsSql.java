package fr.parafight.factions;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.massivecraft.factions.Faction;

import fr.parafight.sql.DbManager;

public class FactionsSql {
	
	
	public static boolean factionExist(String id) {
		
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT fid FROM Factions WHERE fid = ?");
			q.setString(1, id);
			ResultSet resultat = q.executeQuery();
			boolean hasAccount = resultat.next();
			q.close();
			connection.close();
			return hasAccount;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return false;
	}
  public static void addFaction(Faction faction){
		if(!factionExist(faction.getId())){
			try {
				Connection connection = DbManager.BD.getDbAccess().getConnection();
				PreparedStatement q = connection.prepareStatement("INSERT INTO Factions(faction, points , fid) VALUES (?,?,?)");
				q.setString(1, faction.getTag());
				q.setInt(2, 0);
				q.setString(3, faction.getId());
				q.execute();
				q.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
  }
  
  public static void remFaction(Faction f){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("DELETE FROM Factions WHERE fid = ?");
			q.setString(1, f.getId());
			q.execute();
			q.close();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
  }
  
  
  public static void nameChange(Faction faction){
			try {
				Connection connection = DbManager.BD.getDbAccess().getConnection();
				PreparedStatement q = connection.prepareStatement("UPDATE Factions SET faction = ? WHERE fid = ?");
				q.setString(1, faction.getTag());
				q.setString(2, faction.getId());
				q.executeUpdate();
				q.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
  }
  public static void setFPoints(Faction faction, int amount)
  {
    try
    {
    	Connection connection = DbManager.BD.getDbAccess().getConnection();
      PreparedStatement rs = connection.prepareStatement("UPDATE Factions set points = ? WHERE fid = ?");
      rs.setInt(1, amount);
      rs.setString(2, faction.getId());
      rs.executeUpdate();
      rs.close();
      connection.close();
    }
    catch (SQLException e) {
      e.printStackTrace();
    }
  }
  
	public static int getFPoints(Faction faction){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT points FROM Factions WHERE fid = ?");
			q.setString(1, faction.getId());
			ResultSet rs = q.executeQuery();
	           int nombre = 0;
	           
	            while(rs.next()){
	                nombre = rs.getInt("points");
	            }
	           
	            q.close();
	            connection.close();
			
			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static ArrayList<String> getFTopFactions(){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT faction FROM Factions ORDER BY points DESC LIMIT 10") ;
			ResultSet rs = q.executeQuery();
			ArrayList<String> list = new ArrayList<>();
	         
	           
	            while(rs.next()){
	                list.add(rs.getString("faction"));
	            }
	            q.close();
	            connection.close();
	           
			
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static ArrayList<String> getFTopPoints(){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT points FROM Factions ORDER BY points DESC LIMIT 10") ;
			ResultSet rs = q.executeQuery();
			ArrayList<String> list = new ArrayList<>();
	         
	           
	            while(rs.next()){
	                list.add(rs.getString("points"));
	            }
	            q.close();
	            connection.close();
	           
			
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public static int getApX(Faction faction){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT apX FROM Factions WHERE fid = ?");
			q.setString(1, faction.getId());
			ResultSet rs = q.executeQuery();
	           int nombre = 0;
	           
	            while(rs.next()){
	                nombre = rs.getInt("apX");
	            }
	           
	            q.close();
	            connection.close();
			
			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	public static int getApY(Faction faction){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT apY FROM Factions WHERE fid = ?");
			q.setString(1, faction.getId());
			ResultSet rs = q.executeQuery();
	           int nombre = 0;
	           
	            while(rs.next()){
	                nombre = rs.getInt("apY");
	            }
	           
	            q.close();
	            connection.close();
			
			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	public static int getApZ(Faction faction){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT apZ FROM Factions WHERE fid = ?");
			q.setString(1, faction.getId());
			ResultSet rs = q.executeQuery();
	           int nombre = 0;
	           
	            while(rs.next()){
	                nombre = rs.getInt("apZ");
	            }
	           
	            q.close();
	            connection.close();
			
			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	public static void setAp(Faction faction, int x, int y, int z)
	{
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("UPDATE Factions SET apX = ?, apY = ?, apZ = ?  WHERE fid = ?");
			q.setInt(1, x);
			q.setInt(2, y);
			q.setInt(3, z);
			q.setString(4, faction.getId());
			q.executeUpdate();
			q.close();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
