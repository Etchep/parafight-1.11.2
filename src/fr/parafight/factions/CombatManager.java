package fr.parafight.factions;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import fr.parafight.Main;

public class CombatManager implements Listener{

	private static Main main;

	public CombatManager(Main main) {
		CombatManager.main = main;
	}

	public static enum CombatStatut1 {
		GAME,FREE;
	}
	public static enum CombatStatut2 {
		GAME,FREE;
	}

	public static HashMap<Player, ItemStack[][]> inv_store = new HashMap<Player, ItemStack[][]>();
	public static HashMap<Player, String> teams = CommandCombat.teams;
	public static Map<Faction, Faction> attente = CommandCombat.attente;
	public static HashMap<Faction, Integer> kills = new HashMap<Faction, Integer>();
	public static HashMap<Player, Integer> playersinarena = CommandCombat.playersinarena;
	public static HashMap<Player, Boolean> died = new HashMap<Player, Boolean>();
	public static CombatStatut1 Arene1;
	public static CombatStatut2 Arene2;

	public static ArrayList<Player> getMembers(Faction f){
		ArrayList<Player> list = new ArrayList<>();
		for(Player p : teams.keySet()){
			if(teams.get(p) == f.getTag()){
				list.add(p);
			}
		}
		return list;
	}

	public static void saveInv(Player p){
		ItemStack[] [] store = new ItemStack[2][1];
		store[0] = p.getInventory().getContents();
		store[1] = p.getInventory().getArmorContents();
		inv_store.put(p, store);
		p.getInventory().clear();
	}

	public static void restoreInv(Player p){
		if(inv_store.containsKey(p)){
			p.getInventory().clear();
			p.getInventory().setContents(inv_store.get(p)[0]);
			p.getInventory().setArmorContents(inv_store.get(p)[1]);
			inv_store.remove(p);
			p.updateInventory();
		}
	}

	static Location a1a = new Location(Bukkit.getWorld("world"), -17, 145, -87.5, 90, 0);
	static Location a1b = new Location(Bukkit.getWorld("world"), -16.5, 145, -87.5, -90, 0);
	static Location a2a = new Location(Bukkit.getWorld("world"), -274, 69, 96);
	static Location a2b = new Location(Bukkit.getWorld("world"), -274, 69, 96);

	private static BukkitTask task;


	public static void startCombat(Plugin pl, Faction a, Faction b, int arene){
		if(CommandCombat.teamMembers(a) != CommandCombat.teamMembers(b)){
			endCombat(pl, a, b, arene, true);
			Bukkit.broadcastMessage("§dArène 1 §5§l>>§d Le combat n'a pas pu commencer, un ou plusieurs participants on quitté la partie. Arrêt forcé du combat...");
			return;
		}
		if(arene == 1){
			Arene1 = CombatStatut1.GAME;
		}else if(arene == 2){
			Arene2 = CombatStatut2.GAME;
		}
		Bukkit.broadcastMessage("§dArène 1 §5§l>>§d Début d'un combat à §5"+ CommandCombat.teamMembers(a) +" §dcontre §5" + CommandCombat.teamMembers(b) + "§d, La faction §5"+ a.getTag() +" §dcontre §5"+ b.getTag() +"§d !");
		BukkitRunnable runnable = new BukkitRunnable() {
			int timer = 312;
			ArrayList<Player> aplayers = getMembers(a);
			ArrayList<Player> bplayers = getMembers(b);
			@Override
			public void run()
			{
				timer--;
				if(kills.get(a) == null){
					kills.put(b, bplayers.size());
					kills.put(a, aplayers.size());
				}
				if(timer == 310 || timer == 305 || timer == 304 || timer == 303 || timer == 302 || timer == 301){
					for(Player p : aplayers){
						main.title.sendTitle(p, "§dLe combat débute dans...", "§5" + (timer-300)  + "s", 20);
					}
					for(Player p : bplayers){
						main.title.sendTitle(p, "§dLe combat débute dans...", "§5" + (timer-300)  + "s", 20);
					}

				}

				if(timer == 300){
					for(Player p : aplayers){
						saveInv(p);
						p.getActivePotionEffects().clear();
						Kits.setKit(p);
						main.title.sendTitle(p, "§dDébut du combat...", "§5Téléportation !", 40);
						p.sendMessage("§dFin du combat dans 5 minutes , ou une fois qu'une équipe entière aura été éliminée");
						if(arene == 1){
							p.teleport(a1a);
						}else{
							p.teleport(a2a);
						}
					}
					for(Player p : bplayers){
						saveInv(p);
						p.getActivePotionEffects().clear();
						Kits.setKit(p);
						main.title.sendTitle(p, "§dDébut du combat...", "§5Téléportation !", 40);
						p.sendMessage("§dFin du combat dans 5 minutes , ou une fois qu'une équipe entière aura été éliminée");
						if(arene == 1){
							p.teleport(a1b);
						}else{
							p.teleport(a2b);
						}
					}
				}
				if(timer < 312){
					if(kills.get(a) == 0){
						endCombat(pl, b, a, arene, false);
					}
					if(kills.get(b) == 0){
						endCombat(pl, a, b, arene, false);
					}
				}
				if(timer == 60 || timer == 30 || timer == 10 || timer == 5 || timer == 4 || timer == 3|| timer == 2 || timer == 1){
					for(Player p : aplayers){
						p.sendMessage("§dFin du combat dans §l"+ timer +" secondes§d !");
					}
					for(Player p : bplayers){
						p.sendMessage("§dFin du combat dans §l"+ timer +" secondes§d !");
					}
				}

				if(timer == 0){
					if(kills.get(a) == kills.get(b)){
						endCombat(pl, a, b, arene, true);
					}else if(kills.get(a) > kills.get(b)){
						endCombat(pl, a, b, arene, false);						
					}else{
						endCombat(pl, b, a, arene, false);
					}

				}

			}
		};
		task = runnable.runTaskTimer(pl, 20L, 20L);

	}

	@SuppressWarnings("deprecation")
	public static void endCombat(Plugin pl, Faction win, Faction lose, int arene, boolean egalite){
		task.cancel();
		if(arene == 1){
			Arene1 = CombatStatut1.FREE;
		}
		ArrayList<Player> aplayers = getMembers(win);
		ArrayList<Player> bplayers = getMembers(lose);
		kills.remove(win);
		kills.remove(lose);
		if(aplayers != null){
			for(Player p : aplayers){
				if(playersinarena.get(p) == arene){
					playersinarena.remove(p);
				}
			}
		}
		if(bplayers != null){
			for(Player p : bplayers){
				if(playersinarena.get(p) == arene){
					playersinarena.remove(p);
				}
			}
		}
		if(egalite){
			Bukkit.broadcastMessage("§dArène 1 §5§l>>§d Le combat s'est terminé sur une égalité !");
			if(aplayers != null){
				for(Player p : aplayers){
					main.title.sendTitle(p, "§f§lEgalité !", "", 40);
					p.sendMessage("§5§lFin du combat !");
					p.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
					restoreInv(p);
					p.setHealth(p.getMaxHealth());
				}
			}
			if(bplayers != null){
				for(Player p : bplayers){
					main.title.sendTitle(p, "§f§lEgalité !", "", 40);
					p.sendMessage("§5§lFin du combat !");
					p.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
					restoreInv(p);
					p.setHealth(p.getMaxHealth());
				}
			}
		}else{
			Bukkit.broadcastMessage("§dArène 1 §5§l>>§d La faction §5"+ win.getTag() + "§d est venu à bout de la faction §5"+ lose.getTag()+" !");
			if(aplayers != null){
				for(Player p : aplayers){
					main.title.sendTitle(p, "§6§lVictoire ! :)", "", 40);
					p.sendMessage("§5§lFin du combat !");
					p.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
					restoreInv(p);
					p.setHealth(p.getMaxHealth());
				}
			}
			if(bplayers != null){
				for(Player p : bplayers){
					main.title.sendTitle(p, "§c§lDéfaite :(", "", 40);
					p.sendMessage("§5§lFin du combat !");
					p.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
					restoreInv(p);
					p.setHealth(p.getMaxHealth());
				}
			}
		}
		if(arene == 1){
			if(!attente.isEmpty()){
				Entry<Faction, Faction> entry = attente.entrySet().iterator().next();
				startCombat(pl, entry.getKey(), entry.getValue(), 1);
				attente.remove(entry.getKey());
				for(Player player : CombatManager.getMembers(entry.getKey())){
					playersinarena.put(player, 1);
				}
				for(Player player : CombatManager.getMembers(entry.getValue())){
					playersinarena.put(player, 1);
				}
				CommandCombat.resetRequest(entry.getValue());
				CommandCombat.resetRequest(entry.getKey());
			}
		}else if(arene == 2){
			if(!attente.isEmpty()){
				Entry<Faction, Faction> entry = attente.entrySet().iterator().next();
				startCombat(pl, entry.getKey(), entry.getValue(), 2);
				attente.remove(entry.getKey());
				for(Player player : CombatManager.getMembers(entry.getKey())){
					playersinarena.put(player, 1);
				}
				for(Player player : CombatManager.getMembers(entry.getValue())){
					playersinarena.put(player, 1);
				}
				CommandCombat.resetRequest(entry.getValue());
				CommandCombat.resetRequest(entry.getKey());
			}
		}

	}

	@SuppressWarnings("deprecation")
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if(e.getEntity() instanceof Player){
			Player d = (Player) e.getEntity();
			if((d.getHealth() - e.getFinalDamage()) < 1){
				if(playersinarena.containsKey(d)){
					FPlayer fpd = FPlayers.getInstance().getByPlayer(d);
					Faction fd = fpd.getFaction();
					e.setCancelled(true);
					d.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
					d.sendMessage("§c§lVous êtes mort :(");
					d.setHealth(d.getMaxHealth());
					restoreInv(d);
					kills.put(fd, kills.get(fd) - 1 );
					}
			}

		}
	}
	
	@EventHandler
	public void onDisconect(PlayerQuitEvent e){
		Player p = e.getPlayer();
		Faction fp = FPlayers.getInstance().getByPlayer(p).getFaction();
		if(playersinarena.containsKey(p)){
			kills.put(fp, kills.get(fp)-1);
		}
		if(teams.containsKey(p)){
			playersinarena.remove(p);
			restoreInv(p);
			p.teleport(Bukkit.getServer().getWorld("world").getSpawnLocation());
			teams.remove(p);
		}
	}
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		if(playersinarena.containsKey(p)){
			e.setCancelled(true);
			p.sendMessage("§cImpossible d'éxécuter une commande pendant un combat d'arène !");
		}
	}
	

}
