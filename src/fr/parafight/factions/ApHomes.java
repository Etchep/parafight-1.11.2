package fr.parafight.factions;

import java.util.HashMap;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.scheduler.BukkitRunnable;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.Role;

import fr.parafight.Main;


public class ApHomes implements CommandExecutor, Listener {
	private Main main;

	public ApHomes(Main main) {
		this.main = main;
	}
	private HashMap<String, Integer> CountdownTime =  Main.CountdownTime;
	private HashMap<String, BukkitRunnable> CountdownTask =  Main.CountdownTask;


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if ((sender instanceof Player))
		{
			Player p = (Player)sender;
			FPlayer fp = FPlayers.getInstance().getByPlayer(p);
			Faction f = fp.getFaction();
			if(cmd.getLabel().equalsIgnoreCase("setap")){
				if(fp.getRole() == Role.MODERATOR || fp.getRole() == Role.COLEADER || fp.getRole() == Role.ADMIN){
					Faction faction = null;
					Location loc = p.getLocation();
					FLocation fLoc = new FLocation(loc);
					faction = Board.getInstance().getFactionAt(fLoc);
					if(faction == f){
						int x = (int)p.getLocation().getX();
						int y = (int)p.getLocation().getY();
						int z = (int)p.getLocation().getZ();
						FactionsSql.setAp(f, x, y, z);
						p.sendMessage("�dVous venez de d�finir la position de votre Avant Poste. \n�dUtilisez �5/f ap�d pour y acc�der.");	
					}else{
						p.sendMessage("�cVous devez �tre dans votre claim pour d�finir votre Ap.");
					}
				}else{
					p.sendMessage("�cVous devez �tre au minimum mod�rateur de votre faction.");
				}
			}
			if(cmd.getLabel().equalsIgnoreCase("ap")){
				if(FactionsSql.getApY(f) != 0){
					if(p.getWorld() == Bukkit.getWorld("world")){
						CountdownTime.put(p.getName(), 5);
						p.sendMessage("�aT�l�portation dans 5 secondes, ne bougez pas.");
						CountdownTask.put(p.getName(), new BukkitRunnable() {
							public void run() {
								CountdownTime.put(p.getName(), CountdownTime.get(p.getName()) - 1);
								if (CountdownTime.get(p.getName()) == 0) {
									p.sendMessage("�aT�l�portation....");
									p.teleport(new Location(Bukkit.getWorld("world"), FactionsSql.getApX(f), FactionsSql.getApY(f), FactionsSql.getApZ(f)));	
									CountdownTime.remove(p.getName());
									CountdownTask.remove(p.getName());
									cancel();
								}
							}
						});
						CountdownTask.get(p.getName()).runTaskTimer(main, 20, 20);
					}
				}else{
					p.sendMessage("�cVous n'avez pas encore d�fini votre ap. (/f setap)");
				}
			}

		}
		return false;
	}

	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(CountdownTime.containsKey(p.getName())){
			if(e.getPlayer().getLocation().equals(e.getTo())){
				return;
			}else{
				CountdownTask.get(p.getName()).cancel();
				CountdownTask.remove(p.getName());
				CountdownTime.remove(p.getName());
				p.sendMessage("�cVous avez boug�, la t�l�portation a �t� annul�e.");
			}
		}
	}


}
