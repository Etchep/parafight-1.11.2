package fr.parafight.factions;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.Factions;
import com.massivecraft.factions.struct.Role;

import fr.parafight.Main;
import fr.parafight.factions.CombatManager.CombatStatut1;

public class CommandCombat implements CommandExecutor{

	private Main main;

	public CommandCombat(Main main) {
		this.main = main;
	}

	public static HashMap<Player, String> teams = new HashMap<Player, String>();
	public static HashMap<String, String> demandes = new HashMap<String, String>();
	public static HashMap<Player, Integer> playersinarena = new HashMap<Player, Integer>();
	public static HashMap<Faction, Faction> attente = new LinkedHashMap<Faction, Faction>();


	public static void sendRequest(Faction requester, Faction f){
		demandes.put(requester.getTag(), f.getTag());
	}

	public static void resetRequest(Faction requester){
		demandes.remove(requester.getTag());
	}

	public static boolean hasRequest(Faction f, Faction requester){
		return demandes.containsKey(requester.getTag()) && demandes.get(requester.getTag()) == f.getTag();
	}

	public static int teamMembers(Faction f){
		int i = 0;
		for(Player p : teams.keySet()){
			if(teams.get(p).equals(f.getTag())){
				i++;
			}
		}
		return i;
	}



	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if ((sender instanceof Player))
		{  
			Player p = (Player)sender;
			FPlayer fp = FPlayers.getInstance().getByPlayer(p);
			Faction pfaction = fp.getFaction();
			if(fp.getRole() == Role.MODERATOR || fp.getRole() == Role.COLEADER || fp.getRole() == Role.ADMIN){
				if (args.length == 0)
				{
					p.sendMessage("§6§lCombats Factions :");
					p.sendMessage("§61- Créez votre équipe de combattants!");
					p.sendMessage("§6> §e/combat team add <joueur> | §7Pour ajouter les joueurs qui participerons au combat.");
					p.sendMessage("§6> §e/combat team add <joueur> | §7Pour retirer actuellement dans le groupe de combattants.");
					p.sendMessage("§6> §e/combat team | §7Pour avoir la liste des joueurs dans le groupe de combattants.");
					p.sendMessage("§62- Provoquez une faction en duel !");
					p.sendMessage("§6> §e/combat <Faction> | §7Provoquez la Faction de votre choix.");
					p.sendMessage("§8§lInformations :");
					p.sendMessage("§7- Un combat entre Factions se déroule à participants par équipe, et stuff égale. "
							+ "Le nombre de participants au combat est défini par le nombre de combattants dans le groupe de la faction qui provoque le duel.");
					p.sendMessage("§7- Les combats n'affectent pas le classement Factions.");
					p.sendMessage("§7- La déconnexion en combat engendre la discalification du joueur en question.");
					p.sendMessage("§7- Un combat peut durer 5 minutes maximum.");
				}
				if(args.length >= 1){
					switch(args[0].toLowerCase()){
					case "team":
						if(args.length == 1){
							p.sendMessage("§ePour participer engager un combat faction, vous devez d'abord choisir les participants parmis les membres votre faction.");
							p.sendMessage("§e§l>> §6/combat team add <Joueur> ");
							p.sendMessage("§7Joueurs dans le groupe :");
							for(Player play : teams.keySet()){
								if(teams.get(play).equals(pfaction.getTag())){
									p.sendMessage("§7-" + play.getName());
								}
							}
						}
						if(args.length == 2){
							if(args[1].toLowerCase().equals("add")){
								p.sendMessage("§7/combat team add <Joueur>");
							}
							if(args[1].toLowerCase().equals("debug")){
								if(p.hasPermission("arene.debug")){
								CombatManager.Arene1 = CombatStatut1.FREE;
								p.sendMessage("§aL'arène est désormais libre.");
								}
							}	 
						}
						if(args.length == 3){
							Player padd = Bukkit.getPlayer(args[2]);
							if(padd == null){
								p.sendMessage("§cLe joueur "+ args[2] +" n'est pas en ligne");
							}else if(fp.getFaction().getOnlinePlayers().contains(padd)){
								if(args[1].equalsIgnoreCase("add")){
									if(!teams.containsKey(padd)){
										teams.put(padd, fp.getFaction().getTag());
										p.sendMessage("§8Le joueur §e" + padd.getName() + "§8 a été ajouté au groupe de combat !");
										p.sendMessage("§8Joueur(s) actuellement dans le groupe :");
										for(Player play : teams.keySet()){
											if(teams.get(play).equals(pfaction.getTag())){
												p.sendMessage("§7-" + play.getName());
											}
										}
										if(teamMembers(pfaction) == 0){
											p.sendMessage("§cAucun");
										}
									}else{
										p.sendMessage("§cCe joueur est déjà dans la team.");
									}
								}
								if(args[1].equalsIgnoreCase("remove")){
									teams.remove(padd);
									p.sendMessage("§7Le joueur " + padd.getName() + " a été retiré du groupe de combat !");
									p.sendMessage("§8Joueur(s) actuellement dans le groupe :");
									for(Player play : teams.keySet()){
										if(teams.get(play).equals(pfaction.getTag())){
											p.sendMessage("§7-" + play.getName());
										}
									}
									if(teamMembers(pfaction) == 0){
										p.sendMessage("§cAucun");
									}
								}
							}else{
								p.sendMessage("Le joueur n'est pas dans votre faction.");
							}
						}
						break;
					case "accept":
						if(args.length == 1){
							p.sendMessage("§7/combat accept <Faction>");
						}
						if(args.length == 2){
							Faction target = Factions.getInstance().getByTag(args[1]);
							if(target != null){
								if(hasRequest(target, pfaction) || hasRequest(pfaction, target)){
									if(teamMembers(pfaction) != 0){
										if(teamMembers(pfaction) == teamMembers(target)){
											if(CombatManager.Arene1 == CombatStatut1.FREE){
												for(Player player : CombatManager.getMembers(target)){
													playersinarena.put(player, 1);
												}
												for(Player player : CombatManager.getMembers(pfaction)){
													playersinarena.put(player, 1);
												}
												CombatManager.startCombat(main, pfaction, target, 1);
												
												resetRequest(pfaction);
												resetRequest(target);
												/*}else if(CombatManager.Arene2 == CombatStatut2.FREE){
												for(Player player :CombatManager.getMembers(target)){
													playersinarena.put(player, 2);
												}
												for(Player player :CombatManager.getMembers(pfaction)){
													playersinarena.put(player, 2);
												}
												CombatManager.startCombat(main, pfaction, target, 2);
												resetRequest(pfaction);
												resetRequest(target);
												/* */
											}else{
												for(Player player : target.getOnlinePlayers()){
													attente.put(pfaction, target);
													player.sendMessage("§eLes arènes sont occupées, vous avez été placés en liste d'attente.");
													player.sendMessage("§eLe combat commençera dès qu'une arène se sera libérée.");
												}
												for(Player player : pfaction.getOnlinePlayers()){
													attente.put(pfaction, target);
													player.sendMessage("§eLes arènes sont occupées, vous avez été placés en liste d'attente.");
													player.sendMessage("§eLe combat commençera dès qu'une arène se sera libérée.");
												}
											}


										}else{
											p.sendMessage("§cC'est un combat à §4" + teamMembers(target)+" contre " + teamMembers(target) + "§c.");
											p.sendMessage("§cAjustez votre nombre de participants à §4" + teamMembers(target) + " avec les commandes ci-dessous");
											p.sendMessage("§cRetirer un joueur : §4§l/combat team remove <joueur>");
											p.sendMessage("§cAjouter un joueur : §4§l/combat team add <joueur>");
										}
									}else{
										p.sendMessage("§cVous n'avez pas crée votre équipe ! (§4/combat team add <joueur>§c)");
									}
								}else{
									p.sendMessage("Vous n'avez pas de demande de combat de cette faction");
								}
							}else{
								p.sendMessage("La faction " + args[1] + " n'a pas été trouvée.");
							}
						}
						break;
					default:
						Faction target = Factions.getInstance().getByTag(args[0]);
						if(target != null){
							if(target != pfaction){
								if(!hasRequest(target, pfaction)){
									if(teamMembers(pfaction) > 0){
										if(target.getOnlinePlayers().size() >= teamMembers(pfaction)){
											sendRequest(pfaction, target);
											for(Player player : target.getOnlinePlayers()){
												player.sendMessage("§eLa faction §6" + pfaction.getTag() + "§e vous provoque en duel §6" + teamMembers(pfaction)+"vs" + teamMembers(pfaction) + "§e !");
												player.sendMessage("§e§l1 §8§l➥ §eCréez votre équipe de §6§l"+ teamMembers(pfaction)+ "§e personnes avec la commande §6/combat team add <joueur>");
												player.sendMessage("§e§l2 §8§l➥ §6/combat accept "+ pfaction.getTag() + "§e pour accepter le duel !");
											}
											p.sendMessage("§dVous avez envoyé une demande de combat à " + target.getTag() + ".");
										}else{
											p.sendMessage("§cLe nombre de joueurs en ligne dans la faction " +target.getTag()+ " est de §4" + target.getOnlinePlayers().size() + " joueurs §c.");
											p.sendMessage("§cRéduisez votre nombre de participants à §4§l" + target.getOnlinePlayers().size()+ "§c pour pouvoir les défier.");
											p.sendMessage("§cUtilisez la commande : §4§l/combat team remove <joueur>");
										}
									}else{
										p.sendMessage("§cVous n'avez pas crée votre équipe ! (§4/combat team add <joueur>§c)");
									}
								}else{
									p.sendMessage("§cVous avez déjà envoyé une demande à cette faction !");
								}
							}else{
								p.sendMessage("§cVous ne pouvez pas vous combattre vous même... Réfléchissez un peu !");
							}
						}else{
							p.sendMessage("La faction " + args[0] + " n'a pas été trouvée.");
						}
						break;
					}
				}
			}else{ 
				p.sendMessage("§cIl faut être au minimum modérateur de la faction pour organiser un combat.");
			}

		}	
		return false;
	}

}
