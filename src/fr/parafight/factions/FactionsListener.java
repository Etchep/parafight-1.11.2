package fr.parafight.factions;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.event.FPlayerJoinEvent;
import com.massivecraft.factions.event.FPlayerLeaveEvent;
import com.massivecraft.factions.event.FactionDisbandEvent;
import com.massivecraft.factions.event.FactionRenameEvent;
import com.massivecraft.factions.event.FPlayerLeaveEvent.PlayerLeaveReason;

public class FactionsListener implements Listener{

	@EventHandler
	public void onFCreate(FPlayerJoinEvent e){
		FactionsSql.addFaction(e.getFaction());
	}
	@EventHandler
	public void onFDisband(FactionDisbandEvent e){
		FactionsSql.remFaction(e.getFaction());
	}

	@EventHandler
	public void onFNameChange(FactionRenameEvent e){
		FactionsSql.nameChange(e.getFaction());
	}
	@EventHandler
	public void onFLeave(FPlayerLeaveEvent e){
		if(e.getReason() == PlayerLeaveReason.KICKED || e.getReason() == PlayerLeaveReason.BANNED || e.getReason() == PlayerLeaveReason.JOINOTHER || e.getReason() == PlayerLeaveReason.LEAVE){
			if(e.getfPlayer().getPlayer() != null && e.getfPlayer().getPlayer().isOnline()){
				e.getfPlayer().getPlayer().teleport(Bukkit.getWorld("world").getSpawnLocation());
				e.getfPlayer().getPlayer().teleport(Bukkit.getWorld("world").getSpawnLocation());	
			}
		}
	}
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		FPlayer fpd = FPlayers.getInstance().getByPlayer(p);
		Faction fd = fpd.getFaction();

		if(e.getMessage().contains("f sethome") == false){
			if(e.getMessage().contains("sethome")){
				Faction faction = null;
				Location loc = p.getLocation();
				FLocation fLoc = new FLocation(loc);
				faction = Board.getInstance().getFactionAt(fLoc);
				if(faction.equals(fd) || faction.isWilderness() || faction.isWarZone() || faction.isSafeZone()){
					return;
				}else{
					e.setCancelled(true);
					p.sendMessage("�cVous ne pouvez pas cr�er de r�sidences dans le claim d'une autre faction.");
				}
			}
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		Player d = e.getEntity().getPlayer();
		FPlayer fpd = FPlayers.getInstance().getByPlayer(d);
		Faction fd = fpd.getFaction();

		if(e.getEntity().getKiller() instanceof Player){

			Player k = e.getEntity().getKiller().getPlayer();
			FPlayer fpk = FPlayers.getInstance().getByPlayer(k);
			Faction fk = fpk.getFaction();

			if(fpk.hasFaction()){
				if(fpd.hasFaction()){
					FactionsSql.setFPoints(fk, FactionsSql.getFPoints(fk) + 1);
				}
			}
			if(fpd.hasFaction()){
				if(fpk.hasFaction()){
					FactionsSql.setFPoints(fk, FactionsSql.getFPoints(fk) + ((int)(FactionsSql.getFPoints(fd)*0.05)));
					FactionsSql.setFPoints(fd, FactionsSql.getFPoints(fd) - ((int)(FactionsSql.getFPoints(fd)*0.05)));
					if(FactionsSql.getFPoints(fd) > 1){
						FactionsSql.setFPoints(fd, FactionsSql.getFPoints(fd) - 1);
					}else{
						FactionsSql.setFPoints(fd, 0);
					}
				}
			}



		}

	}

}
