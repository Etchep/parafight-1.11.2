package fr.parafight.moderation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractAtEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;

public class SpectatorMod implements CommandExecutor, Listener{

	private ArrayList<String> Spectators = Main.Spectators;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player){
			Player p = (Player)sender;
			if(p.hasPermission("parafight.spectate")){
				if(isSpectator(p)){
					removeSpectatorMod(p);
				}else{
					setSpectatorMod(p);
				}
			}
		}
		return false;
	}

	public ItemStack getItem(Material material, String name, int quantity, int damage, String desc){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		it.setItemMeta(itM);
		return it;
	}

	public void setSpectatorMod(Player player){
		Spectators.add(player.getName());
		player.setGameMode(GameMode.CREATIVE);
		Inventory inv = player.getInventory();
		inv.clear();
		inv.setItem(0, getItem(Material.NETHER_STAR, "�6Vanish", 1, 0, "�eActiver/d�sactiver le mode vanish."));
		inv.setItem(1, getItem(Material.DIAMOND_SWORD, "�6Tuer le joueur", 1, 0, "�eClic sur le joueur pour le tuer."));
		inv.setItem(2, getItem(Material.PACKED_ICE, "�6Freeze le joueur", 1, 0, "�eClic droit sur le joueur pour le freeze/unfreeze"));
		inv.setItem(3, getItem(Material.CHEST, "�6�lInvSee", 1, 0, "�eClic droit sur le joueur pour voir son inventaire."));
		inv.setItem(4, getItem(Material.EMERALD, "�6Switch Gamemode", 1, 0, "�cChanger de gm (Creatif/Spectateur) "));
		inv.setItem(6, getItem(Material.ENDER_PEARL, "�6T�l�portation al�atoire", 1, 0, "�eClic droit pour te t�l�porter � un joueur."));
		inv.setItem(8, getItem(Material.REDSTONE_BLOCK, "�6Menu Sanctions", 1, 0, "�eClic sur le joueur pour ouvrir le menu des sanctions."));

	}

	public void removeSpectatorMod(Player player){
		Spectators.remove(player.getName());
		player.getInventory().clear();
		player.setGameMode(GameMode.SURVIVAL);
	}
	public boolean isSpectator(Player player){
		return Spectators.contains(player.getName());
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent e)
	{
		Player p = e.getPlayer();

		if(isSpectator(p)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void getDrop(PlayerPickupItemEvent e)
	{
		Player p = e.getPlayer();
		if(isSpectator(p)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void Clickevent(InventoryClickEvent e)
	{
		Player p = (Player)e.getWhoClicked();
		if(isSpectator(p)){
			e.setCancelled(true);
		}
	}

	@EventHandler
	public void onPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		if(isSpectator(p)){
			e.setCancelled(true);
		}
	}
	@EventHandler
	public void interact(PlayerInteractEvent e)
	{
		Player p = e.getPlayer();
		if(isSpectator(p)){

			if (p.getInventory().getItemInMainHand().getType() == Material.ENDER_PEARL) {
				ArrayList<Player> list = new ArrayList<Player>();

				for (Player pl : Bukkit.getOnlinePlayers()) {

					if ((!isSpectator(pl)) || (!pl.hasPermission("admin"))) {
						list.add(pl);
					}
				}

				int random = new Random().nextInt(list.size());

				p.teleport((Entity)list.get(random));
				list.clear();
			}
			if (p.getInventory().getItemInMainHand().getType() == Material.EMERALD) {
				if(p.getGameMode() == GameMode.CREATIVE){
					p.setGameMode(GameMode.SPECTATOR);
				}else{
					p.setGameMode(GameMode.CREATIVE);
				}
			}
			if(p.getInventory().getItemInMainHand().getType() == Material.NETHER_STAR){
				p.chat("/vanish");
			}
		}
	}
	  @EventHandler
	  public void onInteract(PlayerInteractAtEntityEvent e)
	  {
	    if ((e.getRightClicked() instanceof Player)) {
	      Player pl = (Player)e.getRightClicked();
	      Player p = e.getPlayer();


	      if (p.hasPermission("admin")) {
	        e.setCancelled(true);
	      }

	      if (isSpectator(p)){
		        if (p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_SWORD) {
			          pl.setHealth(0.0D);
			          p.sendMessage("Tu viens de d�capiter un joueur !");
			          pl.sendMessage("Vous avez �t� tu� par un membre de la mod�ration.");
			        }
		        if (p.getInventory().getItemInMainHand().getType() == Material.PACKED_ICE) {
			          p.chat("/freeze " + pl.getName());
			        }
		        if (p.getInventory().getItemInMainHand().getType() == Material.CHEST) {
			          p.openInventory(pl.getInventory());
			        }
		        if (p.getInventory().getItemInMainHand().getType() == Material.REDSTONE_BLOCK) {
			          p.chat("/as " + pl.getName());
			        }
	      }
	    }
	  }

}
