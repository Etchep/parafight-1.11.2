package fr.parafight.moderation;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import fr.parafight.Main;
import fr.parafight.utils.PlayerData;


public class Freeze implements CommandExecutor, Listener{
	
	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender.hasPermission("parafight.freeze")){
			if(args.length == 0){
				sender.sendMessage("/freeze joueur");
			}
			if (args.length == 1) {
				Player target = Bukkit.getPlayer(args[0]);
				if (target != null) {
					if(PlayerMap.get(target.getName()).isFrozen() == true){
						PlayerMap.get(target.getName()).setFrozen(false);
						target.sendMessage("�cVous n'�tes plus freeze.");
						sender.sendMessage("�aVous avez unfreeze " + target.getName() + ".");
					}else{
						PlayerMap.get(target.getName()).setFrozen(true);
						sender.sendMessage("�aVous avez freeze " + target.getName() + ".");
						target.sendMessage("�cVous avez �t� freeze par un membre du staff.");
						target.sendMessage("�c-Ne vous d�connectez pas.");
						target.sendMessage("�c-Suivez ses instructions.");
					}
				}else{
					sender.sendMessage("�cLe joueur n'est pas en ligne");
				}
				return true;
			}
		}
		return false;
	}
	
	@EventHandler
	public void onMove(PlayerMoveEvent e){
		Player p = e.getPlayer();
		if(e.getPlayer().getLocation().equals(e.getTo())){
			return;
		}else
		if(PlayerMap.get(p.getName()).isFrozen() == true){
			e.setTo(e.getFrom());
		}
	}
	@EventHandler (priority = EventPriority.LOWEST)
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		if(PlayerMap.get(p.getName()).isFrozen() == true){
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "tempban " + p.getName() + "1 month Refus de v�rification");
		}
	}
	@EventHandler
	public void onDamage(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof Player){
			Player damager = (Player)e.getDamager();
			if(PlayerMap.get(damager.getName()).isFrozen()){
				e.setCancelled(true);
			}
		}
		if(e.getEntity() instanceof Player){
			Player damaged = (Player)e.getEntity();
			if(PlayerMap.get(damaged.getName()).isFrozen()){
				e.setCancelled(true);
			}
		}
	}
	

}
