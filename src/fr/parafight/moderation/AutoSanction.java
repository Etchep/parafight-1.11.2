package fr.parafight.moderation;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;


public class AutoSanction implements CommandExecutor, Listener{
	
	HashMap<String, String> ASMap = Main.ASMap;
	
	public Inventory GUI = Bukkit.createInventory(null, 9, "�6�lMenu des sanctions");

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String msg, String[] args) {

		if (sender instanceof Player) {
			if(sender.hasPermission("parafight.autosanction")){
				Player p = (Player) sender;

				if (args.length == 0) {
					p.sendMessage(ChatColor.GRAY + "/as <joueur>");
				} else if (args.length == 1) {
						Player target = Bukkit.getPlayer(args[0]);
						if (target != null) {
							if (ASMap.containsKey(p)){
								ASMap.remove(p);
								}
							ASMap.put(p.getName(), target.getName());
							GUI.setItem(0, createItem(Material.WOOL, "�e�lArticle�4.5", 1, 13, "�eLes messages ou sujets ayant pour but de flood, de spam ou de harceler.."));
							GUI.setItem(1, createItem(Material.WOOL, "�e�lArticle 3.3", 1, 13, "�eProvocations menants � la haine "));
							GUI.setItem(2, createItem(Material.WOOL, "�e�lArticle 4.3", 1, 13, "�eTout message comportant des insultes ou mots visant � blesser"));
							GUI.setItem(3, createItem(Material.WOOL, "�e�lArticle 3.8", 1, 13, "�eLe TP-Kill* (*Tuer un joueur qui ne vous a pas attaqu�, suite � sa t�l�portation."));
							GUI.setItem(6, createItem(Material.WOOL, "�e�lArticle 3.4", 1, 14, "�eToutes menaces directes ou indirectes envers un joueur ou un groupe de joueurs"));
							GUI.setItem(7, createItem(Material.WOOL, "�e�lArticle�3.6", 1, 14, "�eToutes sortes de cheat."));
							GUI.setItem(8, createItem(Material.WOOL, "�e�lArticle�4.6", 1, 14, "�ePublicit�e pour un autre serveur."));
							p.openInventory(GUI);
							
						}else{
							p.sendMessage("�cLe joueur n'est pas en ligne");
						}
						return true;
				}
			}

		}
		return false;
	}
	
	public ItemStack createItem(Material material, String name, int quantity, int damage, String desc){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		it.setItemMeta(itM);
		return it;
	}
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();
		String cible = ASMap.get(p.getName());

		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(GUI.getName())) {
			switch (e.getSlot()) {
			case 0:
				p.chat("/tempmute " + cible + " 30 minute Non respect de l'article 4.5");
				break;
			case 1:
				p.chat("/tempmute " + cible + " 30 minute Non respect de l'article 3.3");
				break;
			case 2:
				p.chat("/tempmute " + cible + " 1 hour Non respect de l'article 4.3");
				break;
			case 3:
				p.chat("/tempmute " + cible + " 1 day Non respect de l'article 3.8");
				break;
			case 6:
				p.chat("/tempban " + cible + " 1 month Non respect de l'article 3.4");
				break;
			case 7:
				p.chat("/tempban " + cible + " 1 year Non respect de l'article 3.6");
				break;
			case 8:
				p.chat("/tempban " + cible + " 1 year Non respect de l'article 4.6");
				Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "banip " + cible + "Non respect de l'article�4.6" );
				break;
			default : 
				break;
			}
			e.setCancelled(true);
			p.closeInventory();
		}
	}

}
