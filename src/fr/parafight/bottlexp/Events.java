package fr.parafight.bottlexp;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

public class Events implements Listener {
	  
	  public static double getExpFromLevel(double level) {
			if (level > 30) {
				return (double) (4.5 * level * level - 162.5 * level + 2220);
			}
			if (level > 15) {
				return (double) (2.5 * level * level - 40.5 * level + 360);
			}
			return level * level + 6 * level;
		}
		

		
  @EventHandler
  public void onBottleUse(PlayerInteractEvent e) {
    if ((e.getAction() == Action.RIGHT_CLICK_AIR) || (e.getAction() == Action.RIGHT_CLICK_BLOCK)) {
      ItemStack is = e.getItem();
      Player p = e.getPlayer();
      if (is == null) {
        return;
      }
      
      if ((e.getItem().getType() == Material.EXP_BOTTLE) && (e.getItem().getItemMeta().getDisplayName() != null) && 
        (e.getItem().getItemMeta().getDisplayName().startsWith("�d�lNiveaux : �5�l"))) {
        e.setCancelled(true);
        String str = e.getItem().getItemMeta().getDisplayName().replace("�d�lNiveaux : �5�l", "");
        double level = Double.parseDouble(str);
        if (level <= 0) {
          return;
        }
        double amountxp = getExpFromLevel(level);
        p.giveExp((int) amountxp);
        if (e.getPlayer().getInventory().getItemInMainHand().getAmount() > 1) {
          e.getPlayer().getInventory().getItemInMainHand().setAmount(e.getPlayer().getInventory().getItemInMainHand().getAmount() - 1);
          e.getPlayer().getInventory().setItemInMainHand(e.getPlayer().getInventory().getItemInMainHand());
        } else {
          e.getPlayer().getInventory().setItemInMainHand(new ItemStack(Material.AIR));
        }
      }
    }
  }
}