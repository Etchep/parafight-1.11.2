package fr.parafight.bottlexp;

import java.util.Arrays;
import org.bukkit.Material;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class BottlexpCmd
  implements CommandExecutor
{

  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
    Player p = (Player)sender;
    if (label.equalsIgnoreCase("bottlexp")) {
      if (args.length == 0)
      {
        int count = p.getLevel();
        float xp = p.getExp();
        if (count != 0) {
          p.setExp(0.0F);
          p.setLevel(0);
          ItemStack stack = new ItemStack(Material.EXP_BOTTLE);
          ItemMeta meta = stack.getItemMeta();
          meta.setDisplayName("�d�lNiveaux : �5�l" + (count + xp));
          meta.setLore(Arrays.asList(new String[] { "�aClic droit pour r�cup�rer" }));
          stack.setItemMeta(meta);
          p.getInventory().addItem(new ItemStack[] { stack });
        } else {
          p.sendMessage("�cVous n'avez plus de lvl d'xp !");
        }
      }
    }

    return false;
  }
}
