package fr.parafight;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.plugin.PluginManager;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitScheduler;

import fr.parafight.Listeners.CommandPreprocessEvent;
import fr.parafight.Listeners.JoinQuit;
import fr.parafight.Listeners.MobsKillMoney;
import fr.parafight.Listeners.NoHitCooldown;
import fr.parafight.Listeners.PotionsBlock;
import fr.parafight.Listeners.WitherEvents;
import fr.parafight.Meteors.MeteorAutoStart;
import fr.parafight.Meteors.MeteorData;
import fr.parafight.banque.AddRem;
import fr.parafight.banque.BankMenu;
import fr.parafight.banque.BankTop;
import fr.parafight.banque.CommandBank;
import fr.parafight.banque.MoneyLoot;
import fr.parafight.chat.AnnounceTask;
import fr.parafight.factions.CombatManager;
import fr.parafight.newrecipes.FireballLauncher;
import fr.parafight.newrecipes.PainEffects;
import fr.parafight.newrecipes.Pioche3x3;
import fr.parafight.planning.Autostart;
import fr.parafight.planning.PlanningGui;
import fr.parafight.sql.DbManager;
import fr.parafight.sql.SqlConnection;
import fr.parafight.utils.PlayerData;
import net.milkbowl.vault.economy.Economy;

public class Main extends JavaPlugin implements Listener {


	
	public Titles title = new Titles();
	public SqlConnection sql;


	public static HashMap<String, PlayerData> PlayerMap;
	public static HashMap<Integer, MeteorData> MeteorHash;
	public static ConcurrentHashMap<Integer, BukkitRunnable> MeteorTask;
	public static HashMap<String, Integer> CountdownTime;
	public static HashMap<String, BukkitRunnable> CountdownTask;
	public static ArrayList<String> lorefireball = new ArrayList<String>();
	public static ArrayList<String> lorepioche = new ArrayList<String>();
	public static HashMap<String, String> ASMap;
	public static ArrayList<String> Spectators;
	public static ArrayList<String> rtpList;

	private static Main instance;
	
    public static Main getInstance() {
        return instance;
    }

	public void onEnable() {
		super.onEnable();
		instance = this;
		PluginManager pm = Bukkit.getPluginManager();
		DbManager.initAllDbConnections();
		CountdownTime = new HashMap<String, Integer>();
		CountdownTask = new HashMap<String, BukkitRunnable>();
		MeteorTask = new ConcurrentHashMap<Integer, BukkitRunnable>();
		MeteorHash = new HashMap<Integer, MeteorData>();
		PlayerMap = new HashMap<String, PlayerData>();
		ASMap = new HashMap<String, String>();
		Spectators = new ArrayList<String>();
		rtpList = new ArrayList<String>();
		Bukkit.getServer().addRecipe(PainEffects.getpainforce());
		Bukkit.getServer().addRecipe(PainEffects.getpainspeed());
		lorefireball.add(ChatColor.RED + "Allez ! Clic !");
		FireballLauncher.getFireball().register();
		lorepioche.add(ChatColor.YELLOW + "Pioche 3x3");
		Bukkit.getServer().addRecipe(Pioche3x3.getPioche3x3());
		CombatManager.Arene1 = CombatManager.CombatStatut1.FREE;
		CombatManager.Arene2 = CombatManager.CombatStatut2.FREE;
		pm.registerEvents(this, this);
		pm.registerEvents(new JoinQuit(this), this);
		pm.registerEvents(new fr.parafight.chat.ChatMentions(), this);
		pm.registerEvents(new CommandPreprocessEvent(), this);
		pm.registerEvents(new MobsKillMoney(), this);
		pm.registerEvents(new PotionsBlock(), this);
		pm.registerEvents(new fr.parafight.chat.HoverChat(), this);
		pm.registerEvents(new fr.parafight.skills.SkillsGui(), this);
		pm.registerEvents(new fr.parafight.skills.SkillsEvents(), this);
		pm.registerEvents(new fr.parafight.tablist.tablist(this), this);
		pm.registerEvents(new fr.parafight.gui.WarpsGui(), this);
		pm.registerEvents(new fr.parafight.boutique.Divers(), this);
		pm.registerEvents(new fr.parafight.boutique.Kits(), this);
		pm.registerEvents(new fr.parafight.boutique.Grades(), this);
		pm.registerEvents(new fr.parafight.boutique.Effects(), this);
		pm.registerEvents(new fr.parafight.boutique.Keys(), this);
		pm.registerEvents(new fr.parafight.boutique.Menu(), this);
		pm.registerEvents(new fr.parafight.boutique.Points(), this);
		pm.registerEvents(new fr.parafight.newrecipes.PainEffects(), this);
		pm.registerEvents(new fr.parafight.newrecipes.Pioche3x3(), this);
		pm.registerEvents(new fr.parafight.newrecipes.FireballLauncher(), this);
		pm.registerEvents(new fr.parafight.bottlexp.Events(), this);
		pm.registerEvents(new fr.parafight.factions.FactionsListener(), this);
		pm.registerEvents(new fr.parafight.factions.CombatManager(this), this);
		pm.registerEvents(new fr.parafight.Listeners.TNTModif(), this);
		pm.registerEvents(new fr.parafight.Listeners.NoRepairPickaxe(), this);
		pm.registerEvents(new fr.parafight.factions.ApHomes(this), this);
		pm.registerEvents(new fr.parafight.Meteors.MeteorListeners(), this);
		pm.registerEvents(new fr.parafight.ctf.GameManager(this), this);
		pm.registerEvents(new fr.parafight.moderation.Freeze(), this);
		pm.registerEvents(new fr.parafight.moderation.AutoSanction(), this);
		pm.registerEvents(new fr.parafight.moderation.SpectatorMod(), this);
		pm.registerEvents(new fr.parafight.rtp.RandomTp(), this);
		pm.registerEvents(new NoHitCooldown(this), this);
		pm.registerEvents(new AddRem(), this);
		pm.registerEvents(new BankMenu(), this);
		pm.registerEvents(new MoneyLoot(), this);
		pm.registerEvents(new WitherEvents(), this);
		pm.registerEvents(new PlanningGui(), this);

		getCommand("ts").setExecutor(new Commands());
		getCommand("site").setExecutor(new Commands());
		getCommand("info").setExecutor(new Commands());
		getCommand("ping").setExecutor(new Commands());
		getCommand("annonce").setExecutor(new Commands());
		getCommand("Discord").setExecutor(new Commands());
		getCommand("ytb").setExecutor(new Commands());
		getCommand("youtube").setExecutor(new Commands());
		getCommand("map").setExecutor(new Commands());
		getCommand("competences").setExecutor(new fr.parafight.skills.SkillsCmd());
		getCommand("skills").setExecutor(new fr.parafight.skills.SkillsCmd());
		getCommand("chat").setExecutor(new fr.parafight.chat.ChatManageCmd());
		getCommand("pc").setExecutor(new fr.parafight.inventories.CmdCoffre());
		getCommand("boutique").setExecutor(new fr.parafight.boutique.CommandBoutique());
		getCommand("bottlexp").setExecutor(new fr.parafight.bottlexp.BottlexpCmd());
		getCommand("classement").setExecutor(new fr.parafight.factions.CommandClassement());
		getCommand("combat").setExecutor(new fr.parafight.factions.CommandCombat(this));
		getCommand("points").setExecutor(new fr.parafight.factions.CommandPoints());
		getCommand("setap").setExecutor(new fr.parafight.factions.ApHomes(this));
		getCommand("ap").setExecutor(new fr.parafight.factions.ApHomes(this));
		getCommand("meteor").setExecutor(new fr.parafight.Meteors.CommandMeteorites(this));
		getCommand("launchmeteor").setExecutor(new fr.parafight.Meteors.CommandMeteorites(this));
		getCommand("ctf").setExecutor(new fr.parafight.ctf.CTFCommand());
		getCommand("freeze").setExecutor(new fr.parafight.moderation.Freeze());
		getCommand("as").setExecutor(new fr.parafight.moderation.AutoSanction());
		getCommand("mod").setExecutor(new fr.parafight.moderation.SpectatorMod());
		getCommand("rtp").setExecutor(new fr.parafight.rtp.RandomTp());
		getCommand("bank").setExecutor(new CommandBank());
		getCommand("banque").setExecutor(new CommandBank());
		getCommand("planning").setExecutor(new PlanningGui());
		getCommand("baltop").setExecutor(new BankTop());
		getCommand("tnt").setExecutor(new Commands());
		getCommand("explosions").setExecutor(new Commands());

		setupEconomy();
		if (!setupEconomy()) {
			getLogger().severe(
					String.format("[%s] - Disabled due to no Vault dependency found!", getDescription().getName()));
			getServer().getPluginManager().disablePlugin(this);
			return;
		}
		Bukkit.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
			
			@Override
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()){
					PlayerData data = PlayerMap.get(p.getName());
					SqlConnection.updateAll(p, data.getMineur(), data.getFarmeur(), data.getAssaillant(), data.getPecheur(), data.getChasseur(), data.getPrivateChest(), data.getBankBalance());
				}	
			}
		}, 144000L, 144000L);

		AnnounceTask task = new AnnounceTask();
		task.runTaskTimer(this, 20, 4000);
		MeteorAutoStart task2 = new MeteorAutoStart(this);
		task2.runTaskTimer(this, 72000, 72000);
		Autostart autostart = new Autostart();
		autostart.runTaskTimer(this, 0L, 20L);
	}

	public static Economy econ = null;

	public void onDisable() {
		super.onDisable();
		for(Player p : Bukkit.getOnlinePlayers()){
			PlayerData data = PlayerMap.get(p.getName());
			SqlConnection.updateAll(p, data.getMineur(), data.getFarmeur(), data.getAssaillant(), data.getPecheur(), data.getChasseur(), data.getPrivateChest(), data.getBankBalance());
		}	

		DbManager.closeAllDbConnections();
	}

	private boolean setupEconomy() {
		if (getServer().getPluginManager().getPlugin("Vault") == null) {
			return false;
		}
		RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
		if (rsp == null) {
			return false;
		}
		econ = rsp.getProvider();
		return econ != null;
	}

	public static Economy getEcon() {
		return econ;
	}
	@EventHandler
	public void alSpawnear(CreatureSpawnEvent e)
	{
		int limite = e.getLocation().getChunk().getEntities().length;
		if (limite >= 150)
		{
			e.setCancelled(true);
		}
	}

}
