package fr.parafight.Meteors;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.parafight.Main;
import fr.parafight.utils.Meteorites;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class CommandMeteorites implements CommandExecutor{
	
	private Main main;

	public CommandMeteorites(Main main) {
		this.main = main;
	}

	public static HashMap<Integer, MeteorData> MeteorHash = Main.MeteorHash;
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		
		if(cmd.getName().equalsIgnoreCase("launchmeteor")){
			Player p = (Player)sender;
			if(p.hasPermission("parafight.meteors")){
				if(args.length == 0){
					Meteorites.MeteorPrepare(main,1);
				}else if(args.length == 1){
					Meteorites.MeteorPrepare(main,Integer.parseInt(args[0]));
				}
				
			}
		}
		
		if(cmd.getName().equalsIgnoreCase("meteor")){
			Player p = (Player)sender;
			if(args.length == 0){
				if(MeteorHash.isEmpty() == false){
					for(int i: MeteorHash.keySet()){
						int id = i;
						String s = "�bMeteorite " + id + " : �bX = �9�l"+ MeteorHash.get(id).getTo().getBlockX()+" �bY = �9�l" + MeteorHash.get(id).getTo().getBlockY()+" �bZ = �9�l"+ MeteorHash.get(id).getTo().getBlockZ()+ "�b.";
						TextComponent ss = new TextComponent(s);
						ss.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�9Clic pour conna�te le loot !").create()));
						ss.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/meteor " + id));
						p.spigot().sendMessage(ss);
					}
				}else{
					p.sendMessage("Aucune m�t�orite � l'heure actuelle.");
				}

			}else if(args.length == 1){
				int id = Integer.parseInt(args[0]);
				p.openInventory(MeteorHash.get(id).getInv());
			}
			
		}
		
		return false;
	}
	

}
