package fr.parafight.Meteors;

import org.bukkit.Location;
import org.bukkit.inventory.Inventory;

import fr.parafight.utils.Meteorites;

public class MeteorData {
	
	private int id;
	private Location from;
	private Location to;
	private Inventory inv;
	private Location chestloc;
	
	public MeteorData(int id, Location from, Inventory inventory){
		this.id = id;
		this.from = from;
		this.to = Meteorites.RandomMeteorLocationTo(from);
		this.inv = inventory;
	}
	
	public int getId(){
		return this.id;
	}
	public Location getFrom(){
		return this.from;
	}
	
	public Location getTo(){
		return this.to;
	}
	public Inventory getInv(){
		return this.inv;
	}
	public Location getChestLoc(){
		return this.chestloc;
	}
	public void setChestLoc(Location loc){
		this.chestloc = loc;
	}

}
