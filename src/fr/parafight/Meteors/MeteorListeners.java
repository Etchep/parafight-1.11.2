package fr.parafight.Meteors;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import fr.parafight.Main;

public class MeteorListeners implements Listener{

	public static HashMap<Integer, MeteorData> MeteorHash = Main.MeteorHash;

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent e) {
		if (e.getAction().equals(Action.RIGHT_CLICK_BLOCK)) {
			if (e.getClickedBlock().getState() instanceof Chest) {
				if(MeteorHash.isEmpty() == false){
					for(int i : MeteorHash.keySet()){
						if(e.getClickedBlock().getLocation().getBlock().getLocation().equals(MeteorHash.get(i).getChestLoc().getBlock().getLocation())){
							Player p = e.getPlayer();
							e.setCancelled(true);
							e.getClickedBlock().breakNaturally(new ItemStack(Material.IRON_PICKAXE,1));
							Bukkit.broadcastMessage("�9�l>> �aLe loot de la m�t�orite �9" + i + " �aa �t� r�ceptionn� par �9" + p.getName() + " �a!");
							MeteorHash.remove(i);
						}

					}
				}

			}
		}
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		if(e.getInventory().getName().equalsIgnoreCase("Humm du bon stuff !")){
			e.setCancelled(true);
			p.closeInventory();
		}
	}
}
