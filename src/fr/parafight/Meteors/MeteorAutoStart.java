package fr.parafight.Meteors;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import fr.parafight.Main;
import fr.parafight.utils.Meteorites;

public class MeteorAutoStart extends BukkitRunnable{
	
	private Main main;

	public MeteorAutoStart(Main main) {
		this.main = main;
	}
	
	@Override
	public void run() {
		
		if(Bukkit.getOnlinePlayers().size() < 10){
			Bukkit.broadcastMessage("�9�l>> �bIl faut au moins 10 joueurs en ligne pour qu'une m�t�orite apparaisse.");
		}else{
			Meteorites.MeteorPrepare(main, (int)(Bukkit.getOnlinePlayers().size()/10));
		}
		
	}
	

}

