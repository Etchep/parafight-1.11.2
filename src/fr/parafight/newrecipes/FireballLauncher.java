package fr.parafight.newrecipes;

import java.util.HashMap;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;
import fr.parafight.utils.ShapedRegister;

public class FireballLauncher implements Listener{
	
	public HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	int cooldownTime = 5;

	public static ItemStack getItemFireball(){
		ItemStack i = new ItemStack(new ItemStack(Material.FIREBALL, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.DURABILITY, 1, true);
		im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		im.setDisplayName("�6�lBoule de feu");
		im.setLore(Main.lorefireball);
		i.setItemMeta(im);
		return i;
	}
	
	public static ShapedRegister getFireball(){
		ShapedRegister recipe = new ShapedRegister(getItemFireball());
		ItemStack i = new ItemStack(Material.MONSTER_EGG, 1, (short) 50);
		recipe.shape(new String[] {"BBB", "BFB", "BBB"});
		recipe.setIngredient('F', i);
		recipe.setIngredient('B', Material.BLAZE_POWDER);
		
		return recipe;
	}
	
	@EventHandler
	public void onFireballLaunch(PlayerInteractEvent e) {
		Player p = e.getPlayer();
		ItemStack itemInHand = p.getInventory().getItemInMainHand();
		if ((itemInHand != null) && (itemInHand.getType() != Material.AIR) && (itemInHand.getType() == Material.FIREBALL)){
			if ((itemInHand.hasItemMeta()) && (itemInHand.getItemMeta().getLore() != null) && (itemInHand.getItemMeta().getLore().equals(Main.lorefireball))){
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					if(cooldowns.containsKey(p.getName())) {
						long secondsLeft = ((cooldowns.get(p.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
						if(secondsLeft > 0) {
							p.sendMessage("�cPatientez "+ secondsLeft +" secondes pour renvoyer une boule de feu !");
							e.setCancelled(true);
							return;
						}else{
							cooldowns.remove(p.getName());
							cooldowns.put(p.getName(), System.currentTimeMillis());
							ItemStack i = e.getItem();
							if (i == null) return;
							int amount = i.getAmount();
							if (amount == 1){
								p.getInventory().removeItem(new ItemStack[] { i });
								}else{
									i.setAmount(amount - 1);
								}	
							doThrow((Fireball)p.launchProjectile(Fireball.class));
						}
					}else{
						cooldowns.put(p.getName(), System.currentTimeMillis());
						ItemStack i = e.getItem();
						if (i == null) return;
						int amount = i.getAmount();
						if (amount == 1){
							p.getInventory().removeItem(new ItemStack[] { i });
							}else{
								i.setAmount(amount - 1);
							}	
						doThrow((Fireball)p.launchProjectile(Fireball.class));
					}
				}
			}
		}
	}

	private void doThrow(Fireball ball) {
		if (ball.getType().toString().equals("FIREBALL")) {
			ball.setIsIncendiary(true);
			ball.setYield(2);
			ball.setIsIncendiary(false);
		}
	}
}
