package fr.parafight.newrecipes;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import fr.parafight.Main;

public class Pioche3x3 implements Listener{


	public static ItemStack getItemPioche3x3(){
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_PICKAXE, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName("žažlPioche 3x3");
		im.setLore(Main.lorepioche);
		i.setItemMeta(im);
		return i;
	}

	public static ShapedRecipe getPioche3x3(){
		ShapedRecipe pioche = new ShapedRecipe(getItemPioche3x3());
		pioche.shape(new String[] {"BBB", " S ", " S "});
		pioche.setIngredient('S', Material.STICK);
		pioche.setIngredient('B', Material.DIAMOND_BLOCK);

		return pioche;
	}

	@EventHandler
	public void onBreak(BlockBreakEvent e){
		Player p = e.getPlayer();
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		ItemStack itemInHand = p.getInventory().getItemInMainHand();
		Faction faction = null;
		if(!p.isSneaking()){
			if ((itemInHand != null) && (itemInHand.getType() != Material.AIR)) {
				if ((itemInHand.hasItemMeta()) && (itemInHand.getItemMeta().getLore() != null) && (itemInHand.getItemMeta().getLore().equals(Main.lorepioche))){
					Block block;
					if((itemInHand.getType().getMaxDurability()  - (itemInHand.getDurability() + 11)) > 0 ){
						itemInHand.setDurability((short) (itemInHand.getDurability() + 10));
						for(int xOff = -1; xOff <= 1; ++xOff){
							for(int yOff = -1; yOff <= 1; ++yOff){
								for(int zOff = -1; zOff <= 1; ++zOff){
									block = e.getBlock().getRelative(xOff, yOff, zOff);
									Location loc = block.getLocation();
									FLocation fLoc = new FLocation(loc);
									faction = Board.getInstance().getFactionAt(fLoc);
									if(faction.isWilderness() || faction == fp.getFaction()){
										if(!block.getType().equals(Material.BEDROCK)){
											block.breakNaturally(itemInHand);
										}
									}else{
										return;
									}
								}
							}
						}
					}
				}
			}
		}
	} 
}
