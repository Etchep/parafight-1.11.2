package fr.parafight.newrecipes;

import java.util.ArrayList;

import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.ShapedRecipe;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class PainEffects implements Listener{
	

	public static ItemStack getItemPainForce(){
		    ItemStack i = new ItemStack(new ItemStack(Material.BREAD, 1));
		    ItemMeta im = i.getItemMeta();
		    im.addEnchant(Enchantment.DURABILITY, 1, true);
		    im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		    ArrayList<String> description = new ArrayList<String>();
		    im.setDisplayName("�2�lPain de Force");
		    description.add("�aCe pain fourr� aux �pinards te donnera une force");
		    description.add("�asemblable � celle de Popeye !");
		    im.setLore(description);
		    i.setItemMeta(im);
		    return i;
	}
	
	public static ShapedRecipe getpainforce(){
		ShapedRecipe painforce = new ShapedRecipe(getItemPainForce());
		painforce.shape(new String[] {"BBB", "BPB", "BBB"});
		painforce.setIngredient('P', Material.BREAD);
		painforce.setIngredient('B', Material.BLAZE_POWDER);
		
		return painforce;
	}
	
	public static ItemStack getItemPainSpeed(){
	    ItemStack i = new ItemStack(new ItemStack(Material.BREAD, 1));
	    ItemMeta im = i.getItemMeta();
	    im.addEnchant(Enchantment.DURABILITY, 1, true);
	    im.addItemFlags(ItemFlag.HIDE_ENCHANTS);
	    ArrayList<String> description = new ArrayList<String>();
	    im.setDisplayName("�2�lPain de Rapidit�");
	    description.add("�aCe pain saupoudr� d'une myst�rieuse poudre blanche");
	    description.add("�ate rendra plus rapide qu'Usain Bolt !");
	    im.setLore(description);
	    i.setItemMeta(im);
	    return i;
}

	public static ShapedRecipe getpainspeed(){
		ShapedRecipe painspeed = new ShapedRecipe(getItemPainSpeed());
		painspeed.shape(new String[] {"SSS", "SPS", "SSS"});
		painspeed.setIngredient('P', Material.BREAD);
		painspeed.setIngredient('S', Material.SUGAR);
		
		return painspeed;
	}
	@EventHandler
	public void onInterract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		ItemStack itemInHand = p.getInventory().getItemInMainHand();
		if ((itemInHand != null) && (itemInHand.getType() != Material.AIR) && (itemInHand.getType() == Material.BREAD)){
			if ((itemInHand.hasItemMeta()) && (itemInHand.getItemMeta().getLore() != null) && (itemInHand.getItemMeta().getDisplayName().equals("�2�lPain de Force"))){
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					ItemStack i = e.getItem();
					if (i == null) return;
					int amount = i.getAmount();
					if (amount == 1){
						p.getInventory().removeItem(i);
						}else{
							i.setAmount(amount - 1);
						}	
					p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 7200, 0, true, false));
				}
			}
			if ((itemInHand.hasItemMeta()) && (itemInHand.getItemMeta().getLore() != null) && (itemInHand.getItemMeta().getDisplayName().equals("�2�lPain de Rapidit�"))){
				if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
					ItemStack i = e.getItem();
					if (i == null) return;
					int amount = i.getAmount();
					if (amount == 1){
						p.getInventory().removeItem(i);
						}else{
							i.setAmount(amount - 1);
						}	
					p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 7200, 1, true, false));
				}
			}
			
		}
				
	}
	
}
