package fr.parafight.banque;

import java.util.Arrays;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;


public class MoneyLoot implements Listener {
	
	
	public static ItemStack MoneyDrop(String d) {
		ItemStack i = new ItemStack(new ItemStack(Material.GOLD_INGOT, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName(d);
		im.setLore(Arrays.asList(new String[] { "moneyloot" }));
		i.setItemMeta(im);
		return i;
	}
	
	
	
	  @EventHandler
	  public void onDeath(PlayerDeathEvent e) {
		  Player p = e.getEntity().getPlayer();
		  if(Main.getEcon().getBalance(p) >= 1){
			  p.sendMessage("�cVous venez de perdre "+ Main.getEcon().getBalance(p) +"� � votre mort !");
			  p.getLocation().getWorld().dropItem(p.getLocation(), MoneyDrop(String.valueOf(Main.getEcon().getBalance(p))));
			  Main.econ.withdrawPlayer(p, Main.getEcon().getBalance(p));
		  }
		  
	  }
	
	  @EventHandler
	  public void onPickup(PlayerPickupItemEvent e) {
		  Player p = e.getPlayer();
	      ItemMeta meta = e.getItem().getItemStack().getItemMeta();
	      if (!meta.hasLore()) return;
	      if (!meta.getLore().contains("moneyloot")) return;
	      if (meta.getDisplayName() == null) return;
	      e.setCancelled(true);
	      Double name = Double.parseDouble((meta.getDisplayName()));
	        Main.getEcon().depositPlayer(p, name);
	        p.sendMessage("�aVous venez de r�cup�rer " + name + "� !");
	      e.getItem().remove();
	  }
   }
