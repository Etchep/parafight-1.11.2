package fr.parafight.banque;

import java.util.ArrayList;
import java.util.Arrays;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;
import fr.parafight.utils.PlayerData;


public class AddRem implements Listener{


	public static ItemStack getItem(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}

	public static ItemStack getBankBal(double b, double p) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("�6�lBanque");
		description.add("");
		description.add("�6Banque : �e" + b+" �");
		description.add("");
		description.add("�dPoche : �e" + p+" �");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static void openInv(int type, Player p){
		String t = null;
		double 	amount = 0;
		if(type == 1){
			t = "D�poser" ;
		}
		if(type == 0){
			t = "Retirer" ;
		}
		Inventory inv = Bukkit.createInventory(null, 54, "�6�l" + t);
		p.openInventory(inv);
		Main.PlayerMap.get(p.getName()).setBankTransacting(true);
		inv.setItem(4, getBankBal(Main.PlayerMap.get(p.getName()).getBankBalance(), Main.getEcon().getBalance(p)));
		inv.setItem(9, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 100000", 1, 14, null, false));
		inv.setItem(10, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 10000", 1, 14, null, false));
		inv.setItem(18, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 1000", 1, 14, null, false));
		inv.setItem(19, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 100", 1, 14, null, false));
		inv.setItem(20, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 10", 1, 14, null, false));
		inv.setItem(21, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 1", 1, 14, null, false));
		inv.setItem(22, getItem(Material.STAINED_GLASS_PANE, "�e�lR�initialiser", 1, 4, null, false));
		inv.setItem(23, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 1", 1, 5, null, false));
		inv.setItem(24, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 10", 1, 5, null, false));
		inv.setItem(25, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 100", 1, 5, null, false));
		inv.setItem(26, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 1000", 1, 5, null, false));
		inv.setItem(17, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 100000", 1, 5, null, false));
		inv.setItem(16, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 10000", 1, 5, null, false));
		inv.setItem(30, getItem(Material.STAINED_GLASS, "�a�lConfirmer", 1, 5, "�e�l" + t + " " + amount +"�", false));
		inv.setItem(32, getItem(Material.STAINED_GLASS, "�c�lAnnuler", 1, 14, null ,false));
		inv.setItem(40, getItem(Material.STAINED_GLASS, "�a�lTout " + t.toLowerCase(), 1, 5, null , false));
	}

	public static void updateInv(Player p, Inventory inv){
		PlayerData data = Main.PlayerMap.get(p.getName());
		String t = null;
		double 	amount = data.getBankChangeAmount();
		if(data.getBankType() == 1){
			t = "D�poser" ;
		}
		if(data.getBankType() == 0){
			t = "Retirer" ;
		}
		inv.setItem(4, getBankBal(data.getBankBalance(), Main.getEcon().getBalance(p)));
		inv.setItem(9, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 100000", 1, 14, null, false));
		inv.setItem(10, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 10000", 1, 14, null, false));
		inv.setItem(18, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 1000", 1, 14, null, false));
		inv.setItem(19, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 100", 1, 14, null, false));
		inv.setItem(20, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 10", 1, 14, null, false));
		inv.setItem(21, getItem(Material.STAINED_GLASS_PANE, "�e�lRetirer 1", 1, 14, null, false));
		inv.setItem(22, getItem(Material.STAINED_GLASS_PANE, "�e�lR�initialiser", 1, 4, null, false));
		inv.setItem(23, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 1", 1, 5, null, false));
		inv.setItem(24, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 10", 1, 5, null, false));
		inv.setItem(25, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 100", 1, 5, null, false));
		inv.setItem(26, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 1000", 1, 5, null, false));
		inv.setItem(17, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 100000", 1, 5, null, false));
		inv.setItem(16, getItem(Material.STAINED_GLASS_PANE, "�e�lAjouter 10000", 1, 5, null, false));
		inv.setItem(30, getItem(Material.STAINED_GLASS, "�a�lConfirmer", 1, 5, "�e�l" + t + " " + amount +"�", false));
		inv.setItem(32, getItem(Material.STAINED_GLASS, "�c�lAnnuler", 1, 14, null ,false));
		inv.setItem(40, getItem(Material.STAINED_GLASS, "�a�lTout " + t.toLowerCase(), 1, 5, null , false));
	}



	public static void close(Player p){
		PlayerData data = Main.PlayerMap.get(p.getName());
		data.setBankChangeAmount(0);
		data.setBankTransacting(false);
	}

	public void remBankChangeAmount(Player p, double amount){
		PlayerData data = Main.PlayerMap.get(p.getName());
		if(data.getBankChangeAmount() >= amount){
			data.setBankChangeAmount(data.getBankChangeAmount() - amount);
		}
	}
	public void bankDeposit(Player p, double amount){

		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				PlayerData data = Main.PlayerMap.get(p.getName());
				if(Main.getEcon().getBalance(p) >= amount){
					data.setBankBalance(data.getBankBalance() + amount);
					Main.getEcon().withdrawPlayer(p, amount);
					p.sendMessage("�a[SUCCES] Vous venez de d�poser " + amount + "� sur votre compte banquaire!");
					p.closeInventory();
				}else{
					p.sendMessage("�c[ECHEC] Vous n'avez pas " + amount + "� en poche !");
					p.closeInventory();
				}
				
			}
		});
	}
	
	public void bankRemove(Player p, double amount){

		Bukkit.getScheduler().runTaskAsynchronously(Main.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				PlayerData data = Main.PlayerMap.get(p.getName());
				if(data.getBankBalance() >= amount){
					Main.getEcon().depositPlayer(p, amount);
					data.setBankBalance(data.getBankBalance() - amount);
					p.sendMessage("�a[SUCCES] Vous venez de retirer " + amount + "� de votre compte banquaire !");
					p.closeInventory();
				}else{
					p.sendMessage("�c[ECHEC] Vous n'avez pas " + amount + "� dans votre compte banquaire !");
					p.closeInventory();
				}
				
			}
		});
	}

	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		ItemStack current = e.getCurrentItem();
		if(current == null) return;
		if(Main.PlayerMap.get(p.getName()).isBankTransacting() == true){
			PlayerData data = Main.PlayerMap.get(p.getName());
			int type = data.getBankType();
			switch(e.getSlot()){
			case 9:
				remBankChangeAmount(p, 100000);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 10:
				remBankChangeAmount(p, 10000);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 18:
				remBankChangeAmount(p, 1000);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 19:
				remBankChangeAmount(p, 100);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 20:
				remBankChangeAmount(p, 10);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 21:
				remBankChangeAmount(p, 1);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 22:
				data.setBankChangeAmount(0);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 23:
				data.setBankChangeAmount(data.getBankChangeAmount() + 1);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 24:
				data.setBankChangeAmount(data.getBankChangeAmount() + 10);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 25:
				data.setBankChangeAmount(data.getBankChangeAmount() + 100);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 26:
				data.setBankChangeAmount(data.getBankChangeAmount() + 1000);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 16:
				data.setBankChangeAmount(data.getBankChangeAmount() + 10000);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 17:
				data.setBankChangeAmount(data.getBankChangeAmount() + 100000);
				updateInv(p, e.getInventory());
				e.setCancelled(true);
				break;
			case 30:
				if(type == 1){
					bankDeposit(p, data.getBankChangeAmount());
				}
				if(type == 0){
					bankRemove(p, data.getBankChangeAmount());
				}
				e.setCancelled(true);
				break;
			case 32:
				p.openInventory(BankMenu.Menu);
				e.setCancelled(true);
				break;
			case 40:
				if(type == 1){
					bankDeposit(p, Main.getEcon().getBalance(p));
				}
				if(type == 0){
					bankRemove(p, data.getBankBalance());
				}
				e.setCancelled(true);
				break;
			default:
				e.setCancelled(true);
				break;
			}
		}
	}

	@EventHandler
	public void onClose(InventoryCloseEvent e){
		Player p = (Player) e.getPlayer();
		if(Main.PlayerMap.get(p.getName()).isBankTransacting() == true){
			close(p);
		}
	}

}
