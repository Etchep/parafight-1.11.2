package fr.parafight.banque;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.parafight.Main;

public class CommandBank implements CommandExecutor{

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
	    if ((sender instanceof Player)) {
	    	Player p = (Player)sender;
	    	if (args.length == 0) {
	        if ((string.equalsIgnoreCase("bank") || (string.equalsIgnoreCase("banque"))))
	        	 p.sendMessage("§e§m--------------------------------");
	          	 p.sendMessage("§a§lVous avez §2§l" + Main.PlayerMap.get(p.getName()).getBankBalance() + " €§a§l dans votre banque.");
	          	 p.sendMessage("§e");
	          	 p.sendMessage("§7Utilisez la commande §8/warp banque§7 pour");
	          	 p.sendMessage("§7déposer ou retirer de l'argent.");
	          	 p.sendMessage("§e");
	          	 p.sendMessage("§4⚠ §cL'argent non déposé à la banque est looté à votre mort !.");
	          	 p.sendMessage("§e§m--------------------------------");
	    }
	    	if (args.length > 0) {
	    		if(args[0].equalsIgnoreCase("menu")){
	    			if(p.hasPermission("bank.menu")){
	    				p.openInventory(BankMenu.Menu);
	    			}
	    		}
	    	}
	    }
		return false;
	}

}
