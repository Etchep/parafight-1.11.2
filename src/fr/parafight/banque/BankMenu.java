package fr.parafight.banque;

import java.util.Arrays;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;
import fr.parafight.utils.PlayerData;

public class BankMenu implements Listener{
	
	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;

	public static Inventory Menu = Bukkit.createInventory(null, 27, "�d�lQue voulez-vous faire?");

	public static ItemStack getItem(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}
	
	
	@EventHandler
	public void onOpen(InventoryOpenEvent e){
		Inventory inv = e.getInventory();
		if(inv.getName().equalsIgnoreCase(Menu.getName())){
			Menu.setItem(11, getItem(Material.NAME_TAG, "�a�lD�poser", 1, 0, "�dClic d�poser de l'argent !", true));
			Menu.setItem(15, getItem(Material.NAME_TAG, "�a�lRetirer", 1, 0, "�dClic pour retirer de l'argent !" , true));
		}
	}

	@EventHandler
	public void onInteract(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		PlayerData data = PlayerMap.get(p.getName());
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if(current == null) return;
		if(inv.getName().equals(Menu.getName())){
			e.setCancelled(true);
			if(e.getSlot() == 11){
				data.setBankChangeAmount(0);
				data.setBankType(1);
				AddRem.openInv(1, p);
			}
			if(e.getSlot() == 15){
				data.setBankChangeAmount(0);
				data.setBankType(0);
				AddRem.openInv(0, p);
			}
		}


	}

}
