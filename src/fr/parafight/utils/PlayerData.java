package fr.parafight.utils;

import org.bukkit.inventory.Inventory;

public class PlayerData {
	
	private int mineur;
	private int assaillant;
	private int chasseur;
	private int farmeur;
	private int pecheur;
	private Inventory pchest;
	private boolean frozen;
	private double BankBalance;
	private double BankChangeAmount;
	private int BankType;
	private boolean isBankTransacting;
	
	public PlayerData(int mineur, int assaillant, int chasseur, int farmeur, int pecheur, Inventory pchest, double bank){
		this.mineur = mineur;
		this.assaillant = assaillant;
		this.chasseur = chasseur;
		this.farmeur = farmeur;
		this.pecheur = pecheur;
		this.pchest = pchest;
		this.frozen = false;
		this.BankBalance = bank;
	}
	public boolean isFrozen(){
		return frozen;
	}
	public void setFrozen(boolean booelan){
		this.frozen = booelan;
	}
	public int getMineur(){
		return mineur;
	}
	public int getAssaillant(){
		return assaillant;
	}
	public int getChasseur(){
		return chasseur;
	}
	public int getFarmeur(){
		return farmeur;
	}
	public int getPecheur(){
		return pecheur;
	}
	public Inventory getPrivateChest(){
		return pchest;
	}
	public void setMineur(int mineur){
		this.mineur = mineur;
	}
	public void setAssaillant(int assaillant){
		this.assaillant = assaillant;
	}
	public void setChasseur(int chasseur){
		this.chasseur = chasseur;
	}
	public void setFarmeur(int farmeur){
		this.farmeur = farmeur;
	}
	public void setPecheur(int pecheur){
		this.pecheur = pecheur;
	}
	public void setPrivateChest(Inventory inv){
		this.pchest = inv;
	}
	
	public double getMineurPercent(){
		double percent = this.mineur / 400.0;
		if (percent < 100.0){
			return percent;
		}else{
			return 100.0;
		}
	}
	public double getFarmeurPercert(){
		double percent = this.farmeur / 200.0;
		if (percent < 100.0){
			return percent;
		}else{
			return 100.0;
		}
	}
	public double getAssaillantPercent(){
		double percent = this.assaillant / 5.0;
		if (percent < 100.0){
			return percent;
		}else{
			return 100.0;
		}
	}
	public double getChasseurPercent(){
		double percent = this.chasseur / 200.0;
		if (percent < 100.0){
			return percent;
		}else{
			return 100.0;
		}
	}
	public double getPecheurPercent(){
		double percent = this.pecheur / 5.0;
		if (percent < 100.0){
			return percent;
		}else{
			return 100.0;
		}
	}
	public double getTotalPercent(){
		double percent1 = (this.getChasseurPercent() + this.getMineurPercent() + this.getPecheurPercent() + this.getAssaillantPercent() + this.getFarmeurPercert())/5.0;
		double percent2 = percent1*100;
		double percent = (int)percent2/100.0;
		return percent;
	}
	public double getBankBalance() {
		return BankBalance;
	}
	public void setBankBalance(double bankBalance) {
		BankBalance = bankBalance;
	}
	public int getBankType() {
		return BankType;
	}
	public void setBankType(int bankType) {
		BankType = bankType;
	}
	public double getBankChangeAmount() {
		return BankChangeAmount;
	}
	public void setBankChangeAmount(double bankChangeAmount) {
		BankChangeAmount = bankChangeAmount;
	}
	public boolean isBankTransacting() {
		return isBankTransacting;
	}
	public void setBankTransacting(boolean isBankTransacting) {
		this.isBankTransacting = isBankTransacting;
	}

}
