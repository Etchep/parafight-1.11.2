package fr.parafight.utils;

import java.util.HashMap;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Chest;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.Entity;
import org.bukkit.entity.FallingBlock;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import fr.parafight.Main;
import fr.parafight.Meteors.MeteorData;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import net.minecraft.server.v1_11_R1.EnumParticle;
import net.minecraft.server.v1_11_R1.PacketPlayOutWorldParticles;

public class Meteorites {

	public static HashMap<Integer, MeteorData> MeteorHash = Main.MeteorHash;
	private static ConcurrentHashMap<Integer, BukkitRunnable> MeteorTask =  Main.MeteorTask;

	public static void MeteorPrepare(Plugin plugin, int amount){
		CountdownMeteor(plugin);
		if(amount == 1){
			int id = MeteorHash.size() + 1;
			MeteorHash.put(id, new MeteorData(id, RandomMeteorLocationFrom(), LootChest.setChest()));
			Bukkit.broadcastMessage("�e�m---------------------------");
			Bukkit.broadcastMessage("�9�l>> �a�lUne m�teorite se dirigeant vers Parafight a �t� d�tect� !");
			Bukkit.broadcastMessage("�b -Temps avant impact : �915 minutes�b .");
			String s = "�b -Point d'impact approximatif : �bX = �9�l"+ MeteorHash.get(id).getTo().getBlockX()+" �bY = �9�l" + MeteorHash.get(id).getTo().getBlockY()+" �bZ = �9�l"+ MeteorHash.get(id).getTo().getBlockZ()+ "�b.";
			TextComponent ss = new TextComponent(s);
			ss.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�9Clic pour conna�te le loot !").create()));
			ss.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/meteor " + id));
			Bukkit.spigot().broadcast(ss);
			Bukkit.broadcastMessage("�b -Info utile : �cAttention � l'impact ;)");
			Bukkit.broadcastMessage("�e�m---------------------------");
		}else if(amount > 1){
			Bukkit.broadcastMessage("�e�m---------------------------");
			Bukkit.broadcastMessage("�9�l>> �a�ldes m�teorites se dirigeants vers Parafight ont �t� d�tect�s !");
			Bukkit.broadcastMessage("�b -Temps avant impacts : �915 minutes�b .");
			Bukkit.broadcastMessage("�b -Points d'impact approximatifs : ");
			for(int i = 0; i < amount; i++){
				int id = MeteorHash.size() + 1;
				MeteorHash.put(id, new MeteorData(id, RandomMeteorLocationFrom(), LootChest.setChest()));
				String s = "�bMeteorite " + id + " : �bX = �9�l"+ MeteorHash.get(id).getTo().getBlockX()+" �bY = �9�l" + MeteorHash.get(id).getTo().getBlockY()+" �bZ = �9�l"+ MeteorHash.get(id).getTo().getBlockZ()+ "�b.";
				TextComponent ss = new TextComponent(s);
				ss.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("�9Clic pour conna�te le loot !").create()));
				ss.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/meteor " + id));
				Bukkit.spigot().broadcast(ss);
			}
			Bukkit.broadcastMessage("�b -Info utile : �cAttention � l'impact ;)");
			Bukkit.broadcastMessage("�e�m---------------------------");
		}
	}

	public static void CountdownMeteor(Plugin plugin){
		new BukkitRunnable() {
			int minutes = 0;
			@Override
			public void run() {
				minutes++;
				if(minutes == 5){
					Bukkit.broadcastMessage("�9�l>> �a�lImpact(s) dans 10 minutes ! (/meteor)");
				}
				if(minutes == 10){
					Bukkit.broadcastMessage("�9�l>> �a�lImpact(s) dans 5 minutes ! (/meteor)");
				}
				if(minutes == 14){
					Bukkit.broadcastMessage("�9�l>> �a�lImpact(s) dans 1 minute ! (/meteor)");
				}
				if(minutes == 15){
					Bukkit.broadcastMessage("�9�l>> �a�lImpact(s) imminant(s) !");
					for(int i: MeteorHash.keySet()){
						MeteorLaunch(MeteorHash.get(i).getId(),MeteorHash.get(i).getFrom(), MeteorHash.get(i).getTo(), plugin);
					}
					cancel();
				}
			}
		}.runTaskTimer(plugin, 1200, 1200);
	}

	public static Location RandomMeteorLocationFrom(){
		Random r = new Random();
		double x = -1250 + r.nextInt(2500);
		double z = -1250 + r.nextInt(2500);
		double y = 120;
		Location from = new Location(Bukkit.getWorld("world"),x,y,z);
		if(Math.abs(x) < 600 && Math.abs(z) < 600){
			from = RandomMeteorLocationFrom();
		}
		return from;
	}
	public static Location RandomMeteorLocationTo(Location from){
		Random r = new Random();
		double x = from.getX() + r.nextInt(100);
		double z = from.getZ() + r.nextInt(100);
		double y = 200;
		Location to = Meteorites.getHighestY(new Location(Bukkit.getWorld("world"),x,y,z));
		return to;
	}

	public static Location getHighestY(Location loc){
		Location locBelow = loc.subtract(0, 1, 0);
		if(locBelow.getBlock().getType() == Material.AIR) {
			locBelow = getHighestY(locBelow);
		}
		return locBelow;
	}

	@SuppressWarnings("deprecation")
	public static void MeteorLaunch(int id, Location from, Location to, Plugin plugin){
		if(!from.getChunk().isLoaded()){
			from.getChunk().load();
		}
		FallingBlock fall = from.getWorld().spawnFallingBlock(from, Material.BEDROCK, (byte)0);
		fall.setDropItem(false);

		ArmorStand stand = (ArmorStand)from.getWorld().spawn(from, 
				ArmorStand.class);
		stand.setCustomName("MeteorStand");
		stand.setCustomNameVisible(false);
		stand.setGravity(false);
		stand.setVisible(false);
		stand.setRemoveWhenFarAway(false);
		stand.setPassenger(fall);
		MeteorTask.put(id, new BukkitRunnable() {
			int tick = 0;
			float f = 0;
			public void run() {

				tick++;
				if (tick > 5000) this.cancel();
				double Vx = to.getX() - from.getX();
				double Vy = (to.getY()) - from.getY();
				double Vz = to.getZ() - from.getZ();
				Vector v = new Vector(Vx,Vy,Vz).multiply(0.008);
				Entity passenger = stand.getPassenger();
				stand.eject();
				if(!stand.getLocation().add(v).getChunk().isLoaded()){
					stand.getLocation().add(v).getChunk().load();
				}
				stand.teleport(stand.getLocation().add(v));
				stand.setPassenger(passenger);
				Location moveloc = fall.getLocation();
				int reste = tick % 2;
				if(reste == 0){
					for(Player p : Bukkit.getOnlinePlayers()){
						PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(EnumParticle.EXPLOSION_LARGE, true, (float)moveloc.getX(), (float)moveloc.getY(), (float)moveloc.getZ(), f, f, f, f, 0, null);
						((CraftPlayer) p).getHandle().playerConnection.sendPacket(particles);
					}
				}
				if(fall.getLocation().getBlock().getType() != Material.AIR){
					for(Player p : Bukkit.getOnlinePlayers()){
						PacketPlayOutWorldParticles particles = new PacketPlayOutWorldParticles(EnumParticle.EXPLOSION_HUGE, true, (float)moveloc.getX(), (float)moveloc.getY(), (float)moveloc.getZ(), f, f, f, f, 0, null);
						((CraftPlayer) p).getHandle().playerConnection.sendPacket(particles);
					}
					for(Entity entity : fall.getNearbyEntities(9, 9, 9)){
						if(entity instanceof LivingEntity){
							Vector direction = new Vector();
							direction.setX(1 + (Math.random() - Math.random()));
							direction.setY(0.1D + Math.random());
							direction.setZ(1 + (Math.random() - Math.random()));
							((LivingEntity)entity).damage(15);
							entity.setVelocity(direction.multiply(1.5));
						}
					}
					Location chestloc = fall.getLocation().add(0,1,0);
					chestloc.getBlock().setType(Material.CHEST);
					Chest chest = (Chest)chestloc.getBlock().getState();
					chest.getInventory().setContents(MeteorHash.get(id).getInv().getContents());
					MeteorHash.get(id).setChestLoc(chestloc);
					stand.eject();
					fall.remove();
					stand.remove();
					MeteorTask.remove(id);
					cancel();
				}
			}
		});
		MeteorTask.get(id).runTaskTimer(plugin, 1, 1);
	}

}
