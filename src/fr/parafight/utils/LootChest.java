package fr.parafight.utils;

import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.EnchantmentStorageMeta;

public class LootChest {
	
	public static Inventory setChest(){

        Random r = new Random();
        
        Inventory chest = Bukkit.createInventory(null, 27, "Humm du bon stuff !");
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.DIAMOND, 1+r.nextInt(16));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.1) {
			ItemStack it = new ItemStack(Material.GOLDEN_APPLE, 1+r.nextInt(2), (short) 1);
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.GOLDEN_APPLE, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.TNT, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.OBSIDIAN, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.IRON_INGOT, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.GOLD_INGOT, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.LEATHER, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.COOKED_BEEF, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack it = new ItemStack(Material.WHEAT, 1+r.nextInt(32));
			setItemRandomlySloted(chest, it);
		}
		if (Math.random() < 0.6) {
			ItemStack Protection1 = new ItemStack(Material.ENCHANTED_BOOK, 1);
	        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)Protection1.getItemMeta();
	        meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 1, true);
	        Protection1.setItemMeta(meta);
	        setItemRandomlySloted(chest, Protection1);
		}
		if (Math.random() < 0.05) {
			ItemStack Protection4 = new ItemStack(Material.ENCHANTED_BOOK, 1);
	        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)Protection4.getItemMeta();
	        meta.addStoredEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
	        Protection4.setItemMeta(meta);
	        setItemRandomlySloted(chest, Protection4);
		}
		if (Math.random() < 0.6) {
			ItemStack Power1 = new ItemStack(Material.ENCHANTED_BOOK, 1);
	        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)Power1.getItemMeta();
	        meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 1, true);
	        Power1.setItemMeta(meta);
	        setItemRandomlySloted(chest, Power1);
		}
		if (Math.random() < 0.6) {
			ItemStack Power3 = new ItemStack(Material.ENCHANTED_BOOK, 1);
	        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)Power3.getItemMeta();
	        meta.addStoredEnchant(Enchantment.ARROW_DAMAGE, 3, true);
	        Power3.setItemMeta(meta);
	        setItemRandomlySloted(chest, Power3);
		}
		if (Math.random() < 0.6) {
			ItemStack Punch = new ItemStack(Material.ENCHANTED_BOOK, 1);
	        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)Punch.getItemMeta();
	        meta.addStoredEnchant(Enchantment.ARROW_KNOCKBACK, 1, true);
	        Punch.setItemMeta(meta);
	        setItemRandomlySloted(chest, Punch);
		}
		if (Math.random() < 0.6) {
			ItemStack Unbreak = new ItemStack(Material.ENCHANTED_BOOK, 1);
	        EnchantmentStorageMeta meta = (EnchantmentStorageMeta)Unbreak.getItemMeta();
	        meta.addStoredEnchant(Enchantment.DURABILITY, 3, true);
	        Unbreak.setItemMeta(meta);
	        setItemRandomlySloted(chest, Unbreak);
			
		}
		return chest;
		
	}
	
	public static void setItemRandomlySloted(Inventory inv, ItemStack item){
		Random r = new Random();
		int slot = r.nextInt(26);
		if(inv.getItem(slot) == null) {
			inv.setItem(slot, item);
		}else{
			while(inv.getItem(slot) == null){
				slot = r.nextInt(26);
				if(inv.getItem(slot) == null) {
					inv.setItem(slot, item);
					return;
				}
			}
		}
	}

}
