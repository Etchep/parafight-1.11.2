package fr.parafight;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;

import net.minecraft.server.v1_11_R1.EntityPlayer;

public class Commands implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command command, String arg2, String[] args) {
		   Player player = (Player)sender;
		    if ((command.getName().equalsIgnoreCase("ytb")) || (command.getName().equalsIgnoreCase("youtube"))){
		        player.sendMessage("�e�lGrades youtubeurs:");
		        player.sendMessage("   �e- Youtubeur : �6350�e abonn�s + �650�e vues minimum sur une vid�o sur le serveur");
		        player.sendMessage("   �e- Youtubeur+ : �6550�e abonn�s + �6125�e vues minimum sur une vid�o sur le serveur");
		        player.sendMessage("   �e- Famous : �61000�e abonn�s + �6300�e vues minimum sur une vid�o sur le serveur");
		        player.sendMessage("�8Il faudra faire des vid�os r�guli�rement sur le serveur pour garder votre grade.");
		        player.sendMessage("�6�lSi vous �tes youtubeur, veuillez faire votre demande sur Discord.");
			  }
		    if (command.getName().equalsIgnoreCase("ts")) {
		        player.sendMessage(ChatColor.GREEN + "Il n'y a pas de serveur teamspeak.");
		        return true;
		      }
		    if (command.getName().equalsIgnoreCase("discord")) {
		        player.sendMessage(ChatColor.GREEN + "Discord :" + ChatColor.DARK_GREEN + " https://discord.gg/Mun6vYu");
		        return true;
		      }
		    if (command.getName().equalsIgnoreCase("site")) {
		        player.sendMessage(ChatColor.GREEN + "Lien du site :" + ChatColor.DARK_GREEN + " https://parafight.fr/");
		        return true;
		      }
		    if (command.getName().equalsIgnoreCase("map")) {
		    	player.sendMessage("�e�lLimite de la map : �6�l15 000 x 15 000 blocs");
		        return true;
		      }
		    if (command.getName().equalsIgnoreCase("explosions") || command.getName().equalsIgnoreCase("tnt")) {
		    	player.sendMessage("-�e�lExplosion de l'obsidienne: 10 explosions");
		    	player.sendMessage("-�e�lExplosion du bloc de fer: 15 explosions");
		    	player.sendMessage("-�e�lExplosion de la pierre de l'ender: 20 explosions");
		    	player.sendMessage("-�e�lExplosion de l'enclume : 20 explosions");
		    	player.sendMessage("-�e�lExplosion de la table d'enchant: 25 explosions");
		    	player.sendMessage("-�e�lExplosion de l'enderchest: 25 explosions");
		        return true;
		      }
		    
		     
		    
		    if (command.getName().equalsIgnoreCase("annonce")){
				    if ((sender instanceof Player)){
				    	if (args.length == 0) {
				    		player.sendMessage("Faites /annonce text");
				    	}
				    	
				    	if (args.length >= 1){
				    		   StringBuilder bc = new StringBuilder();
							   for(String part : args){
								   bc.append(part + " ");
							   }
							   Bukkit.broadcastMessage("�6�lAnnonce �6[�e" + player.getName() +"�6] �e�l>>" +"�e�l "+ bc.toString());
				    		
				    	}
				    	return true;
				    }
		    	}
		    if (command.getName().equalsIgnoreCase("info")) {
		        player.sendMessage("Informations g�n�rales sur le serveur :");
		        player.sendMessage("");
		        player.sendMessage("-�e�lExplosions d�sactiv�es");
		        player.sendMessage("-�e�lPotions de Force II bloqu�es");
		        player.sendMessage("-�e�lCooldown Pomme cheat : 1 minute");
		        player.sendMessage("-�e�lLimite de la map : �615 000 x 15 000 blocs");
		        player.sendMessage("");
		        player.sendMessage("-�e�lExplosion de l'obsidienne: 10 explosions");
		        player.sendMessage("-�e�lExplosion du bloc de fer: 15 explosions");
		        player.sendMessage("-�e�lExplosion de la pierre de l'ender: 20 explosions");
		        player.sendMessage("-�e�lExplosion de l'enclume : 20 explosions");
		        player.sendMessage("-�e�lExplosion de la table d'enchant: 25 explosions");
		        player.sendMessage("-�e�lExplosion de l'enderchest: 25 explosions");
		        player.sendMessage("");
		        player.sendMessage("");
		        player.sendMessage("�e�lGrades youtubeurs:");
		        player.sendMessage("   �eTapez la commande �6�l/ytb�c.");
		        player.sendMessage("");
		        player.sendMessage("-�e�lSite web : �6https://parafight.fr/");
		        player.sendMessage("-�e�lDiscord : �6https://discord.gg/Mun6vYu");
		        player.sendMessage("-�e�lIP : �6play.parafight.fr");
		        return true;
		      }
		    
		    
		    
		    
		    if ((sender instanceof Player))
		    {
		      Player p = (Player)sender;

		      if (command.getName().equalsIgnoreCase("ping"))
		      {
		        if (args.length == 0) {
		          int ping = ((CraftPlayer)p).getHandle().ping;
		          p.sendMessage("�bVotre latence est de : �3" + ping +" ms");
		        }
		        else
		        {
		          Player target = Bukkit.getPlayer(args[0]);
		          if (target != null)
		          {
		            CraftPlayer cbTarget = (CraftPlayer)target;
		            EntityPlayer epTarget = cbTarget.getHandle();

		            int ping = epTarget.ping;
		            p.sendMessage(ChatColor.DARK_AQUA+ "La latence de" + target.getName() + "est de : �3" + ping +" ms");
		          }
		        }
		        return true;
		      }

		    }
		return false;
	}

}
