package fr.parafight.boutique;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.massivecraft.factions.FPlayers;


public class Menu implements Listener {
	
	public static Inventory Menu = Bukkit.createInventory(null, 36, "�dMenu");

	public ItemStack getItem(Material material, String name, int quantity, String desc){
		ItemStack it = new ItemStack(material, quantity);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		it.setItemMeta(itM);
		return it;
	}
	@EventHandler
	public void onOpen(InventoryOpenEvent e1){
		Inventory inv = e1.getInventory();
		if(inv.getName().equalsIgnoreCase(Menu.getName())){
			Menu.setItem(12, getItem(Material.DIAMOND_CHESTPLATE, "�5�lGrades", 1, "�dClic pour acc�der � la boutique des grades !"));
			Menu.setItem(13, getItem(Material.CHEST, "�5�lKits", 1, "�dClic pour acc�der � la boutique des kits !"));
			Menu.setItem(14, getItem(Material.POTION, "�5�lEffets", 1, "�dClic pour acc�der � la boutique des effets !"));
			Menu.setItem(21, getItem(Material.DIAMOND_AXE, "�5�lDivers", 1, "�dClic pour y acc�der !"));
			Menu.setItem(22, getItem(Material.TRIPWIRE_HOOK, "�5�lKeys", 1, "�dClic pour y acc�der !"));
			Menu.setItem(23, getItem(Material.DOUBLE_PLANT, "�5�lPoints", 1, "�dClic pour y acc�der !"));
			}
		}
	
	@EventHandler
	public void onInteract(InventoryClickEvent e){
		 Player p = (Player)e.getWhoClicked();
		 Inventory inv = e.getInventory();
		 ItemStack current = e.getCurrentItem();
		 
		 if(current == null) return;
		 if(inv.getName().equalsIgnoreCase("�dMenu")){
			 e.setCancelled(true);
			 
			 switch (current.getType()) {
			case DIAMOND_CHESTPLATE:
				p.openInventory(Grades.Grades);
				break;
			case CHEST:
				p.openInventory(Kits.KitsGui);
				break;
			case POTION:
				p.openInventory(Effects.Effects);
				break;
			case DIAMOND_AXE:
				p.openInventory(Divers.Divers);
				break;
			case TRIPWIRE_HOOK:
				p.openInventory(Keys.Keys);
				break;
			case DOUBLE_PLANT:
				if(FPlayers.getInstance().getByPlayer(p).getFaction().isWilderness() == false){
					p.openInventory(Points.Points);	
				}else{
					p.closeInventory();
					p.sendMessage("�cVous devez avoir une faction.");
				}
				break;
			default:
				break;
			}
		 
	    }
	      
		
	}
}
