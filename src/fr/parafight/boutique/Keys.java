package fr.parafight.boutique;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import fr.parafight.Main;
import net.milkbowl.vault.economy.Economy;

public class Keys implements Listener {

	public static Economy econ = null;

	public static Inventory Keys = Bukkit.createInventory(null, 36, "§dClées !");

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}

	public ItemStack getItemKey(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}



	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();
		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(Keys.getName())) {
			switch (e.getSlot()) {
			case 27:
				e.setCancelled(true);
				p.openInventory(Menu.Menu);
				break;
			case 10:
				if (Main.econ.getBalance(p) >= 100000) {
					Main.econ.withdrawPlayer(p, 100000);
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"crate key " + p.getName() + " DiamondBox 1");
				} else {
					p.sendMessage("§cVous n'avez pas suffisament d'argent !");
				}
				e.setCancelled(true);
				p.closeInventory();
				break;
			case 13:
				if (Main.econ.getBalance(p) >= 180000) {
					Main.econ.withdrawPlayer(p, 180000);
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"crate key " + p.getName() + " DiamondBox 2");
				} else {
					p.sendMessage("§cVous n'avez pas suffisament d'argent !");
				}
				e.setCancelled(true);
				p.closeInventory();
				break;
			case 16:
				if (Main.econ.getBalance(p) >= 250000) {
					Main.econ.withdrawPlayer(p, 250000);
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"crate key " + p.getName() + " DiamondBox 3");
				} else {
					p.sendMessage("§cVous n'avez pas suffisament d'argent !");
				}
				e.setCancelled(true);
				p.closeInventory();
				break;
			case 19:
				if (Main.econ.getBalance(p) >= 50000) {
					Main.econ.withdrawPlayer(p, 50000);
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"crate key " + p.getName() + " GoldenBox 1");
				} else {
					p.sendMessage("§cVous n'avez pas suffisament d'argent !");
				}
				e.setCancelled(true);
				p.closeInventory();
				break;
			case 22:
				if (Main.econ.getBalance(p) >= 145000) {
					Main.econ.withdrawPlayer(p, 145000);
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"crate key " + p.getName() + " GoldenBox 2");
				} else {
					p.sendMessage("§cVous n'avez pas suffisament d'argent !");
				}
				e.setCancelled(true);
				p.closeInventory();
				break;
			case 25:
				if (Main.econ.getBalance(p) >= 175000) {
					Main.econ.withdrawPlayer(p, 175000);
					Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
							"crate key " + p.getName() + " GoldenBox 3");
				} else {
					p.sendMessage("§cVous n'avez pas suffisament d'argent !");
				}
				e.setCancelled(true);
				p.closeInventory();
				break;
				default:
					e.setCancelled(true);
					p.closeInventory();
					break;
			}
		}
	}

	@EventHandler
	public void onOpen2(InventoryOpenEvent e) {

		if (e.getInventory().getName().equalsIgnoreCase(Keys.getName())) {

			Player player = (Player) e.getPlayer();
			ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta playerheadmeta = (SkullMeta) playerhead.getItemMeta();
			playerheadmeta.setOwner(player.getName());
			playerheadmeta.setDisplayName("§6§lInformations");
			playerhead.setItemMeta(playerheadmeta);
			ArrayList<String> description = new ArrayList<String>();
			description.add("§6");
			description.add("➢§6§lArgent §e: §6§l" + Main.econ.getBalance(player) + "$");
			playerheadmeta.setLore(description);
			playerhead.setItemMeta(playerheadmeta);
			Keys.setItem(0, playerhead);
		}
	}

	@EventHandler
	public void onOpen(InventoryOpenEvent e1) {
		Inventory inv = e1.getInventory();
		if (inv.getName().equalsIgnoreCase(Keys.getName())) {
			Keys.setItem(27, getItemBack());
			Keys.setItem(10, getItemKey(Material.DIAMOND, "§b§l1 DiamondKey", 1, 0, "§f§lPrix : 100 000€", false));
			Keys.setItem(13, getItemKey(Material.DIAMOND, "§b§l2 DiamondKey", 2, 0, "§f§lPrix : 180 000€", false));
			Keys.setItem(16, getItemKey(Material.DIAMOND, "§b§l3 DiamondKey", 3, 0, "§f§lPrix : 250 000€", false));
			Keys.setItem(19, getItemKey(Material.GOLD_INGOT, "§6§l1 GoldenKey", 1, 0, "§f§lPrix : 50 000€", false));
			Keys.setItem(22, getItemKey(Material.GOLD_INGOT, "§6§l2 GoldenKey", 2, 0, "§f§lPrix : 145 000€", false));
			Keys.setItem(25, getItemKey(Material.GOLD_INGOT, "§6§l3 GoldenKey", 3, 0, "§f§lPrix : 175 000€", false));

		}

	}

}

