package fr.parafight.boutique;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import fr.parafight.Main;

public class Kits implements Listener {

	public static Inventory KitsGui = Bukkit.createInventory(null, 36, "§dKits !");

	public static ItemStack getItemMineur() {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_PICKAXE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lMineur");
		description.add(ChatColor.YELLOW + " ");
		description.add(ChatColor.DARK_GREEN + "Ce kit contient:");
		description.add(ChatColor.YELLOW + "-Pioche en diamant Toucher de soie 1/Efficacitée 5/Unbreaking 2");
		description.add(ChatColor.YELLOW + "-32 steaks");
		description.add(ChatColor.GREEN + "(Disponible toutes les 24 heures.)");
		description.add("");
		description.add("§f§lPrix : 500 000€");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemPillage() {
		ItemStack i = new ItemStack(Material.MONSTER_EGG, 1, (short) 50);
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lPillage");
		description.add(ChatColor.YELLOW + " ");
		description.add(ChatColor.DARK_GREEN + "Ce kit contient:");
		description.add(ChatColor.YELLOW + "-Oeuf de Creeper [x3]");
		description.add(ChatColor.YELLOW + "-TNT [x10]");
		description.add(ChatColor.YELLOW + "-Briquet Unbreaking 5");
		description.add(ChatColor.YELLOW + "-Piston collant [x2]");
		description.add(ChatColor.YELLOW + "-Piston [x5]");
		description.add(ChatColor.YELLOW + "-Torche de redstone [x5]");
		description.add(ChatColor.GREEN + "(Disponible toutes les 24 heures.)");
		description.add("");
		description.add("§f§lPrix : 500 000€");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemAlchimiste() {
		ItemStack i = new ItemStack(new ItemStack(Material.BREWING_STAND_ITEM, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lAlchimiste");
		description.add(ChatColor.YELLOW + " ");
		description.add(ChatColor.DARK_GREEN + "Ce kit contient:");
		description.add(ChatColor.YELLOW + "-Stand d'alchimie [x3]");
		description.add(ChatColor.YELLOW + "-Larme de Ghast [x8]");
		description.add(ChatColor.YELLOW + "-Poudre de feu [x16]");
		description.add(ChatColor.YELLOW + "-Sucre [x16]");
		description.add(ChatColor.YELLOW + "-Pastheque dorée [x16]");
		description.add(ChatColor.YELLOW + "-Magma cream [x16]");
		description.add(ChatColor.YELLOW + "-Carrote d'or [x16]");
		description.add(ChatColor.YELLOW + "-Oeuil d'arraigné fermenté [x16]");
		description.add(ChatColor.YELLOW + "-Flacon [x128]");
		description.add(ChatColor.GREEN + "(Disponible toutes les 24 heures.)");
		description.add("");
		description.add("§f§lPrix : 500 000€");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemConstruct() {
		ItemStack i = new ItemStack(new ItemStack(Material.SEA_LANTERN, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lConstructeur");
		description.add(ChatColor.YELLOW + " ");
		description.add(ChatColor.DARK_GREEN + "Ce kit contient:");
		description.add(ChatColor.YELLOW + "-Obsidienne [x128]");
		description.add(ChatColor.YELLOW + "-Seau d'eau [x2]");
		description.add(ChatColor.YELLOW + "-Seau de lave [x2]");
		description.add(ChatColor.YELLOW + "-Lanterne des mers [x8]");
		description.add(ChatColor.YELLOW + "-Glace compactée [x16]");
		description.add(ChatColor.YELLOW + "-Pierre sculptée [x32]");
		description.add(ChatColor.YELLOW + "-Prismarin [x32]");
		description.add(ChatColor.YELLOW + "-Verre [x64]");
		description.add(ChatColor.YELLOW + "-Argile durcie [x64]");
		description.add(ChatColor.GREEN + "(Disponible toutes les 24 heures.)");
		description.add("");
		description.add("§f§lPrix : 50 000€");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	public static ItemStack getItemObsi() {
		ItemStack i = new ItemStack(new ItemStack(Material.OBSIDIAN, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lObsidienne");
		description.add(ChatColor.YELLOW + " ");
		description.add(ChatColor.DARK_GREEN + "Ce kit contient:");
		description.add(ChatColor.YELLOW + "-Obsidienne [x576] (9 stacks)");
		description.add(ChatColor.GREEN + "(Disponible toutes les 24 heures.)");
		description.add("");
		description.add("§f§lPrix : 500 000€");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemPvp() {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lPvP");
		im.addEnchant(Enchantment.PROTECTION_ENVIRONMENTAL, 4, true);
		im.addEnchant(Enchantment.DURABILITY, 3, true);
		description.add(ChatColor.YELLOW + " ");
		description.add(ChatColor.DARK_GREEN + "Ce kit contient:");
		description.add(ChatColor.YELLOW + "-Armure protection 4 unbreaking 3");
		description.add(ChatColor.YELLOW + "-Epée sharpness 5, fireaspect 2");
		description.add(ChatColor.YELLOW + "-Armure p4 u3");
		description.add(ChatColor.YELLOW + "-Pomme d'or [x32]");
		description.add(ChatColor.YELLOW + "-Pomme cheat [x5]");
		description.add(ChatColor.GREEN + "(Disponible tous les 5 jours.)");
		description.add("");
		description.add("§f§lPrix : 850 000€");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}


	private static ItemStack getItemConfirm() {
		ItemStack i = new ItemStack(new ItemStack(Material.WOOL, 1, (short) 5));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§2§lCONFIRMER");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour confirmer l'achat !");
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(KitsGui.getName())) {
			switch (current.getType()) {
			case NETHER_STAR:
				e.setCancelled(true);
				p.openInventory(Menu.Menu);
				break;
			case DIAMOND_PICKAXE:
				KitsGui.setItem(14, getItemConfirm());
				e.setCancelled(true);
				break;
			case MONSTER_EGG:
				KitsGui.setItem(15, getItemConfirm());
				e.setCancelled(true);
				break;
			case BREWING_STAND_ITEM:
				KitsGui.setItem(23, getItemConfirm());
				e.setCancelled(true);
				break;
			case SEA_LANTERN:
				KitsGui.setItem(24, getItemConfirm());
				e.setCancelled(true);
				break;
			case OBSIDIAN:
				KitsGui.setItem(22, getItemConfirm());
				e.setCancelled(true);
				break;
			case DIAMOND_CHESTPLATE:
				KitsGui.setItem(13, getItemConfirm());
				e.setCancelled(true);
				break;
			case WOOL:
				switch (e.getSlot()) {
				case 14:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("essentials.kits.mineur")) {
						p.sendMessage("§cVous avez déjà ce kit !");
					}else if(Main.econ.getBalance(p) >= 500000) {
						Main.econ.withdrawPlayer(p, 500000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add essentials.kits.mineur");
					} else {
						p.sendMessage("§cVous n'avez pas suffisament d'argent !");
					}
					break;
				case 15:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("essentials.kits.pillage")) {
						p.sendMessage("§cVous avez déjà ce kit !");
					}else if(Main.econ.getBalance(p) >= 500000) {
						Main.econ.withdrawPlayer(p, 500000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add essentials.kits.pillage");
					}else{
						p.sendMessage("§cVous n'avez pas suffisament d'argent !");
					}
					break;
				case 23:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("essentials.kits.alchimiste")) {
						p.sendMessage("§cVous avez déjà ce kit !");
					}else if(Main.econ.getBalance(p) >= 500000) {
						Main.econ.withdrawPlayer(p, 500000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add essentials.kits.alchimiste");
					}else{
						p.sendMessage("§cVous n'avez pas suffisament d'argent !");
					}
					break;
				case 24:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("essentials.kits.constructeur")) {
						p.sendMessage("§cVous avez déjà ce kit !");
					}else if(Main.econ.getBalance(p) >= 500000) {
						Main.econ.withdrawPlayer(p, 500000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add essentials.kits.constructeur");
					}else{
						p.sendMessage("§cVous n'avez pas suffisament d'argent !");
					}
					break;
				case 22:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("essentials.kits.obsidienne")) {
						p.sendMessage("§cVous avez déjà ce kit !");
					}else if(Main.econ.getBalance(p) >= 500000) {
						Main.econ.withdrawPlayer(p, 500000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add essentials.kits.obsidienne");
					}else{
						p.sendMessage("§cVous n'avez pas suffisament d'argent !");
					}
					break;
				case 13:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("essentials.kits.pvp")) {
						p.sendMessage("§cVous avez déjà ce kit !");
					}else if(Main.econ.getBalance(p) >= 850000) {
						Main.econ.withdrawPlayer(p, 850000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add essentials.kits.pvp");
					}else{
						p.sendMessage("§cVous n'avez pas suffisament d'argent !");
					}
					break;
				}
				break;
			default:
				e.setCancelled(true);
				p.closeInventory();
				break;

			}
		}

	}
	@EventHandler
	public void onOpen2(InventoryOpenEvent e)
	{

		if (e.getInventory().getName().equalsIgnoreCase(KitsGui.getName()))
		{

			Player player = (Player)e.getPlayer();
			ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			SkullMeta playerheadmeta = (SkullMeta)playerhead.getItemMeta();
			playerheadmeta.setOwner(player.getName());
			playerheadmeta.setDisplayName("§6§lInformations");
			playerhead.setItemMeta(playerheadmeta);
			ArrayList<String> description = new ArrayList<String>();
			description.add("§6");
			description.add("➢§6§lArgent §e: §6§l" + Main.econ.getBalance(player)+"$");
			playerheadmeta.setLore(description);
			playerhead.setItemMeta(playerheadmeta);
			KitsGui.setItem(0, playerhead);
		}
	}
	@EventHandler
	public void onOpen(InventoryOpenEvent e1) {
		Inventory inv = e1.getInventory();
		if (inv.getName().equalsIgnoreCase(KitsGui.getName())) {
			KitsGui.setItem(27, getItemBack());
			KitsGui.setItem(14, getItemMineur());
			KitsGui.setItem(15, getItemPillage());
			KitsGui.setItem(23, getItemAlchimiste());
			KitsGui.setItem(24, getItemConstruct());
			KitsGui.setItem(13, getItemPvp());
			KitsGui.setItem(22, getItemObsi());
		}
	}
}
