package fr.parafight.boutique;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import fr.parafight.Main;
import net.milkbowl.vault.economy.Economy;

public class Grades implements Listener {

	public static Economy econ = null;

	public static Inventory Grades = Bukkit.createInventory(null, 36, "§dGrades !");


	private static ItemStack getItemGenin(int stat) {
		ItemStack i = new ItemStack(new ItemStack(Material.LEATHER_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§b§lCaporal");
		description.add(ChatColor.YELLOW + "");
		description.add("§e> Prefixe : §bCaporal");
		description.add("");
		description.add("§6§lAvantages:");
		description.add("");
		description.add("§e> 4 résidences");
		description.add("§e> /feed");
		description.add("§e> /craft");
		description.add("§e> /hat");
		description.add("§e> /repair (500$/réparation)");
		description.add("§e> Accès au kit Caporal");
		description.add("");
		description.add("§f§lPrix : 500 000$");
		if (stat == 0)
			description.add("§a➥ Clic pour acheter !");
		if (stat == 1)
			description.add("§c➥Vous devez posséder le grade précédent !");
		if (stat == 2)
			description.add("§l§a➥Déjà acheté !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemChunin(int stat) {
		ItemStack i = new ItemStack(new ItemStack(Material.IRON_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§2§lSergent");
		description.add(ChatColor.YELLOW + "");
		description.add("§e> Prefixe : §2Sergent");
		description.add("");
		description.add("§6§lAvantages :");
		description.add("");
		description.add("§e> 6 résidences");
		description.add("§e> /repair (250$/réparation)");
		description.add("§e> /enderchest");
		description.add("§e> Accès au kit Sergent");
		description.add("");
		description.add("+ Tout les avantages du grade précédant.");
		description.add("");
		description.add("§f§lPrix : 1 000 000$");
		if (stat == 0)
			description.add("§a➥ Clic pour acheter !");
		if (stat == 1)
			description.add("§c➥Vous devez posséder le grade précédent !");
		if (stat == 2)
			description.add("§l§a➥Déjà acheté !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	
	private static ItemStack getItemJonin(int stat) {
		ItemStack i = new ItemStack(new ItemStack(Material.GOLD_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lMajor");
		description.add(ChatColor.YELLOW + "");
		description.add("§e> Prefixe : §6Major");
		description.add("");
		description.add("§6§lAvantages :");
		description.add("");
		description.add("§e> 8 résidences");
		description.add("§e> /repair (100$/réparation)");
		description.add("§e> /near");
		description.add("§e> Ecire en §ac§bo§cu§dl§ee§2u§4r§6s");
		description.add("§e> Accès au kit Major");
		description.add("");
		description.add("+ Tout les avantages du grade précédant.");
		description.add("");
		description.add("§f§lPrix : 1 500 000$");
		if (stat == 0)
			description.add("§a➥ Clic pour acheter !");
		if (stat == 1)
			description.add("§c➥Vous devez posséder le grade précédent !");
		if (stat == 2)
			description.add("§l§a➥Déjà acheté !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	
	private static ItemStack getItemKage(int stat) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§d§lCapitaine");
		description.add(ChatColor.YELLOW + "");
		description.add("§e> Prefixe : §dCapitaine");
		description.add("");
		description.add("§6§lAvantages :");
		description.add("");
		description.add("§e> 10 résidences");
		description.add("§e> /repair (Gratuit)");
		description.add("§e> /skin");
		description.add("§e> /nick");
		description.add("§e> /invsee");
		description.add("§e> /back (à l'endroit de la dernière téléportation)");
		description.add("§e> Accès au kit Capitaine");
		description.add("");
		description.add("+ Tout les avantages du grade précédant.");
		description.add("");
		description.add("§f§lPrix : 15€ IRL");
		if (stat == 0)
			description.add("§a➥ Clic pour acheter !");
		if (stat == 2)
			description.add("§l§a➥Déjà acheté !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemConfirm() {
		ItemStack i = new ItemStack(new ItemStack(Material.WOOL, 1, (short) 5));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§2§lCONFIRMER");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour confirmer l'achat !");
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(Grades.getName())) {

			switch (current.getType()) {
			case NETHER_STAR:
				e.setCancelled(true);
				p.openInventory(Menu.Menu);
				break;
			case LEATHER_CHESTPLATE:
				Grades.setItem(13, getItemConfirm());
				e.setCancelled(true);
				break;
			case IRON_CHESTPLATE:
				Grades.setItem(14, getItemConfirm());
				e.setCancelled(true);
				break;
			case GOLD_CHESTPLATE:
				Grades.setItem(15, getItemConfirm());
				e.setCancelled(true);
				break;
			case DIAMOND_CHESTPLATE:
				Grades.setItem(16, getItemConfirm());
				e.setCancelled(true);
				break;
			case WOOL:
				switch (e.getSlot()) {
				case 13:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("capitaine")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("major")){
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("sergent")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("caporal")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("soldat")) {
						if (Main.econ.getBalance(p) >= 500000) {
							Main.econ.withdrawPlayer(p, 500000);
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
									"pex user " + p.getName() + " group set Caporal");
							Bukkit.broadcastMessage("§5§l[Boutique] §7§l>> §dLe joueur §5" + p.getName() + "§d est passé §bCaporal §d!");
						} else {
							p.sendMessage("§cVous n'avez pas assez d'argent !");
						}
					}
					break;
				case 14:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("capitaine")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("major")){
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("sergent")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("caporal")) {
						if (Main.econ.getBalance(p) >= 1000000) {
							Main.econ.withdrawPlayer(p, 1000000);
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
									"pex user " + p.getName() + " group set Sergent");
							Bukkit.broadcastMessage("§5§l[Boutique] §7§l>> §dLe joueur §5" + p.getName() + "§d est passé §2Sergent §d!");
						} else {
							p.sendMessage("§cVous n'avez pas assez d'argent !");
						}
					} else if (p.hasPermission("soldat")) {
						p.sendMessage("§cVous devez d'abord acheter le grade précédent !");
					}
					break;
				case 15:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("capitaine")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("major")){
						p.sendMessage("§cVous avez déjà ce grade !");
					} else if (p.hasPermission("sergent")) {
						if (Main.econ.getBalance(p) >= 1500000) {
							Main.econ.withdrawPlayer(p, 1500000);
							Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
									"pex user " + p.getName() + " group set Major");
							Bukkit.broadcastMessage("§5§l[Boutique] §7§l>> §dLe joueur §5" + p.getName() + "§d est passé §6Major §d!");
						} else {
							p.sendMessage("§cVous n'avez pas assez d'argent !");
						}
					} else if (p.hasPermission("soldat")) {
						p.sendMessage("§cVous devez d'abord acheter le grade précédent !");
					}
					break;
				case 16:
					e.setCancelled(true);
					p.closeInventory();
					if (p.hasPermission("capitaine")) {
						p.sendMessage("§cVous avez déjà ce grade !");
					} else {
						p.sendMessage("§dAchetez le grade depuis le site, ou contactez un fondateur.");
					}
					break;
				}
				break;
			default:
				e.setCancelled(true);
				p.closeInventory();
				break;

			}
		}
	}
	  @EventHandler
	  public void onOpen2(InventoryOpenEvent e)
	  {
		  
	    if (e.getInventory().getName().equalsIgnoreCase(Grades.getName()))
	    {
	    	
	      Player player = (Player)e.getPlayer();
	      ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
	      SkullMeta playerheadmeta = (SkullMeta)playerhead.getItemMeta();
	      playerheadmeta.setOwner(player.getName());
	      playerheadmeta.setDisplayName("§6§lInformations");
	      playerhead.setItemMeta(playerheadmeta);
	      ArrayList<String> description = new ArrayList<String>();
	      description.add("§6");
	      description.add("➢§6§lArgent §e: §6§l" + Main.econ.getBalance(player)+"$");
	      playerheadmeta.setLore(description);
	      playerhead.setItemMeta(playerheadmeta);
	      Grades.setItem(0, playerhead);
	    }
	  }
	@EventHandler
	public void onOpen(InventoryOpenEvent e1) {
		Inventory inv = e1.getInventory();
		Player p = (Player) e1.getPlayer();
		if (inv.getName().equalsIgnoreCase(Grades.getName())) {
			Grades.setItem(27, getItemBack());
			if (p.hasPermission("soldat")) {

				Grades.setItem(13, getItemGenin(0));
				Grades.setItem(14, getItemChunin(1));
				Grades.setItem(15, getItemJonin(1));
				Grades.setItem(16, getItemKage(0));
			}
			if (p.hasPermission("caporal")) {
				Grades.setItem(13, getItemGenin(2));
				Grades.setItem(14, getItemChunin(0));
				Grades.setItem(15, getItemJonin(1));
				Grades.setItem(16, getItemKage(0));
			}
			if (p.hasPermission("sergent")) {
				Grades.setItem(13, getItemGenin(2));
				Grades.setItem(14, getItemChunin(2));
				Grades.setItem(15, getItemJonin(0));
				Grades.setItem(16, getItemKage(0));
				
			}
			if (p.hasPermission("major")) {
				Grades.setItem(13, getItemGenin(2));
				Grades.setItem(14, getItemChunin(2));
				Grades.setItem(15, getItemJonin(2));
				Grades.setItem(16, getItemKage(0));
			}
			if (p.hasPermission("capitaine")) {
				Grades.setItem(13, getItemGenin(2));
				Grades.setItem(14, getItemChunin(2));
				Grades.setItem(15, getItemJonin(2));
				Grades.setItem(16, getItemKage(2));
			}
		}

	}

}

