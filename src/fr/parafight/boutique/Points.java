package fr.parafight.boutique;

import java.util.ArrayList;
import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import fr.parafight.Main;
import fr.parafight.factions.FactionsSql;
import net.milkbowl.vault.economy.Economy;

public class Points implements Listener{

	public static Economy econ = null;

	public static Inventory Points = Bukkit.createInventory(null, 27, "§6Points Faction !");

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}

	public ItemStack getItem(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}
	private static ItemStack getItemConfirm() {
		ItemStack i = new ItemStack(new ItemStack(Material.WOOL, 1, (short)5));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§2§lCONFIRMER");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour confirmer l'achat !");
		i.setItemMeta(im);
		return i;
	}



	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction f = fp.getFaction();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();
		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(Points.getName())) {
			switch (current.getType()) {
			case NETHER_STAR:
				e.setCancelled(true);
				p.openInventory(Menu.Menu);
				break;
			case DOUBLE_PLANT:
				if(e.getSlot() == 11){
					Points.setItem(11, getItemConfirm());
				}
				if(e.getSlot() == 13){
					Points.setItem(13, getItemConfirm());
				}
				if(e.getSlot() == 15){
					Points.setItem(15, getItemConfirm());
				}
				e.setCancelled(true);
				break;
			case WOOL:
				e.setCancelled(true);
				p.closeInventory();
				switch(e.getSlot()){
				case 11:
					if(Main.econ.getBalance(p) >= 2500){
						Main.econ.withdrawPlayer(p, 2500);
						FactionsSql.setFPoints(f, FactionsSql.getFPoints(f) + 1);
						p.sendMessage("§a1 point a été ajouté à votre Faction !");
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 13:
					if(Main.econ.getBalance(p) >= 12500){
						Main.econ.withdrawPlayer(p, 12500);
						FactionsSql.setFPoints(f, FactionsSql.getFPoints(f) + 1);
						p.sendMessage("§a5 points ont été ajoutés à votre Faction !");
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 15:
					if(Main.econ.getBalance(p) >= 25000){
						Main.econ.withdrawPlayer(p, 25000);
						FactionsSql.setFPoints(f, FactionsSql.getFPoints(f) + 10);
						p.sendMessage("§a10 points ont été ajoutés à votre Faction !");
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				default:
					break;
				}
				break;
			default:
				e.setCancelled(true);
				p.closeInventory();
				break;
			}
		}
	}

	@EventHandler
	public void onOpen2(InventoryOpenEvent e) {

		if (e.getInventory().getName().equalsIgnoreCase(Points.getName())) {
			Player player = (Player) e.getPlayer();
			FPlayer fp = FPlayers.getInstance().getByPlayer(player);
			ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta playerheadmeta = (SkullMeta) playerhead.getItemMeta();
			playerheadmeta.setOwner(player.getName());
			playerheadmeta.setDisplayName("§6§lInformations");
			playerhead.setItemMeta(playerheadmeta);
			ArrayList<String> description = new ArrayList<String>();
			description.add("§6");
			description.add("➢§6§lArgent §e: §6§l" + Main.econ.getBalance(player) + "$");
			description.add("➢§6§lPoints de la Faction : §e: §6§l" + FactionsSql.getFPoints(fp.getFaction()) + "points");
			playerheadmeta.setLore(description);
			playerhead.setItemMeta(playerheadmeta);
			Points.setItem(0, playerhead);
		}
	}

	@EventHandler
	public void onOpen(InventoryOpenEvent e1) {
		Inventory inv = e1.getInventory();
		if (inv.getName().equalsIgnoreCase(Points.getName())){
			Points.setItem(18, getItemBack());
			Points.setItem(11, getItem(Material.DOUBLE_PLANT, "§6§l1 Point", 1, 0, "§f§lPrix : 2500€", true));
			Points.setItem(13, getItem(Material.DOUBLE_PLANT, "§6§l5 Point", 5, 0, "§f§lPrix : 12500€", true));
			Points.setItem(15, getItem(Material.DOUBLE_PLANT, "§6§l10 Point", 10, 0, "§f§lPrix : 25000€", true)); 
		}

	}
}


