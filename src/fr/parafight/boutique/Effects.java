package fr.parafight.boutique;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.parafight.Main;
import net.milkbowl.vault.economy.Economy;

public class Effects implements Listener{

	public static Economy econ = null;


	public static Inventory Effects = Bukkit.createInventory(null, 36, "§dEffects !");

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemHaste()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.GOLD_PICKAXE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lHaste I");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§6Haste I");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 50000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemHaste2()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.GOLD_PICKAXE, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.DURABILITY, 1, true);
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lHaste II");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lHaste II");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 100000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemForce()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.BLAZE_POWDER, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lForce I");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lForce I");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 50000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemAntiFire()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.MAGMA_CREAM, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRésistance au Feu");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lRésistance au Feu I");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 50000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemSpeed()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.SUGAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lSpeed I");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lSpeed I");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 50000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemSpeed2()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.SUGAR, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.DURABILITY, 1, true);
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lSpeed II");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lSpeed II");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 75000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemResist()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.CHAINMAIL_CHESTPLATE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRésistance I");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lRésistance I");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 75000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemRegen()
	{
		ItemStack i = new ItemStack(new ItemStack(Material.GHAST_TEAR, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.DURABILITY, 1, true);
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRégénération I");
		description.add(ChatColor.YELLOW+"");
		description.add("§ePotion : §6§lRégénération I");
		description.add("§eTemps : §l§630 minutes");
		description.add("");
		description.add("§f§lPrix : 150 000$"); 
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}
	private static ItemStack getItemConfirm() {
		ItemStack i = new ItemStack(new ItemStack(Material.WOOL, 1, (short)5));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§2§lCONFIRMER");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour confirmer l'achat !");
		i.setItemMeta(im);
		return i;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player)e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if(current == null) return;
		if(inv.getName().equalsIgnoreCase(Effects.getName())){

			switch (current.getType()) {
			case NETHER_STAR:
				e.setCancelled(true);
				p.openInventory(Menu.Menu);
				break;
			case GOLD_PICKAXE:
				if(e.getSlot() == 12){
					Effects.setItem(12, getItemConfirm());
				}
				if(e.getSlot() == 21){
					Effects.setItem(21, getItemConfirm());
				}
				e.setCancelled(true);
				break;
			case BLAZE_POWDER:
				Effects.setItem(13, getItemConfirm());
				e.setCancelled(true);
				break;
			case MAGMA_CREAM:
				Effects.setItem(22, getItemConfirm());
				e.setCancelled(true);
				break;
			case SUGAR:
				if(e.getSlot() == 14){
					Effects.setItem(14, getItemConfirm());
				}
				if(e.getSlot() == 23){
					Effects.setItem(23, getItemConfirm());
				}
				e.setCancelled(true);
				break;
			case CHAINMAIL_CHESTPLATE:
				Effects.setItem(15, getItemConfirm());
				e.setCancelled(true);
				break;
			case GHAST_TEAR:
				Effects.setItem(24, getItemConfirm());
				e.setCancelled(true);
				break;
			case WOOL:
				e.setCancelled(true);
				p.closeInventory();
				switch(e.getSlot()){
				case 12:
					if(Main.econ.getBalance(p) >= 50000){
						Main.econ.withdrawPlayer(p,50000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 600*60, 0, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 21:
					if(Main.econ.getBalance(p) >= 100000){
						Main.econ.withdrawPlayer(p,100000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, 600*60, 1, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 13:
					if(Main.econ.getBalance(p) >= 50000){
						Main.econ.withdrawPlayer(p,50000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 600*60, 0, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 22:
					if(Main.econ.getBalance(p) >= 50000){
						Main.econ.withdrawPlayer(p,50000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.FIRE_RESISTANCE, 600*60, 0, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 14:
					if(Main.econ.getBalance(p) >= 50000){
						Main.econ.withdrawPlayer(p,50000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600*60, 0, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 23:
					if(Main.econ.getBalance(p) >= 75000){
						Main.econ.withdrawPlayer(p, 75000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 600*60, 1, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 15:
					if(Main.econ.getBalance(p) >= 75000){
						Main.econ.withdrawPlayer(p,75000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 600*60, 0, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				case 24:
					if(Main.econ.getBalance(p) >= 150000){
						Main.econ.withdrawPlayer(p, 150000);
						p.addPotionEffect(new PotionEffect(PotionEffectType.REGENERATION, 600*60, 0, true, false));
					}else{
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					break;
				}
				break;
			default:
				e.setCancelled(true);
				p.closeInventory();
				break;

			}
		}	
	}
	@EventHandler
	public void onOpen2(InventoryOpenEvent e)
	{

		if (e.getInventory().getName().equalsIgnoreCase(Effects.getName()))
		{

			Player player = (Player)e.getPlayer();
			ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short)3);
			SkullMeta playerheadmeta = (SkullMeta)playerhead.getItemMeta();
			playerheadmeta.setOwner(player.getName());
			playerheadmeta.setDisplayName("§6§lInformations");
			playerhead.setItemMeta(playerheadmeta);
			ArrayList<String> description = new ArrayList<String>();
			description.add("§6");
			description.add("➢§6§lArgent §e: §6§l" + Main.econ.getBalance(player)+"$");
			playerheadmeta.setLore(description);
			playerhead.setItemMeta(playerheadmeta);
			Effects.setItem(0, playerhead);
		}
	}
	@EventHandler
	public void onOpen(InventoryOpenEvent e1){
		Inventory inv = e1.getInventory();
		if(inv.getName().equalsIgnoreCase(Effects.getName())){
			Effects.setItem(27, getItemBack());
			Effects.setItem(12, getItemHaste());
			Effects.setItem(21, getItemHaste2());
			Effects.setItem(13, getItemForce());
			Effects.setItem(22, getItemAntiFire());
			Effects.setItem(14, getItemSpeed());
			Effects.setItem(23, getItemSpeed2());
			Effects.setItem(15, getItemResist());
			Effects.setItem(24, getItemRegen());		
		}



	}

}

