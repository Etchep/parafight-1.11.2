package fr.parafight.boutique;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;

import fr.parafight.Main;
import net.milkbowl.vault.economy.Economy;

public class Divers implements Listener {

	public static Economy econ = null;


	public static Inventory Divers = Bukkit.createInventory(null, 36, "§dDivers !");

	private static ItemStack getItemBack() {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lRetour au menu");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour retourner au menu !");
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemPeche() {
		ItemStack i = new ItemStack(new ItemStack(Material.FISHING_ROD, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.LURE, 5, true);
		im.addEnchant(Enchantment.DURABILITY, 5, true);
		im.addEnchant(Enchantment.LUCK, 10, true);
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lCanne à pêche");
		description.add(ChatColor.YELLOW + "");
		description.add("");
		description.add("§f§lPrix : 150 000$");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getCanne() {
		ItemStack i = new ItemStack(new ItemStack(Material.FISHING_ROD, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.LURE, 5, true);
		im.addEnchant(Enchantment.DURABILITY, 5, true);
		im.addEnchant(Enchantment.LUCK, 10, true);
		im.setDisplayName("§6§lCanne à pêche");
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemHache() {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_AXE, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.DIG_SPEED, 5, true);
		im.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
		im.addEnchant(Enchantment.DURABILITY, 3, true);
		im.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§6§lHache de combat");
		description.add(ChatColor.YELLOW + "");
		description.add("");
		description.add("§f§lPrix : 150 000$");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getHache() {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_AXE, 1));
		ItemMeta im = i.getItemMeta();
		im.addEnchant(Enchantment.DIG_SPEED, 5, true);
		im.addEnchant(Enchantment.DAMAGE_ALL, 5, true);
		im.addEnchant(Enchantment.DURABILITY, 3, true);
		im.addEnchant(Enchantment.FIRE_ASPECT, 1, true);
		im.setDisplayName("§6§lHache de combat");
		i.setItemMeta(im);
		return i;
	}
	
	private static ItemStack getItemCoffre() {
		ItemStack i = new ItemStack(new ItemStack(Material.CHEST, 1));
		ItemMeta im = i.getItemMeta();
		im.setDisplayName("§6§lCommande /pc");
		ArrayList<String> description = new ArrayList<String>();
		description.add(ChatColor.YELLOW + "");
		description.add("§eCette commande ouvre un inventaire");
		description.add("§ede 54 place pour y stocker vos items.");
		description.add("");
		description.add("§f§lPrix : 250 000$");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	private static ItemStack getItemConfirm() {
		ItemStack i = new ItemStack(new ItemStack(Material.WOOL, 1, (short) 5));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§2§lCONFIRMER");
		description.add(ChatColor.YELLOW + "");
		description.add("§a§lClic pour confirmer l'achat !");
		i.setItemMeta(im);
		return i;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();
		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(Divers.getName())) {

			switch (current.getType()) {
			case NETHER_STAR:
				e.setCancelled(true);
				p.openInventory(Menu.Menu);
				break;
			case FISHING_ROD:
				Divers.setItem(20, getItemConfirm());
				e.setCancelled(true);
				break;
			case DIAMOND_AXE:
				Divers.setItem(24, getItemConfirm());
				e.setCancelled(true);
				break;
			case CHEST:
				Divers.setItem(13, getItemConfirm());
				e.setCancelled(true);
				break;
			case WOOL:
				switch (e.getSlot()) {
				case 13:
					if (Main.econ.getBalance(p) >= 250000) {
						Main.econ.withdrawPlayer(p, 250000);
						Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
								"pex user " + p.getName() + " add parafight.privatechest");
					} else {
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 20:
					if (Main.econ.getBalance(p) >= 125000) {
						Main.econ.withdrawPlayer(p, 125000);
						p.getInventory().addItem(getCanne());
					} else {
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 24:
					if (Main.econ.getBalance(p) >= 100000) {
						Main.econ.withdrawPlayer(p, 100000);
						p.getInventory().addItem(getHache());
					} else {
						p.sendMessage("§cVous n'avez pas assez d'argent !");
					}
					e.setCancelled(true);
					p.closeInventory();
					break;
				}

				break;
			default:
				e.setCancelled(true);
				p.closeInventory();
				break;

			}
		}
	}

	@EventHandler
	public void onOpen2(InventoryOpenEvent e) {

		if (e.getInventory().getName().equalsIgnoreCase(Divers.getName())) {

			Player player = (Player) e.getPlayer();
			ItemStack playerhead = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
			SkullMeta playerheadmeta = (SkullMeta) playerhead.getItemMeta();
			playerheadmeta.setOwner(player.getName());
			playerheadmeta.setDisplayName("§6§lInformations");
			playerhead.setItemMeta(playerheadmeta);
			ArrayList<String> description = new ArrayList<String>();
			description.add("§6");
			description.add("➢§6§lArgent §e: §6§l" + Main.econ.getBalance(player) + "$");
			playerheadmeta.setLore(description);
			playerhead.setItemMeta(playerheadmeta);
			Divers.setItem(0, playerhead);
		}
	}

	@EventHandler
	public void onOpen(InventoryOpenEvent e1) {
		Inventory inv = e1.getInventory();
		if (inv.getName().equalsIgnoreCase(Divers.getName())) {
			Divers.setItem(27, getItemBack());
			Divers.setItem(13, getItemCoffre());
			Divers.setItem(20, getItemPeche());
			Divers.setItem(24, getItemHache());

		}

	}

}

