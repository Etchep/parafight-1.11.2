package fr.parafight.boutique;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class CommandBoutique implements CommandExecutor {

	@Override
	  public boolean onCommand(CommandSender sender, Command arg1, String label, String[] args)
	  {
	    if ((sender instanceof Player)) {
	      Player p = (Player)sender;
	      if (label.equalsIgnoreCase("boutique"))
	        p.openInventory(Menu.Menu);
	    }
	    return false;
	  }

}