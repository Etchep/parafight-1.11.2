package fr.parafight.sql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

public class SqlConnection {

	public static void updateAll(Player player, int mineur, int chasseur, int kills, int pecheur, int farmeur, Inventory inv, double bank)
	{
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("UPDATE joueurs SET privateinv = ?, mineur = ?, farmeur = ?, assaillant = ?, pecheur = ?, chasseur = ?, bank = ?  WHERE uuid = ?");
			q.setString(1, fr.parafight.inventories.BukkitSerialization.toBase64(inv));
			q.setInt(2, mineur);
			q.setInt(3, farmeur);
			q.setInt(4, kills);
			q.setInt(5, pecheur);
			q.setInt(6, chasseur);
			q.setDouble(7, bank);;
			q.setString(8, player.getUniqueId().toString());
			q.executeUpdate();
			q.close();
			connection.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public static boolean hasAccount(Player player) {

		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT uuid FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet resultat = q.executeQuery();
			boolean hasAccount = resultat.next();
			q.close();
			connection.close();
			return hasAccount;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		//SELECT
		return false;
	}
	public static void createAccount(Player player) {
		if(!hasAccount(player)){
			//INSERT
			try {
				Connection connection = DbManager.BD.getDbAccess().getConnection();
				PreparedStatement q = connection.prepareStatement("INSERT INTO joueurs(pseudo ,uuid ,ip , privateinv, bank) VALUES (?,?,?,?,?)");
				q.setString(1, player.getName());
				q.setString(2, player.getUniqueId().toString());
				q.setString(3, "" + player.getAddress().getAddress());
				q.setString(4, "rO0ABXcEAAAANnBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcHBwcA==");
				q.setDouble(5, 250.0);
				q.execute();
				q.close();
				connection.close();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	public static Inventory getPrivateInv(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT privateinv FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			String Base64 = null;

			while(rs.next()){
				Base64 = rs.getString("privateinv");
			}

			q.close();
			connection.close();

			Inventory inv = fr.parafight.inventories.BukkitSerialization.fromBase64(Base64);
			return inv;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public static int getMineurExp(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT mineur FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			int nombre = 0;

			while(rs.next()){
				nombre = rs.getInt("mineur");
			}

			q.close();
			connection.close();


			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static int getFarmExp(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT farmeur FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			int nombre = 0;

			while(rs.next()){
				nombre = rs.getInt("farmeur");
			}

			q.close();
			connection.close();

			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}


	public static int getKillExp(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT assaillant FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			int nombre = 0;

			while(rs.next()){
				nombre = rs.getInt("assaillant");
			}

			q.close();
			connection.close();


			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}


	public static int getChasseurExp(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT chasseur FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			int nombre = 0;

			while(rs.next()){
				nombre = rs.getInt("chasseur");
			}

			q.close();
			connection.close();


			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static int getPecheurExp(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT pecheur FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			int nombre = 0;

			while(rs.next()){
				nombre = rs.getInt("pecheur");
			}

			q.close();
			connection.close();


			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static double getBankBalance(Player player){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT bank FROM joueurs WHERE uuid = ?");
			q.setString(1, player.getUniqueId().toString());
			ResultSet rs = q.executeQuery();
			double nombre = 0;

			while(rs.next()){
				nombre = rs.getDouble("bank");
			}

			q.close();
			connection.close();

			return nombre;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}

	public static ArrayList<String> getTopPseudo(){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT pseudo FROM joueurs ORDER BY bank DESC LIMIT 10") ;
			ResultSet rs = q.executeQuery();
			ArrayList<String> list = new ArrayList<>();
	         
	           
	            while(rs.next()){
	                list.add(rs.getString("pseudo"));
	            }
	            q.close();
	            connection.close();
	           
			
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	public static ArrayList<String> getTopMoney(){
		try {
			Connection connection = DbManager.BD.getDbAccess().getConnection();
			PreparedStatement q = connection.prepareStatement("SELECT bank FROM joueurs ORDER BY bank DESC LIMIT 10") ;
			ResultSet rs = q.executeQuery();
			ArrayList<String> list = new ArrayList<>();
	         
	           
	            while(rs.next()){
	                list.add(rs.getString("bank"));
	            }
	            q.close();
	            connection.close();
	           
			
			return list;
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}


}
