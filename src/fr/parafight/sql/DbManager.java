package fr.parafight.sql;

public enum DbManager {
	
	BD(new DbCredentials("localhost", "minecraft", "iK3ryPKDnxF1jeam", "minecraft", 3306));
	
	private DbAccess dbAccess;
	
	DbManager(DbCredentials credentials){
		this.dbAccess = new DbAccess(credentials);
	}
	
	public DbAccess getDbAccess(){
		return dbAccess;
	}
	
	
	public static void initAllDbConnections(){
		for(DbManager dbManager : values()){
			dbManager.dbAccess.initPool();
		}
	}
	
	public static void closeAllDbConnections(){
		for(DbManager dbManager : values()){
			dbManager.dbAccess.closePool();
		}
	}
	
	

}
