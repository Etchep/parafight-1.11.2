package fr.parafight.gui;

import java.util.Arrays;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class WarpsGui implements Listener{
	
	public static Inventory Warps = Bukkit.createInventory(null, 54, "�dWarps");

	public ItemStack getItem(Material material, String name, int quantity, int damage, String desc, boolean enchanted){
		ItemStack it = new ItemStack(material, quantity,  (short) damage);
		ItemMeta itM = it.getItemMeta();
		if(name != null )itM.setDisplayName(name);
		if(desc != null) itM.setLore(Arrays.asList(desc));
		if(enchanted == true){
			itM.addEnchant(Enchantment.DURABILITY, 1, true);
		}
		it.setItemMeta(itM);
		return it;
	}
	
	@EventHandler
	public void onOpen(InventoryOpenEvent e1){
		Inventory inv = e1.getInventory();
		if(inv.getName().equalsIgnoreCase(Warps.getName())){
			Warps.setItem(0, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(1, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(2, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(3, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(4, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(5, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(6, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(7, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(8, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(9, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(10, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(11, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(12, getItem(Material.GOLD_INGOT, "�6�lBanque", 1, 0, "�eCliquez pour acc�der � la banque !", true));
			Warps.setItem(13, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(14, getItem(Material.BEACON, "�6�lShop", 1, 0, "�eCliquez pour acc�der au shop !", true));
			Warps.setItem(15, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(16, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));	
			Warps.setItem(17, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(18, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(19, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(20, getItem(Material.DIAMOND_SWORD, "�6�lKOTH", 1, 0, "�eCliquez pour aller au KOTH !", true));	
			Warps.setItem(21, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(22, getItem(Material.DIAMOND_CHESTPLATE, "�6�lSortie Nord", 1, 0, "�eCliquez pour aller � la sortie nord!", true));
			Warps.setItem(23, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(24, getItem(Material.MAP, "�6�lR�glement", 1, 0, "�eCliquez pour acc�der au r�glement !", true));
			Warps.setItem(25, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(26, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(27, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(28, getItem(Material.NETHERRACK, "�6�lNether", 1, 0, "�eCliquez pour aller au Nether !", false));
			Warps.setItem(29, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(30, getItem(Material.EYE_OF_ENDER, "�6�lEnder", 1, 0, "�eCliquez pour aller dans l'end !", true));
			Warps.setItem(31, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(32, getItem(Material.TRIPWIRE_HOOK, "�6�lCaisses surprises", 1, 0, "�eCliquez pour aller au warp keys !", true));
			Warps.setItem(33, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(34, getItem(Material.ENDER_PEARL, "�6�lKOTH end", 1, 0, "�eCliquez pour aller au KOTH de l'end !", true));
			Warps.setItem(35, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(36, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(37, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(38, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(40, getItem(Material.EXP_BOTTLE, "�6�lEnchantement", 1, 0, "�eCliquez pour aller au warp enchantement !", false));
			Warps.setItem(39, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(41, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(42, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(43, getItem(Material.STAINED_GLASS_PANE, null, 1, 5, null, false));
			Warps.setItem(44, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(45, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(46, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(47, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(48, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(49, getItem(Material.STAINED_GLASS_PANE, "�4�lFERMER", 1, 14, "�cCliquez pour fermer le menu des warps.", false));
			Warps.setItem(50, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(51, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(52, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			Warps.setItem(53, getItem(Material.STAINED_GLASS_PANE, null, 1, 8, null, false));
			}
		}
	
	@EventHandler
	public void onInteract(InventoryClickEvent e){
		 Player p = (Player)e.getWhoClicked();
		 Inventory inv = e.getInventory();
		 ItemStack current = e.getCurrentItem();
		 
		 if(current == null) return;
		 if(inv.getName().equalsIgnoreCase(Warps.getName())){
			 e.setCancelled(true);
			 
			 switch (e.getSlot()) {
				case 12:
					p.chat("/ewarp banque");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 14:
					p.chat("/ewarp shop");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 20:
					p.chat("/ewarp koth");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 22:
					p.chat("/ewarp nord");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 24:
					p.chat("/ewarp regles");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 28:
					p.chat("/ewarp nether");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 30:
					p.chat("/ewarp end");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 32:
					p.chat("/ewarp keys");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 34:
					p.chat("/ewarp kothend");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 40:
					p.chat("/ewarp enchant");
					e.setCancelled(true);
					p.closeInventory();
					break;
				case 49:
					e.setCancelled(true);
					p.closeInventory();
					break;
			default:
				e.setCancelled(true);
				break;
			}
		 
	    }
	      
		
	}

}
