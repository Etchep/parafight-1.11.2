package fr.parafight.rtp;

import java.util.HashMap;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

public class RandomTp implements CommandExecutor, Listener {

	private HashMap<String, Long> cooldowns = new HashMap<String, Long>();
	int cooldownTime = 120;

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		if(sender instanceof Player){
			Player p = (Player)sender;
			if(cooldowns.containsKey(p.getName())){
				long secondsLeft = ((cooldowns.get(p.getName())/1000)+cooldownTime) - (System.currentTimeMillis()/1000);
				if(secondsLeft > 0) {
					p.sendMessage("�cPatientez "+ secondsLeft +" secondes pour r�utiliser un RTP !");
					return false;
				}else{
					RandomTeleport(p);
				}
			}else{
				RandomTeleport(p);
			}
		}
		return false;
	}

	public void RandomTeleport(Player player){
		player.sendMessage("�a�lT�l�portation !");
		player.teleport(RandomLocation());
		player.addPotionEffect(new PotionEffect(PotionEffectType.DAMAGE_RESISTANCE, 300, 10, true, false));
		if(cooldowns.containsKey(player.getName()))
			cooldowns.remove(player.getName());
		cooldowns.put(player.getName(), System.currentTimeMillis());
	}


	public static Location RandomLocation(){
		Random r = new Random();
		double x = -14800 + r.nextInt(29600);
		double z = -14800 + r.nextInt(29600);
		double y = 200;
		Location from = new Location(Bukkit.getWorld("world"),x,y,z);
		if(Math.abs(x) < 3000 && Math.abs(z) < 3000){
			from = RandomLocation();
		}
		return from;
	}

}
