package fr.parafight.ctf;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import fr.parafight.ctf.GameManager.GameState;

public class CTFCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		Player p = (Player)sender;
		if(args.length == 0){
			if(GameManager.CTF == GameState.GAME){
				p.sendMessage("�d�lInformations sur le CTF :");
				p.sendMessage("�d Position initiale du drapeau : �5�lX : "+ GameManager.loc.getX() + " / Z : "+ GameManager.loc.getZ() + "�d");
				p.sendMessage("�d Position de d�pot du drapeau : �5�lX : "+ GameManager.locdepot.getX() + " / Z : "+ GameManager.locdepot.getZ() + "�d");
				p.sendMessage("�d");
				p.sendMessage("�dInformations du joueur qui d�tient le drapeau :");
				if(GameManager.joueur != null){
					p.sendMessage("�d Pseudo du joueur : �5�l" + GameManager.joueur.getName());
					p.sendMessage("�d Position du joueur : �5�lX : "+ GameManager.joueur.getLocation().getX() + " / Y : "+ GameManager.joueur.getLocation().getY() + " / Z : "+ GameManager.joueur.getLocation().getZ() + "�d");
				}else{
					p.sendMessage("�5Le drapeu est toujours � sa position initiale");
				}
			}else{
				p.sendMessage("�cL'event n'est pas actif.");
			}
		}
		if(args.length == 1){
			if(args[0].equals("start")){
				if(sender.hasPermission("parafight.ctf")){
					if(GameManager.CTF != GameState.GAME){
						GameManager.StartCTF();
					}else{
						p.sendMessage("�cL'event est d�j� lanc�.");
					}
				}else{
					p.sendMessage("�cVous n'avez pas la permission.");
				}
			}
		}
		return false;
	}

}
