package fr.parafight.ctf;

import java.util.ArrayList;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;

import fr.parafight.Main;


public class GameManager implements Listener{
	
	public static enum GameState {
		GAME,NONE;
	}
	public static GameState CTF;
	
	private Main main;
	public GameManager(Main main) {
		this.main = main;
	}
	private static BukkitTask task;
	private int timer = 0;
	public static Player joueur = null;
	public static Location loc = new Location(Bukkit.getWorld("world"), -173, 65, 220);
	public static Location locdepot = new Location(Bukkit.getWorld("world"), 17, 81, 243);

	private ItemStack getItemFlag(){
		ItemStack i = new ItemStack(new ItemStack(Material.WOOL, 1, (short) 5));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("�2�lDRAPEAU");
		description.add("�a�lD�posez le dans la zone !");
		i.setItemMeta(im);
		return i;
	}
	
	@SuppressWarnings("deprecation")
	public static void StartCTF(){
		Bukkit.broadcastMessage("�d[CTF] �dL'�v�nement Capture de drapeau a �t� lanc� !");
		Bukkit.broadcastMessage("�d[CTF] �dL'objectif est d'apporter de drapeau � la position indiqu�e, ou d'emp�cher son d�tenteur actuel de le faire.");
		Bukkit.broadcastMessage("�d[CTF] �dLe drapeau est apparu � �5X : "+ loc.getX() + " / Z : "+ loc.getZ() + "�d.");
		loc.getBlock().setType(Material.WOOL);
		loc.getBlock().setData((byte)5);	
		CTF = GameState.GAME;
	}
	
	public void EndCTF(Player player){
		Bukkit.broadcastMessage("�d[CTF] �dLe joueur "+ player.getName() +" a r�ussi � d�poser le drapeau en "+ timer +" secondes ! GG � lui !");
		joueur.getInventory().remove(getItemFlag());
		timer = 0;
		joueur = null;
		CTF = GameState.NONE;
		task.cancel();
		Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
				"crate key " + player.getName() + " GoldenBox 1");
	}
	@SuppressWarnings("deprecation")
	public void ResetCTF(){
		Bukkit.broadcastMessage("�d[CTF] �dLe drapeau a retrouv� sa position initiale. (X : "+ loc.getX() + " / Z : "+ loc.getZ() + "�d)");
		joueur.getInventory().remove(getItemFlag());
		joueur = null;
		task.cancel();
		loc.getBlock().setType(Material.WOOL);
		loc.getBlock().setData((byte)5);	
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		if(e.getPlayer() == joueur){
			Bukkit.broadcastMessage("�d�l[CTF] �dLe d�tenteur du drapeau s'est d�connect� !");
			ResetCTF();
		}
	}
	@EventHandler
	public void onDeath(PlayerDeathEvent e){
		if(e.getEntity() instanceof Player){
			Player p = e.getEntity();
			if(p == joueur){
				Bukkit.broadcastMessage("�d�l[CTF] �dLe d�tenteur du drapeau s'est fait �liminer !");
				ResetCTF();
			}
		}
	}
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		if(p == joueur){
			if(e.getMessage().contains("ctf")){
				return;
			}else{
				p.sendMessage("�cVous ne pouvez pas �x�cuter de commande en possession du drapeau !");
				e.setCancelled(true);
			}
			
		}
	}
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		Player p = e.getPlayer();
		Block target = e.getClickedBlock();
		if(CTF == GameState.GAME){
				if(e.getAction() == Action.RIGHT_CLICK_BLOCK && target != null && target.getType() == Material.WOOL && target.getLocation().equals(loc)){
					if(p.getInventory().getItemInMainHand().getType() == Material.AIR){
						p.getInventory().setItemInMainHand(getItemFlag());
						Bukkit.broadcastMessage("�d[CTF] �5" + p.getName() + " vient de r�cup�rer le drapeau ! (/ctf pour avoir sa position en temps r�el)");
						p.sendMessage("�a�lVous devez d�poser le drapeau � : �5X : "+locdepot.getX()+" / Z : "+locdepot.getZ()+".");
						joueur = p;
						loc.getBlock().setType(Material.AIR);
						BukkitRunnable runnable = new BukkitRunnable() {
							@Override
							public void run() {
								timer++;
								if(joueur != null){
									Faction faction = null;
									Location loc = joueur.getLocation();
									FLocation fLoc = new FLocation(loc);
									faction = Board.getInstance().getFactionAt(fLoc);
									if(faction.isWarZone() == false){
										Bukkit.broadcastMessage("�d[CTF] Le d�tenteur du drapeau a voulu s'�chapper !");
										ResetCTF();
									}
								}
								
							}
						};
						task = runnable.runTaskTimer(main, 20L, 20L);
					
					}else{
					p.sendMessage("�cCliquez sur le drapeau sans item en main.");
					e.setCancelled(true);
				}
			}
			
			
			if(joueur == p){
				if(e.getAction() == Action.RIGHT_CLICK_BLOCK && target != null && target.getLocation().equals(locdepot)){
					if(p.getInventory().contains(getItemFlag())){
						EndCTF(p);
					}
				}
			}
		}
	}
	@EventHandler
	public void onDrop(PlayerDropItemEvent e){
		Player p = e.getPlayer();
		if(p == joueur){
			if(e.getItemDrop() != null && e.getItemDrop().getItemStack().getItemMeta().getDisplayName() == getItemFlag().getItemMeta().getDisplayName()){
				e.setCancelled(true);
				p.sendMessage("�cIl faut d�poser le drapeau dans la zone indiqu�e !");
			}
		}
	}
	@EventHandler
	public void onClick(InventoryClickEvent e){
		Player p = (Player) e.getWhoClicked();
		if(p == joueur){
			if(e.getCurrentItem() != null && e.getCurrentItem().getItemMeta().getDisplayName() == getItemFlag().getItemMeta().getDisplayName()){
				e.setCancelled(true);
				p.sendMessage("�cIl faut d�poser le drapeau dans la zone indiqu�e !");
			}
		}
	}
	@EventHandler
	public void onPlace(BlockPlaceEvent e){
		Player p = e.getPlayer();
		if(p == joueur){
			if(p.getInventory().getItemInMainHand() != null && p.getInventory().getItemInMainHand().getItemMeta().getDisplayName() == getItemFlag().getItemMeta().getDisplayName()){
				e.setCancelled(true);
				p.sendMessage("�cIl faut d�poser le drapeau dans la zone indiqu�e !");
			}
		}
	}

}
