package fr.parafight.inventories;

import java.util.HashMap;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import fr.parafight.Main;
import fr.parafight.utils.PlayerData;



public class CmdCoffre implements CommandExecutor {
	
	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;


	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String string, String[] args) {
		Player p = (Player) sender;
		if (sender instanceof Player) {
			if(p.hasPermission("parafight.privatechest")){
						p.openInventory(PlayerMap.get(p.getName()).getPrivateChest());
				}else{
					p.sendMessage("Vous n'avez pas d'inventaire priv� ! Achetez le sur le /boutique .");
				}
			}
		
		return false;
	}

}
