package fr.parafight.skills;

import java.util.HashMap;
import java.util.Random;
import org.bukkit.Bukkit;
import org.bukkit.CropState;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.block.BlockState;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Crops;
import org.bukkit.material.MaterialData;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

import fr.parafight.Main;
import fr.parafight.utils.PlayerData;


public class SkillsEvents implements Listener {

	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;

	@EventHandler
	public void onBreak(BlockBreakEvent e) {
		Player p = e.getPlayer();
		FPlayer fp = FPlayers.getInstance().getByPlayer(p);
		Faction faction = null;
		Random r = new Random();
		ItemStack item = p.getInventory().getItemInMainHand();
		if (e.getBlock().getType().isSolid()) {
			if (PlayerMap.get(p.getName()).getMineur() == 39999) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 100% de ses comp�tences Mineur ! Bravo � lui !");
				p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE,0, true, false));
			}
			if (PlayerMap.get(p.getName()).getMineur() == 29999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Mineur !");
			}
			if (PlayerMap.get(p.getName()).getMineur() == 19999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Mineur !");
			}
			if (PlayerMap.get(p.getName()).getMineur() == 9999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Mineur !");
			}
			Location loc = e.getBlock().getLocation();
			FLocation fLoc = new FLocation(loc);
			faction = Board.getInstance().getFactionAt(fLoc);
			if(faction.isWilderness() || faction == fp.getFaction()){
				switch (e.getBlock().getType()) {

				case IRON_ORE:
					if (PlayerMap.get(p.getName()).getMineur() >= 50.0) {
						if (((p.getInventory().getItemInMainHand().getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
								&& item.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1)) {
							return;
						} else {
							PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 5);
							e.setCancelled(true);
							e.getBlock().setType(Material.AIR);
							e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
									new ItemStack(Material.IRON_INGOT, 2));
						}
					}else if (PlayerMap.get(p.getName()).getMineur() >= 25.0) {
						if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
								&& item.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1)) {
							return;
						}else {
							PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 5);
							e.setCancelled(true);
							e.getBlock().setType(Material.AIR);
							e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
									new ItemStack(Material.IRON_INGOT, 1));
						}
					}
					break;
				case GOLD_ORE:
					if (PlayerMap.get(p.getName()).getMineur() >= 50.0) {
						if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
								&& item.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1)) {
							return;
						} else {
							PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 5);
							e.setCancelled(true);
							e.getBlock().setType(Material.AIR);
							e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
									new ItemStack(Material.GOLD_INGOT, 2));
						}
					}else if (PlayerMap.get(p.getName()).getMineur() >= 25.0) {
						if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
								&& item.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1)) {
							return;
						}else {
							PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 5);
							e.setCancelled(true);
							e.getBlock().setType(Material.AIR);
							e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
									new ItemStack(Material.GOLD_INGOT, 1));
						}
					}
					break;
				case OBSIDIAN:
					if (PlayerMap.get(p.getName()).getMineur() >= 75.0 && (Math.random() < 0.25)) {
						e.setCancelled(true);
						e.getBlock().setType(Material.AIR);
						e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
								new ItemStack(Material.OBSIDIAN, 2));
					}
					PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 20);
					break;
				case DIAMOND_ORE:
					if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
							&& item.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1)) {
						return;
					} else {
						PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 20);
						if (PlayerMap.get(p.getName()).getMineur() >= 50.0) {
							if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
									&& item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) == 3)) {
								e.setCancelled(true);
								e.getBlock().setType(Material.AIR);
								int i = r.nextInt(6);
								if (i == 0) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 2));
								}
								if (i == 1) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 3));
								}
								if (i == 2) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 4));
								}
								if (i == 3) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 5));
								}
								if (i == 4) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 6));
								}
								if (i == 5) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 7));
								}
								if (i == 6) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 8));
								}
							} else if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
									&& item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) == 2)) {
								e.setCancelled(true);
								e.getBlock().setType(Material.AIR);
								int i = r.nextInt(4);
								if (i == 0) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 2));
								}
								if (i == 1) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 3));
								}
								if (i == 2) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 4));
								}
								if (i == 3) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 5));
								}
								if (i == 4) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 6));
								}
							} else if (((item.getType() == Material.DIAMOND_PICKAXE || item.getType() == Material.IRON_PICKAXE)
									&& item.getEnchantmentLevel(Enchantment.LOOT_BONUS_BLOCKS) == 1)) {
								e.setCancelled(true);
								e.getBlock().setType(Material.AIR);
								int i = r.nextInt(2);
								if (i == 0) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 2));
								}
								if (i == 1) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 3));
								}
								if (i == 2) {
									e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
											new ItemStack(Material.DIAMOND, 4));
								}
							} else {
								e.setCancelled(true);
								e.getBlock().setType(Material.AIR);
								e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
										new ItemStack(Material.DIAMOND, 2));
							}
						}
					}
				default:
					PlayerMap.get(p.getName()).setMineur(PlayerMap.get(p.getName()).getMineur() + 1);
					break;
				}
			}
		}
	}

	@EventHandler
	public void onEDeath(EntityDeathEvent e) {
		LivingEntity e1 = e.getEntity();
		if(e1.getKiller() instanceof Player){
			Player p = (Player) e.getEntity().getKiller();
			if (e1.getType() != EntityType.PLAYER) {
				if (PlayerMap.get(p.getName()).getChasseurPercent() == 100.0) {
					e.setDroppedExp((int) (e.getDroppedExp() * 2));
				} else if (PlayerMap.get(p.getName()).getChasseurPercent() >= 80.0) {
					e.setDroppedExp((int) (e.getDroppedExp() * 1.8));
				} else if (PlayerMap.get(p.getName()).getChasseurPercent() >= 60.0) {
					e.setDroppedExp((int) (e.getDroppedExp() * 1.6));
				} else if (PlayerMap.get(p.getName()).getChasseurPercent() >= 40.0) {
					e.setDroppedExp((int) (e.getDroppedExp() * 1.4));
				} else if (PlayerMap.get(p.getName()).getChasseurPercent() >= 20.0) {
					e.setDroppedExp((int) (e.getDroppedExp() * 1.2));
				}
				if (PlayerMap.get(p.getName()).getChasseur() == 19999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 100% de ses comp�tences Chasseur ! Bravo � lui !");
				}
				if (PlayerMap.get(p.getName()).getChasseur() == 14999) {
					Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Chasseur !");
				}
				if (PlayerMap.get(p.getName()).getChasseur() == 9999) {
					Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Chasseur !");
				}
				if (PlayerMap.get(p.getName()).getChasseur() == 4999) {
					Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Chasseur !");
				}
				PlayerMap.get(p.getName()).setChasseur(PlayerMap.get(p.getName()).getChasseur() + 1);
			}
		}
	}

	@EventHandler
	public void onPDeath(PlayerDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			Player p = e.getEntity().getKiller();
			if (PlayerMap.get(p.getName()).getAssaillant() == 499) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 100% de ses comp�tences Assaillant ! Bravo � lui !");
			}
			if (PlayerMap.get(p.getName()).getAssaillant() == 374) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Assaillant !");
			}
			if (PlayerMap.get(p.getName()).getAssaillant() == 249) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Assaillant !");
			}
			if (PlayerMap.get(p.getName()).getAssaillant() == 124) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Assaillant !");
			}
			PlayerMap.get(p.getName()).setAssaillant(PlayerMap.get(p.getName()).getAssaillant() + 1);
		}
	}

	public boolean isFullyGrown(Block block) {

		BlockState state = block.getState();
		MaterialData data = state.getData();

		if (!(data instanceof Crops))
			return false;

		return (((Crops) data).getState() == CropState.RIPE);
	}

	@EventHandler
	public void onFarm(BlockBreakEvent e) {
		Player p = e.getPlayer();
		Random r = new Random();
		ItemStack item = p.getInventory().getItemInMainHand();
		switch (e.getBlock().getType()) {
		case MELON_BLOCK:
			if (PlayerMap.get(p.getName()).getFarmeur() == 19999) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 100% de ses comp�tences Farmeur ! Bravo � lui !");
			}
			if (PlayerMap.get(p.getName()).getFarmeur() == 14999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Farmeur !");
			}
			if (PlayerMap.get(p.getName()).getFarmeur() == 9999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Farmeur !");
			}
			if (PlayerMap.get(p.getName()).getFarmeur() == 4999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Farmeur !");
			}
			if (PlayerMap.get(p.getName()).getFarmeurPercert() == 100.0) {
				if (((item.getType() != null )
						&& item.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 1)) {
					return;
				} else { e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.MELON, 4 + r.nextInt(12)));
				}
			}
			PlayerMap.get(p.getName()).setFarmeur(PlayerMap.get(p.getName()).getFarmeur() + 1);
			break;
		case PUMPKIN:
			if (PlayerMap.get(p.getName()).getFarmeur() == 19999) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 100% de ses comp�tences Farmeur ! Bravo � lui !");
			}
			if (PlayerMap.get(p.getName()).getFarmeur() == 14999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Farmeur !");
			}
			if (PlayerMap.get(p.getName()).getFarmeur() == 9999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Farmeur !");
			}
			if (PlayerMap.get(p.getName()).getFarmeur() == 4999) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Farmeur !");
			}
			if (PlayerMap.get(p.getName()).getFarmeurPercert() == 100.0) {
				e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.PUMPKIN, 2));
			}
			PlayerMap.get(p.getName()).setFarmeur(PlayerMap.get(p.getName()).getFarmeur() + 1);
			break;
		case CROPS:
			if (isFullyGrown(e.getBlock())) {
				if (PlayerMap.get(p.getName()).getFarmeur() == 19999) {
					Bukkit.broadcastMessage("�5�l>> �d" + p.getName()
					+ " est arriv� � 100% de ses comp�tences Farmeur ! Bravo � lui !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 14999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 9999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 4999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeurPercert() == 100.0) {
					e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
							new ItemStack(Material.WHEAT, 6));
				}
				PlayerMap.get(p.getName()).setFarmeur(PlayerMap.get(p.getName()).getFarmeur() + 1);
			}
			break;
		case CARROT:
			if (isFullyGrown(e.getBlock())) {
				if (PlayerMap.get(p.getName()).getFarmeur() == 19999) {
					Bukkit.broadcastMessage("�5�l>> �d" + p.getName()
					+ " est arriv� � 100% de ses comp�tences Farmeur ! Bravo � lui !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 14999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 9999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 4999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeurPercert() == 100.0) {
					e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
							new ItemStack(Material.CARROT, 6));
				}
				PlayerMap.get(p.getName()).setFarmeur(PlayerMap.get(p.getName()).getFarmeur() + 1);
			}
			break;
		case POTATO:
			if (isFullyGrown(e.getBlock())) {
				if (PlayerMap.get(p.getName()).getFarmeur() == 19999) {
					Bukkit.broadcastMessage("�5�l>> �d" + p.getName()
					+ " est arriv� � 100% de ses comp�tences Farmeur ! Bravo � lui !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 14999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 9999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeur() == 4999) {
					Bukkit.broadcastMessage(
							"�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences Farmeur !");
				}
				if (PlayerMap.get(p.getName()).getFarmeurPercert() == 100.0) {
					e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
							new ItemStack(Material.POTATO, 6));
				}
				PlayerMap.get(p.getName()).setFarmeur(PlayerMap.get(p.getName()).getFarmeur() + 1);
			}
			break;
		case LEAVES:
			if(PlayerMap.get(p.getName()).getFarmeurPercert() >= 25.0 && (Math.random() < 0.25)){
				e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.APPLE, 1));
			}
			if(PlayerMap.get(p.getName()).getFarmeurPercert() >= 75.0 && (Math.random() < 0.05)){
				e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.GOLDEN_APPLE, 1));
			}
		case LEAVES_2:
			if(PlayerMap.get(p.getName()).getFarmeurPercert() >= 25.0 && (Math.random() < 0.25)){
				e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.APPLE, 1));
			}
			if(PlayerMap.get(p.getName()).getFarmeurPercert() >= 75.0 && (Math.random() < 0.05)){
				e.getBlock().getLocation().getWorld().dropItem(e.getBlock().getLocation(),
						new ItemStack(Material.GOLDEN_APPLE, 1));
			}
			break;
		default:
			break;
		}
	}

	@EventHandler
	public void onFish(PlayerFishEvent e) {
		Player p = e.getPlayer();
		if (e.getCaught() == null) {
			return;
		} else {
			if (PlayerMap.get(p.getName()).getPecheur() == 499) {
				Bukkit.broadcastMessage(
						"�5�l>> �d" + p.getName() + " est arriv� � 100% de ses comp�tences P�cheur ! Bravo � lui !");
			}
			if (PlayerMap.get(p.getName()).getPecheur() == 374) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 75% de ses comp�tences P�cheur !");
			}
			if (PlayerMap.get(p.getName()).getPecheur() == 249) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 50% de ses comp�tences P�cheur !");
			}
			if (PlayerMap.get(p.getName()).getPecheur() == 124) {
				Bukkit.broadcastMessage("�5�l>> �d" + p.getName() + " est arriv� � 25% de ses comp�tences P�cheur !");
			}
			PlayerMap.get(p.getName()).setPecheur(PlayerMap.get(p.getName()).getPecheur() + 1);
		}
		if (PlayerMap.get(p.getName()).getPecheurPercent() == 100.0) {
			e.setExpToDrop(e.getExpToDrop() * 10);
		} else if (PlayerMap.get(p.getName()).getPecheurPercent() >= 80.0) {
			e.setExpToDrop(e.getExpToDrop() * 8);
		} else if (PlayerMap.get(p.getName()).getPecheurPercent() >= 60.0) {
			e.setExpToDrop(e.getExpToDrop() * 6);
		} else if (PlayerMap.get(p.getName()).getPecheurPercent() >= 40.0) {
			e.setExpToDrop(e.getExpToDrop() * 4);
		} else if (PlayerMap.get(p.getName()).getPecheurPercent() >= 20.0) {
			e.setExpToDrop(e.getExpToDrop() * 2);
		}
	}

}

