package fr.parafight.skills;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import fr.parafight.Main;
import fr.parafight.utils.PlayerData;

public class SkillsGui implements Listener {
	
	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;


	public static Inventory Skills = Bukkit.createInventory(null, 27, "§dCompétences");

	public static ItemStack getItemTotal(double d) {
		ItemStack i = new ItemStack(new ItemStack(Material.NETHER_STAR, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lTOTAL");
		description.add(ChatColor.YELLOW + "");
		description.add("§dCompétences totales : §d§l" + d+"%");
		description.add(ChatColor.YELLOW + "");
		description.add("§6§lAvantages à 100 %:");
		description.add("§6§l✷ §eDevant votre pseudo");
		description.add("§eAccès au kit Sensei");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemMineur(double d) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_PICKAXE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lMineur");
		description.add(ChatColor.YELLOW + "");
		description.add("§dCompétences mineur : §d§l" + d+ "%");
		description.add(ChatColor.YELLOW + "");
		description.add(ChatColor.YELLOW + "Pour améliorer cette compétence, vous devez miner n'importe quels blocs,");
		description.add(ChatColor.YELLOW + "sachant que les blocs d'obsidienne et les minerais vallent 10 fois plus");
		description.add(ChatColor.YELLOW + "que les simples blocs!");
		description.add("§a");
		description.add("§6§lAvantages :");
		description.add("§e 25% §6 ➜ §e Le fer et l'or deviennent lingot instantanément au ramassage!");
		description.add("§e 50% §6 ➜ §e Le fer et l'or obtenu sont doublés !");
		description.add("§e 75% §6 ➜ §e 25% Obsidienne x2 au ramassage !");
		description.add("§e100% §6➜ §e Haste I illimité !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemChasseur(double d) {
		ItemStack i = new ItemStack(new ItemStack(Material.ARROW, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lChasseur");
		description.add(ChatColor.YELLOW + "");
		description.add("§dCompétences Chasseur : §d§l" + d + "%");
		description.add(ChatColor.YELLOW + "");
		description.add(ChatColor.YELLOW + "Pour améliorer cette compétence, vous devez tuer n'importe quels mobs,");
		description.add(ChatColor.YELLOW + "qu'ils soient passifs ou aggréssifs !");
		description.add("§a");
		description.add("§6§lAvantages :");
		description.add("§e 20% §6 ➜ §e +20% d'xp en tuant des mobs !");
		description.add("§e 40% §6 ➜ §e +40% d'xp en tuant des mobs !");
		description.add("§e 60% §6 ➜ §e +60% d'xp en tuant des mobs !");
		description.add("§e 80% §6 ➜ §e +80% d'xp en tuant des mobs !");
		description.add("§e100% §6➜ §e +100% d'xp en tuant des mobs !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemTueur(double d) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_SWORD, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lAssaillant");
		description.add(ChatColor.YELLOW + "");
		description.add("§dCompétences assaillant : §d§l" + d+ "%");
		description.add(ChatColor.YELLOW + "");
		description.add(ChatColor.YELLOW + "Pour améliorer cette compétence, vous ");
		description.add(ChatColor.YELLOW + "denez tuer des joueurs.");
		description.add("§a");
		description.add("§6§lAvantages :");
		description.add("§e 100% §6 ➜ §eToutes mes féficitations !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemFarmeur(double d) {
		ItemStack i = new ItemStack(new ItemStack(Material.DIAMOND_HOE, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lFarmeur");
		description.add(ChatColor.YELLOW + "");
		description.add("§dCompétences farmeur : §d§l" + d+ "%");
		description.add(ChatColor.YELLOW + "");
		description.add(ChatColor.YELLOW + "Pour améliorer cette compétence, vous devez récolter des");
		description.add(ChatColor.YELLOW + "melons, citrouilles, blé, pattates, carrotes ou cacao");
		description.add(ChatColor.YELLOW + "à la main");
		description.add("§a");
		description.add("§6§lAvantages :");
		description.add("§e 25% §6 ➜ §e 25% des feuillages cassés donnent une pomme.");
		description.add("§e 50% §6 ➜ §e Absolument rien !");
		description.add("§e 75% §6 ➜ §e 5% des feuillages cassés donnent une pomme d'or.");
		description.add("§e 100% §6 ➜ §e 50% de vos récoltes sont doublés !");
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	public static ItemStack getItemPecheur(double d) {
		ItemStack i = new ItemStack(new ItemStack(Material.FISHING_ROD, 1));
		ItemMeta im = i.getItemMeta();
		ArrayList<String> description = new ArrayList<String>();
		im.setDisplayName("§5§lPêcheur");
		description.add(ChatColor.YELLOW + "");
		description.add("§dCompétences pêcheur : §d§l" + d+ "%");
		description.add(ChatColor.YELLOW + "");
		description.add(ChatColor.YELLOW + "Pour améliorer cette compétence, vous devez pêcher");
		description.add(ChatColor.YELLOW + "toutes sortes de choses (poissons ou objets).");
		description.add("§a");
		description.add("§6§lAvantages :");
		description.add("§e 20% §6 ➜ §e +100% d'xp de pêche !");
		description.add("§e 40% §6 ➜ §e +200% d'xp de pêche !");
		description.add("§e 60% §6 ➜ §e +300% d'xp de pêche !");
		description.add("§e 80% §6 ➜ §e +400% d'xp de pêche !");
		description.add("§e100% §6➜ §e +500% d'xp de pêche !");
		
		im.setLore(description);
		i.setItemMeta(im);
		return i;
	}

	@EventHandler
	public void onClick(InventoryClickEvent e) {
		Player p = (Player) e.getWhoClicked();
		Inventory inv = e.getInventory();
		ItemStack current = e.getCurrentItem();

		if (current == null)
			return;
		if (inv.getName().equalsIgnoreCase(Skills.getName())) {
			e.setCancelled(true);
			p.closeInventory();
		}

	}

	@EventHandler
	public void onOpen(InventoryOpenEvent e1) {
		Inventory inv = e1.getInventory();
		Player p = (Player) e1.getPlayer();
		if (inv.getName().equalsIgnoreCase(Skills.getName())) {
			Skills.setItem(9, getItemTotal(PlayerMap.get(p.getName()).getTotalPercent()));
			Skills.setItem(2, new ItemStack(Material.STAINED_GLASS_PANE, 1 , (short)5));
			Skills.setItem(11, new ItemStack(Material.STAINED_GLASS_PANE, 1 , (short)5));
			Skills.setItem(20, new ItemStack(Material.STAINED_GLASS_PANE, 1 , (short)5));
			Skills.setItem(13, getItemMineur(PlayerMap.get(p.getName()).getMineurPercent()));
			Skills.setItem(14, getItemFarmeur(PlayerMap.get(p.getName()).getFarmeurPercert()));
			Skills.setItem(15, getItemTueur(PlayerMap.get(p.getName()).getAssaillantPercent()));
			Skills.setItem(16, getItemChasseur(PlayerMap.get(p.getName()).getChasseurPercent()));
			Skills.setItem(17, getItemPecheur(PlayerMap.get(p.getName()).getPecheurPercent()));
		}
	}
}
