package fr.parafight.skills;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class SkillsCmd implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
	    if ((sender instanceof Player)) {
	        Player p = (Player)sender;
	        if (label.equalsIgnoreCase("competences"))
	          p.openInventory(SkillsGui.Skills);
	        if (label.equalsIgnoreCase("skills"))
		          p.openInventory(SkillsGui.Skills);
	    }
		return false;
	}

}