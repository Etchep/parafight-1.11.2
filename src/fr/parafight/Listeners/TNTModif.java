package fr.parafight.Listeners;

import java.util.Iterator;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Fireball;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;

import com.massivecraft.factions.Board;
import com.massivecraft.factions.FLocation;
import com.massivecraft.factions.Faction;


public class TNTModif implements Listener{
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e){
		ItemStack i = e.getItem();
		Block target = e.getClickedBlock();
		if(i != null && i.getType() == Material.FLINT_AND_STEEL){
			if(e.getAction() == Action.RIGHT_CLICK_BLOCK && target != null && target.getType() == Material.TNT){
				target.setType(Material.AIR);
				e.setCancelled(true);
				Location location = target.getLocation().add(0.5, 0.25, 0.5);
				TNTPrimed tnt = (TNTPrimed) target.getWorld().spawnEntity(location, EntityType.PRIMED_TNT);
				tnt.setVelocity(new Vector(0, 0.25, 0));
				tnt.teleport(location);
			}
			
		}
	}
	
	@EventHandler
	public void onExplodeTnt(EntityExplodeEvent e){
		List<Block> blocks = e.blockList();
		Iterator<Block> bs = blocks.iterator();
		while(bs.hasNext()){
			Block b = bs.next();
			if(b.getType() == Material.TNT){
				b.setType(Material.AIR);
				Location location = b.getLocation().add(0.5, 0.25, 0.5);
				TNTPrimed tnt = (TNTPrimed) b.getWorld().spawnEntity(location, EntityType.PRIMED_TNT);
				tnt.setVelocity(new Vector(0, 0.25, 0));
				tnt.teleport(location);
			}
		}
	}
	@EventHandler
	public void onDamagebyother(EntityDamageByEntityEvent e){
		if(e.getDamager() instanceof TNTPrimed && e.getEntity() instanceof Player){
			TNTPrimed tnt = (TNTPrimed) e.getDamager();
			for(Entity entity : tnt.getNearbyEntities(1.5, 1.5, 1.5)){
				if(entity instanceof Player){
					Player player = (Player)entity;
					if(player.equals(player)){
						e.setDamage(e.getDamage() * 0.1);
						player.setVelocity(player.getLocation().getDirection().multiply(e.getDamage()/10).setY(1));
					}
				}
			}
		}
		if(e.getDamager() instanceof Fireball && e.getEntity() instanceof Player){
			Faction faction = null;
			Fireball ball = (Fireball)e.getDamager();
			for(Entity entity : ball.getNearbyEntities(1.5, 1.5, 1.5)){
				if(entity instanceof Player){
					Player player = (Player)entity;
					Location loc = player.getLocation();
					FLocation fLoc = new FLocation(loc);
					faction = Board.getInstance().getFactionAt(fLoc);
					if(player.equals(player)){
						if(faction.isSafeZone()){
							return;
						}
						player.setVelocity(player.getLocation().getDirection().multiply(e.getDamage()/10).setY(1));
					}
				}
			}
		}
	}

}
