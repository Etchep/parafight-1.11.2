package fr.parafight.Listeners;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.HumanEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.inventory.AnvilInventory;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.InventoryView;
import org.bukkit.inventory.ItemStack;

import fr.parafight.Main;
import net.milkbowl.vault.economy.Economy;

public class NoRepairPickaxe implements Listener {
	public static Economy econ = null;

	@EventHandler
	public void onInventoryClick3(InventoryClickEvent e)
	{
		if (!e.isCancelled()) {
			HumanEntity ent = e.getWhoClicked();

			if ((ent instanceof Player)) {
				Inventory inv = e.getInventory();

				if ((inv instanceof AnvilInventory)) {
					InventoryView view = e.getView();
					int rawSlot = e.getRawSlot();

					if (rawSlot == view.convertSlot(rawSlot))
					{
						if (rawSlot == 2)
						{
							ItemStack item = e.getCurrentItem();

							if (item != null) {
								if(item.hasItemMeta()){
									if (e.getView().getItem(0).getType() != Material.AIR) {
										if(e.getView().getItem(0).getItemMeta().getDisplayName().contains("spawner")){
											ent.closeInventory();
											e.setCancelled(true);
										}
									}
									if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).contains("spawner")){
										ent.closeInventory();
										e.setCancelled(true);	
									}
									if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).contains("GoldenKey"))
										e.setCancelled(true);
									if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).contains("DiamondKey"))
										e.setCancelled(true);
									if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).contains("EmeraldKey"))
										e.setCancelled(true);
									if (ChatColor.stripColor(e.getCurrentItem().getItemMeta().getDisplayName()).contains("Votes"))
										e.setCancelled(true);
								}
							}
						}
					}
				}
			}
		}
	}
	@EventHandler
	public void onPreCommand(PlayerCommandPreprocessEvent e)
	{
		Player p = e.getPlayer();
		ItemStack itemInHand = p.getInventory().getItemInMainHand();
		if ((e.getMessage().contains("fix")) || (e.getMessage().contains("repair"))){
			if ((itemInHand != null) && (itemInHand.getType() != Material.AIR)) {
				if((itemInHand.getType() == Material.GOLD_PICKAXE)){
					e.setCancelled(true);
					e.getPlayer().sendMessage(ChatColor.RED + "Vous ne pouvez pas r�parer une pioche � spawner !");
				}
				if ((itemInHand.hasItemMeta()) && (itemInHand.getItemMeta().getLore() != null) && (itemInHand.getItemMeta().getLore().equals(Main.lorepioche))){
					e.setCancelled(true);
					e.getPlayer().sendMessage(ChatColor.RED + "Vous ne pouvez pas r�parer une pioche 3x3 avec le /repair, utilisez une enclume.");
				}
				if(!p.getInventory().getItemInMainHand().getType().isBlock()){
					if(p.hasPermission("capitaine")){
						p.sendMessage("�aVous n'avez pas eu � payer la r�paration." );
					}else if(p.hasPermission("major")){
						int amount = 100;
						if(Main.econ.getBalance(p) >= amount){
							p.sendMessage("�cVous avez pay� "+ amount+ "� pour la r�paration." );
							Main.econ.withdrawPlayer(p, amount);
						}
					}else if (p.hasPermission("sergent")){
						int amount = 250;
						if(Main.econ.getBalance(p) >= amount){
						p.sendMessage("�cVous avez pay� "+ amount+ "� pour la r�paration." );
						Main.econ.withdrawPlayer(p, amount);
						}
					}else if (p.hasPermission("caporal")){
						int amount = 500;
						if(Main.econ.getBalance(p) >= amount){
						p.sendMessage("�cVous avez pay� "+ amount+ "� pour la r�paration." );
						Main.econ.withdrawPlayer(p, amount);
						}
					}
				}
			}
		}
	}

}
