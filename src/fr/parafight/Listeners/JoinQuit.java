package fr.parafight.Listeners;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import fr.parafight.Main;
import fr.parafight.sql.SqlConnection;
import fr.parafight.utils.PlayerData;

public class JoinQuit implements Listener {
	
	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;
	
	  private Main main;

	  public JoinQuit(Main main)
	  {
	    this.main = main;
	  }
	  
		public Inventory createInventory(Player player) {
		    return Bukkit.getServer().createInventory(player, 54, "§6§lInventaire Privé");
		}
	  

	@EventHandler
	public void onJoin(PlayerJoinEvent e){		
		Player p = e.getPlayer();
		if(SqlConnection.hasAccount(p) == false){
			SqlConnection.createAccount(p);
		}
		this.main.title.sendTitle(p, "§dBienvenue !", "§eAmuse toi bien sur §e§lPara§6§lfight §e!", 40);
		
		p.sendMessage("§d§lBienvenue sur Parafight §e§lPara§6§lfight §d!");
		
		p.sendMessage("§eTapez la commande /info pour obtenir les informations importantes sur le serveur.");
		
		p.sendMessage("§4⚠ §cN'oubliez pas de déposer régulièrement votre argent dans votre banque, l'argent non déposé à la banque est looté à votre mort !.");
		
		PlayerMap.put(p.getName(), new PlayerData(SqlConnection.getMineurExp(p), SqlConnection.getKillExp(p), SqlConnection.getChasseurExp(p), SqlConnection.getFarmExp(p), SqlConnection.getPecheurExp(p), SqlConnection.getPrivateInv(p), SqlConnection.getBankBalance(p)));

		if(PlayerMap.get(p.getName()).getTotalPercent() == 100 && (!p.hasPermission("essentials.kits.sensei"))){
			Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(),
					"pex user " + p.getName() + " add essentials.kits.sensei");
		}
		if (PlayerMap.get(p.getName()).getMineur() >= 40000) {
			p.addPotionEffect(new PotionEffect(PotionEffectType.FAST_DIGGING, Integer.MAX_VALUE,0, true, false));
		}
	}
	
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e){
		Player p = e.getPlayer();
		PlayerData data = PlayerMap.get(p.getName());
		SqlConnection.updateAll(p, data.getMineur(), data.getFarmeur(), data.getAssaillant(), data.getPecheur(), data.getChasseur(), data.getPrivateChest(), data.getBankBalance());
		PlayerMap.remove(p.getName());
	}
	
	

}
