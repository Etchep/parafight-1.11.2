package fr.parafight.Listeners;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.bukkit.scheduler.BukkitRunnable;

import fr.parafight.Main;

public class NoHitCooldown implements Listener{
	
	  private Main main;

	  public NoHitCooldown(Main main)
	  {
	    this.main = main;
	  }
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		resetCooldown(p);
	}
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e){
		new BukkitRunnable() {
			Player p = e.getPlayer();
			@Override
			public void run() {
				resetCooldown(p);
				
			}
		}.runTaskLater(main, 20);
	}
	
	private void resetCooldown(Player p){
		p.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100D);
	}

}
