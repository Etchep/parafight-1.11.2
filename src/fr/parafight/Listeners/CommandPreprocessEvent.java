package fr.parafight.Listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;

import fr.parafight.gui.WarpsGui;

public class CommandPreprocessEvent implements Listener {
	
	@EventHandler
	public void onCommand(PlayerCommandPreprocessEvent e){
		Player p = e.getPlayer();
		if (e.getMessage().contains("/minecraft:me")){
			e.setCancelled(true);
			p.sendMessage("Nop");
		}	
		if (e.getMessage().contains("/bukkit:")){
			e.setCancelled(true);
			p.sendMessage("Nop");
		}
		if (e.getMessage().contains("/mineweb")){
			e.setCancelled(true);
			p.sendMessage("Nop");
		}
		if (e.getMessage().contains("/warp")){
			e.setCancelled(true);
			p.openInventory(WarpsGui.Warps);
		}
		if (e.getMessage().contains("/minecraft:tell")){
			e.setCancelled(true);
		}
		if (e.getMessage().contains("/afk")){
			e.setCancelled(true);
		}
		if (e.getMessage().equalsIgnoreCase("/help")){
			e.setMessage("/info");
		}
		if(e.getMessage().toLowerCase().contains("/op")){
			e.setCancelled(true);
			p.sendMessage("Nop");
		}
		if(e.getMessage().toLowerCase().contains("group set admin") || e.getMessage().toLowerCase().contains("add *") 
				|| e.getMessage().toLowerCase().contains("group set fondateur")){
			e.setCancelled(true);
			p.sendMessage("Nop");
		}
		if(e.getMessage().equalsIgnoreCase("/f top")){
			e.setMessage("/classement");
		}
		if(e.getMessage().equalsIgnoreCase("/f classement")){
			e.setMessage("/classement");
		}
		if(e.getMessage().equalsIgnoreCase("/f ap")){
			e.setMessage("/ap");
		}
		if(e.getMessage().equalsIgnoreCase("/f setap")){
			e.setMessage("/setap");
		}
		if(e.getMessage().equalsIgnoreCase("/info") || e.getMessage().equalsIgnoreCase("/infos")){
			e.setCancelled(true);
	        p.sendMessage("§6§lInformations générales sur le serveur :");
	        p.sendMessage("");
	        
	        p.sendMessage("-§e§lPotions de Force II bloquées");
	        p.sendMessage("-§e§lCooldown Pomme cheat : 1 minute");
	        p.sendMessage("-§e§lLimite de la map : §615 000 x 15 000 blocs");
	        p.sendMessage("");
	        p.sendMessage("§e§lRésistance des blocs aux explosions :");
	        p.sendMessage("   §eTapez la commande §6§l/explosions§c.");
	        p.sendMessage("");
	        p.sendMessage("§e§lGrades youtubeurs:");
	        p.sendMessage("   §eTapez la commande §6§l/ytb§c.");
	        p.sendMessage("");
	        p.sendMessage("");
	        p.sendMessage("§e§lBanque:");
	        p.sendMessage("	  §eConsulter votre solde banquaire : /banque");
	        p.sendMessage("	  §eDéposer/retirer de l'argent : /warp -> banque");
	        p.sendMessage("§4⚠ §cN'oubliez pas de déposer régulièrement votre argent dans votre banque, l'argent non déposé à la banque est looté à votre mort !.");
	        p.sendMessage("");
	        p.sendMessage("-§e§lSite web : §6https://parafight.fr/");
	        p.sendMessage("-§e§lDiscord : §6https://discord.gg/Mun6vYu");
	        p.sendMessage("-§e§lIP : §6play.parafight.fr");
		}
	}

}
