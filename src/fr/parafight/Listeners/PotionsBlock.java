package fr.parafight.Listeners;
import java.util.HashMap;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PotionSplashEvent;
import org.bukkit.event.player.PlayerItemConsumeEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.PotionMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.potion.PotionType;

public class PotionsBlock implements Listener {
	private static final HashMap<String, Long> cooldown = new HashMap<String, Long>();

	@EventHandler(priority=EventPriority.HIGHEST)
	private final void onPlayerItemConsume(PlayerItemConsumeEvent event)
	{
		ItemStack item = event.getItem();
		if ((item.getType() == Material.GOLDEN_APPLE) && (item.getDurability() == 1)) {
			Player player = event.getPlayer();
			String playerName = player.getName();
			Long eatTime = (Long)cooldown.get(playerName);
			if (eatTime != null) {
				eatTime = Long.valueOf((System.currentTimeMillis() - eatTime.longValue()) / 1000L);
				if (eatTime.longValue() < 60L) {
					event.setCancelled(true);
					player.sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_PURPLE + "Cooldown" + ChatColor.DARK_GRAY + "]" + ChatColor.RED + (60L - eatTime.longValue()) + ChatColor.GOLD + " secondes pour pouvoir manger une pomme cheat.");
					return;
				}
			}
			cooldown.put(playerName, Long.valueOf(System.currentTimeMillis()));
			event.getPlayer().sendMessage(ChatColor.DARK_GRAY + "[" + ChatColor.DARK_PURPLE + "Cooldown" + ChatColor.DARK_GRAY + "]" + ChatColor.GOLD + 
					"Tu as consomm� une pomme cheat, tu dois maintenant attendre �c1 �6minute avant de pouvoir en consommer une autre.");
		}
	} 
	@EventHandler
	public void onSplashPotion(PotionSplashEvent e){
		if (e.getEntity().getShooter() instanceof Player){
			Player p = (Player) e.getEntity().getShooter();
			for (PotionEffect effect : e.getPotion().getEffects()){
				if ((effect.getType().getName().equals(PotionEffectType.INCREASE_DAMAGE.getName())) && (effect.getAmplifier() == 1)) {
					p.sendMessage(ChatColor.DARK_RED + "Les potions de Force II sont d�sactiv�es!");
					e.setCancelled(true);
				}
			}

		}
	}

	@EventHandler
	public void onDrinkPotion(PlayerItemConsumeEvent e){
		Player player = e.getPlayer();
		if ((e.getItem() != null) && (e.getItem().getType() == Material.POTION)){
			PotionMeta meta = (PotionMeta) e.getItem().getItemMeta();
			PotionType pt = PotionType.STRENGTH;
			if(meta.getBasePotionData().getType() == pt && meta.getBasePotionData().isUpgraded()){
				player.sendMessage(ChatColor.DARK_RED + "Les potions de Force II sont d�sactiv�es!");
				player.addPotionEffect(new PotionEffect(PotionEffectType.INCREASE_DAMAGE, 7200, 0, true, false));
				player.getInventory().setItem(player.getInventory().getHeldItemSlot(), new ItemStack(Material.GLASS_BOTTLE));
				e.setCancelled(true);
			}
		}

	}

}