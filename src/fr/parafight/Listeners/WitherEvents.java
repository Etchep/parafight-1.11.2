package fr.parafight.Listeners;

import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityChangeBlockEvent;
import org.bukkit.scheduler.BukkitRunnable;

import fr.parafight.Main;

public class WitherEvents implements Listener{
	
	@EventHandler
	public void onCreateSpawn(CreatureSpawnEvent e) {
	    if (e.getEntity().getType() == EntityType.WITHER) {
	        Entity wither = e.getEntity();
			new BukkitRunnable() {
				@Override
				public void run() {
					wither.remove();
					
				}
			}.runTaskLater(Main.getInstance(), 2400);
	        wither.remove();
	    }
	}

	@EventHandler
	public void onBlockEat(EntityChangeBlockEvent e){
		if (e.getEntity().getType() == EntityType.WITHER){
			e.setCancelled(true);
			}
	}
}
