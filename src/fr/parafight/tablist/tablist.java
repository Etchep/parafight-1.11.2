package fr.parafight.tablist;

import java.lang.reflect.Field;

import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_11_R1.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.scheduler.BukkitRunnable;

import fr.parafight.Main;
import net.minecraft.server.v1_11_R1.ChatComponentText;
import net.minecraft.server.v1_11_R1.PacketPlayOutPlayerListHeaderFooter;

public class tablist implements Listener{
	
	private Main main;

	public tablist(Main main) {
		this.main = main;
	}
	private int timer = 0;

	@EventHandler
	public void onJoin(PlayerJoinEvent e){
		Player p = e.getPlayer();
		PacketPlayOutPlayerListHeaderFooter packet = new PacketPlayOutPlayerListHeaderFooter();		
		new BukkitRunnable() {
			@Override
			public void run() {
				ChatComponentText header = new ChatComponentText("§6✷ §d§lParafight§6 ✷\n\n§a Bienvenue sur Parafight\n§e§aAmusez-vous bien !\n§6§m-------------------------");
				ChatComponentText footer = new ChatComponentText("§6§m-------------------------\n§eJoueurs en ligne : §6"+ Bukkit.getServer().getOnlinePlayers().size() + "\n§eVotre lantence : §6" + ((CraftPlayer)p).getHandle().ping + "§e ms");
				if(timer == 1){
					header = new ChatComponentText("§6✷ §d§lParafight§6 ✷\n\n§e §a Partagez l'ip à vos amis !\n§2IP : §aplay.parafight.fr\n§6§m-------------------------");
				}else
				if(timer == 2){
					header = new ChatComponentText("§6✷ §d§lParafight§6 ✷\n\n§e§a Visitez notre site web !\n§2Site : §ahttps://parafight.fr/\n§6§m-------------------------");
				}else
				if(timer == 3){
					header = new ChatComponentText("§6✷ §d§lParafight§6 ✷\n\n§e §a Rejoignez notre Discord !\n§2Discord : §a/discord\n§6§m-------------------------");
					timer = 0;
				}
				try {
					Field a = packet.getClass().getDeclaredField("a");
					a.setAccessible(true);
					Field b = packet.getClass().getDeclaredField("b");
					b.setAccessible(true);
					
					a.set(packet, header);
					b.set(packet, footer);
					
					if(Bukkit.getOnlinePlayers().size() == 0) return;
					
					
						((CraftPlayer) p).getHandle().playerConnection.sendPacket(packet);
					
				} catch (NoSuchFieldException | IllegalArgumentException | IllegalAccessException e) {
					e.printStackTrace();
				}
				timer++;
			}
		}.runTaskTimer(main, 10, 100);
		
	}

}
