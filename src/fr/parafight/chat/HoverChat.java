package fr.parafight.chat;

import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Statistic;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.maxgamer.maxbans.MaxBans;
import org.maxgamer.maxbans.banmanager.Mute;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;
import com.massivecraft.factions.struct.ChatMode;

import fr.parafight.Main;
import fr.parafight.utils.PlayerData;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;

public class HoverChat implements Listener{
	
	private HashMap<String, PlayerData> PlayerMap = Main.PlayerMap;

	@EventHandler(priority = EventPriority.HIGHEST)
	public void onChat(AsyncPlayerChatEvent e) {
		e.setCancelled(true);
		Player player = e.getPlayer();
		FPlayer fplayer = FPlayers.getInstance().getByPlayer(player);
		ChatMode chat = fplayer.getChatMode();
		System.out.println(player.getName() + " : " + e.getMessage());
		if(MaxBans.instance.getBanManager().getMutes().containsKey(player.getName())){
			return;
		}
		Mute mute = MaxBans.instance.getBanManager().getMute(player.getName());
		if (mute != null) {
			return;
		}
		if(chat == ChatMode.PUBLIC){
			if (ChatManageCmd.muted == false || (ChatManageCmd.muted == true && player.hasPermission("parafight.chatmanage"))) {


				for(Player p : Bukkit.getOnlinePlayers()){
					ChatColor cc;


					FPlayer mp = FPlayers.getInstance().getByPlayer(p);
					double rn1 = fplayer.getPower() * 100;
					double rn = (int) rn1 / 100;
					double max = fplayer.getPowerMax();
					cc = fplayer.getColorTo(mp);
					String prefix = fplayer.getRolePrefix();
					Faction fname = fplayer.getFaction();

					String s = e.getFormat();
					if(PlayerMap.get(player.getName()).getTotalPercent() == 100){
						s = s.replaceAll("%1\\$s", "§l§6✷" + player.getDisplayName());
					}else{
						s = s.replaceAll("%1\\$s", player.getDisplayName());
					}
					s = s.replaceAll(" %2\\$s", "");
					s = s.replaceAll("%2\\$s", "");
					if(!fname.isWilderness()){
						s = s.replaceAll("\\{factions_name\\}", fname.getTag()+" ");
						s = s.replaceAll("\\{factions_roleprefix\\}", prefix);
						s = s.replaceAll("\\{factions_relcolor\\}", cc+"");
					}else{
						s = s.replaceAll("\\{factions_name\\}", "");
						s = s.replaceAll("\\{factions_roleprefix\\}", "");
						s = s.replaceAll("\\{factions_relcolor\\}", "");
					}
					TextComponent ss = new TextComponent(s);
					ss.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§6Pseudo : §e§l"+ player.getName() + ""
							+ "\n§6Argent ➥ §e "+ Main.econ.getBalance(player) + "€"
							+ "\n§6Power ➥ §e" + rn + "/" + max 
							+ "\n§6Kills ➥ §e " + player.getStatistic(Statistic.PLAYER_KILLS)
							+ "\n§6Morts ➥ §e " + player.getStatistic(Statistic.DEATHS)
							+ "\n§6Compétences totales ➥§e " + PlayerMap.get(player.getName()).getTotalPercent() + "%").create()));
					String msg = e.getMessage();
					ss.addExtra(msg);
					if(p.hasPermission("parafight.autosanction")){
						ss.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/as " + player.getName()));
					}
					p.spigot().sendMessage(ss);
				}
			}
		}
	}
}
