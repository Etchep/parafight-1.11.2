package fr.parafight.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import com.massivecraft.factions.FPlayer;
import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.struct.ChatMode;

public class ChatMentions implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent e)
	{
		Player player = e.getPlayer();
		FPlayer fplayer = FPlayers.getInstance().getByPlayer(player);
		ChatMode chat = fplayer.getChatMode();
		if(chat == ChatMode.PUBLIC){	
			String newMessage = e.getMessage();

			for (Player on : Bukkit.getServer().getOnlinePlayers())
			{
				newMessage = newMessage.replaceAll(on.getName(), ChatColor.GOLD + "�l" + on.getName() + ChatColor.RESET);
				if (e.getMessage().toLowerCase().contains(on.getName().toLowerCase())) {
					on.playSound(on.getLocation(), Sound.ENTITY_CAT_AMBIENT, 10.0F, 5.0F);
				}
			}

			e.setMessage(newMessage);
		}
	}

	@EventHandler
	public void onChat2(AsyncPlayerChatEvent e2) {
		String newMessage = e2.getMessage();
		for (Player on : Bukkit.getServer().getOnlinePlayers()) {
			newMessage = newMessage.replaceAll("@ " + on.getName(), "");
			if (e2.getMessage().toLowerCase().contains("@ " + on.getName().toLowerCase()))
				e2.setMessage(on.getName() + " �e�l>�r " + newMessage);
		}
	}
}
