package fr.parafight.chat;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;


public class ChatManageCmd implements CommandExecutor {
	

	  public static boolean muted = false;


	  public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	  {
	    if (cmd.getName().equalsIgnoreCase("chat")) {

	      Player p = (Player)sender;

	      if (args.length == 0) {
	        if (p.hasPermission("parafight.chatmanage"))
	        {
	        	p.sendMessage("�8�lCommandes de chat:");
	        	p.sendMessage("�7/chat toggle = activer/d�sactiver le chat");
	        	p.sendMessage("�7/chat clear = clear le chat");
	          return true;
	        }

	        p.sendMessage("&cVous n'avez pas la permission.");
	      }

	      if ((args.length == 1) && (args[0].equalsIgnoreCase("clear"))) {
	        if (p.hasPermission("chatmodifier.clearchat"))
	        {
	          for (int x = 0; x < 100; x++) {
	            Bukkit.broadcastMessage(" ");
	          }
	          Bukkit.broadcastMessage("�9�lLe chat a �t� clear.");
	        }
	        else
	        {
	        	p.sendMessage("&cVous n'avez pas la permission.");
	        }
	      }

	      if ((args.length == 1) && (args[0].equalsIgnoreCase("toggle")))
	      {
	        if (!p.hasPermission("parafight.chatmanage")) {
	        	p.sendMessage("&cVous n'avez pas la permission.");
	        }

	        if ((!muted) && (p.hasPermission("parafight.chatmanage"))) {
	          muted = true;
	          Bukkit.broadcastMessage("�5�l>> �d�lLe chat a �t� d�sactiv�.");
	        }
	        else if ((muted) && (p.hasPermission("parafight.chatmanage"))) {
	          muted = false;
	          Bukkit.broadcastMessage("�5�l>> �d�lLe chat a �t� r�activ�.");
	        }
	      }
	    }
	    return true;
	  }

}
